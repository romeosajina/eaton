# EatOn

## The Eat Is On

EatOn aplikacija omogućuje naručivanje jela iz restorana, fast food-a, pizzeria i slično.

Omogućuje trenutno praćenje narudžbi i rezervacija.

#### Aplikaciju mozete za potrebe testiranja preuzeti sa google play-a: https://play.google.com/store/apps/details?id=hr.nss.eaton 




### Početni ekran aplikacije prikazuje nedavno kupovane jela i restorani iz kojih su ta jela kupljena.




![Scheme](EatOnPics/main.png "Glavni ekran") 



### Sa lijeve strane nalazi se drawer za odabir korisničkih akcija


![Scheme](EatOnPics/drawer.png "Drawer za odabir akcija") 


### Postavke parametara za pretraživanje jela i mjesta


![Scheme](EatOnPics/postavke_pretrazivanja.png "Postavke pretraživanja za jela i mjesta") 


### Prikaz restorana na mapi

![Scheme](EatOnPics/mapa.png "Prikaz restorana na mapi") 


### Prikaz svi jela za zadane parametre (ako ih ima)

![Scheme](EatOnPics/pregled_itema.png "Lista svih jela") 


### Prikaz detalja jednog jela

![Scheme](EatOnPics/detalj_itema.png "") 


### Prikaz svih mjesta za zadane parametre (ako ih ima)

![Scheme](EatOnPics/pregled_mjesta.png "Lista svih mjesta")


### Prikaz detalja mjesta


![Scheme](EatOnPics/detalj_mjesta.png "Detalji mjesta") 


### Pregled trenutno aktivnih narudžbi


![Scheme](EatOnPics/nedavne_aktivnosti.png "Pregled narudžbi") 


### Detalji jedne od narudžbi


![Scheme](EatOnPics/detalj_narudzbe.png "Detalji narudžbe")


### Postavke korisničkih podataka 


![Scheme](EatOnPics/postavke.png "Postavke osobnih podataka") 


