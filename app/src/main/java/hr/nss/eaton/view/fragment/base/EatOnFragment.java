package hr.nss.eaton.view.fragment.base;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import java.util.List;

import hr.nss.eaton.R;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.utils.ReflectionUtils;
import hr.nss.eaton.view.listener.DataChangedListener;
import hr.nss.eaton.view.listener.ListItemListener;

/**
 * Created by Robert on 01-Nov-18.
 */

public abstract class EatOnFragment<T extends EatOnObject, A extends EatOnAdapter> extends Fragment {


    protected ListItemListener<T> mListener;
    protected DataChangedListener<T> dataChangedListener;


    protected List<T> data;

    protected A adapter;

    private int inflateLayoutId;
    private int adapterInflateLayoutId;

    public EatOnFragment(int inflateLayoutId,int adapterInflateLayoutId ){
        this.inflateLayoutId = inflateLayoutId;
        this.adapterInflateLayoutId = adapterInflateLayoutId;
    }


    public void setData(List<T> data) {
        this.data = data;
        if(adapter != null) adapter.setData(data);
    }
    public void notifyDataChanged(){
        setData(data);
    }



    public static <T extends EatOnFragment> T newInstance(Class<T> fragmentClass, List<? extends  EatOnObject> data) {
        T fragment = null;
        try {
            fragment = fragmentClass.newInstance();
            fragment.data = data;
        } catch (java.lang.InstantiationException | IllegalAccessException e) {
           EatOnObject.log("Error instanciating fragment class "+fragmentClass);
        }
        return fragment;
    }

    protected RecyclerView.LayoutManager getLayoutManager(Context context){
        return new LinearLayoutManager(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(inflateLayoutId, container, false);

        if (view instanceof RecyclerView) {

            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;

            recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(context, R.anim.layout_slide));

            RecyclerView.LayoutManager  layoutManager = getLayoutManager(context);
            if(layoutManager != null)
                recyclerView.setLayoutManager(layoutManager);

            adapter = createAdapter();
            onCreateAdapter();
            recyclerView.setAdapter(adapter);
        }

        return view;
    }

    private A createAdapter(){
        A adapter = null;
        try {
            Class<A> adapterClass = ReflectionUtils.getGenericClassArgumentType(getClass(), 1);
            adapter = adapterClass.newInstance();
            adapter.setData(data);
            adapter.setItemListener(mListener);
            adapter.setDataChangedListener(dataChangedListener);
            adapter.setInflateLayoutId(adapterInflateLayoutId);

        } catch (java.lang.InstantiationException | IllegalAccessException e) {
            EatOnObject.log("No constructor for EatOnAdapter for class "+getClass());
        }
        return adapter;
    }
    protected void onCreateAdapter(){

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //  mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        parseListeners(context);
    }


    protected void parseListeners(Object context){
        if (context instanceof ListItemListener) {
            mListener = (ListItemListener) context;
        }
        if( context instanceof  DataChangedListener)
            dataChangedListener = (DataChangedListener) context;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        dataChangedListener = null;
    }


    public int getItemCount(){
        return adapter.getItemCount();
    }




}
