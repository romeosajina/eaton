package hr.nss.eaton.view.activity;

import android.content.Intent;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import hr.nss.eaton.R;
import hr.nss.eaton.view.activity.base.EatOnActivity;

public class NoInternetActivity extends EatOnActivity {


    @Override
    protected boolean isInternetConnectionAvailable() {
        return false;
    }

    @Override
    protected void showNoInternetActivity() {
        setContentView(R.layout.activity_no_internet);

        final SwipeRefreshLayout pullToRefresh = findViewById(R.id.activity_no_internet_refresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkIfConnected();
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    private void checkIfConnected(){
        if(super.isInternetConnectionAvailable())startActivity(new Intent(this, MainActivity.class));

    }

    @Override
    public void onBackPressed() {
        if(super.isInternetConnectionAvailable())
            startActivity(new Intent(this, MainActivity.class));
        else finishAffinity();
    }
}
