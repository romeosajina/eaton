package hr.nss.eaton.view.component;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import hr.nss.eaton.model.common.EatOnObject;


/**
 * Created by Robert on 09-Nov-18.
 */

public class EatOnExpandableListView extends ExpandableListView {
    public EatOnExpandableListView(Context context) {
        super(context);
    }

    public EatOnExpandableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EatOnExpandableListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public EatOnExpandableListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);


    }

    @Override
    public void setAdapter(ExpandableListAdapter adapter) {
        super.setAdapter(adapter);
        expandAll();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        moveArrowToRight();
    }


    private void moveArrowToRight(){

        int left = getRight() - (int)convertDpToPixel(80), right = getWidth();

        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            setIndicatorBounds(left, right);
        } else {
            setIndicatorBoundsRelative(left, right);
        }
    }
    public float convertDpToPixel(float dp){
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public void expandAll() {

        ExpandableListAdapter adapter = getExpandableListAdapter();

        if (adapter == null) return;

        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandGroup(i);
        }
    //    smoothScrollToPosition(0);
    }


    /*

    @Override
    public boolean performItemClick(View v, int position, long id) {

        boolean pref =  super.performItemClick(v, position, id);

        ViewGroup vg = (ViewGroup)v;
        if(vg.getLayoutAnimation() == null) vg.setLayoutAnimation(getLayoutAnimation());
        vg.startLayoutAnimation();


        EatOnObject.log(getChildCount());

        if(isGroupExpanded(position)){
            post(new Runnable() {
                @Override
                public void run() {
                    EatOnObject.log(getChildCount());

                    for(int i = 0; i < getChildCount(); i ++){
                        View child = getChildAt(i);
                        if(child.getId() == R.id.fragment_menu_item_details_list_group_item_card_view){
                            ViewGroup viewGroup = (ViewGroup)child;
                            if(viewGroup.getLayoutAnimation() == null) viewGroup.setLayoutAnimation(getLayoutAnimation());
                            viewGroup.startLayoutAnimation();
                            EatOnObject.log(viewGroup.getParent());

                        }
                    }
                }
            });
        }

        return pref;
    }
*/

}
