package hr.nss.eaton.view.activity;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.fragment.app.DialogFragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.nss.eaton.R;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.data.RequestListener;

import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.activity.base.EatOnActivity;
import hr.nss.eaton.view.dialog.SearchParamsDialogFragment;
import hr.nss.eaton.view.fragment.company.CompanyFragment;
import hr.nss.eaton.view.fragment.menuitem.MenuItemFragment;

public class SearchActivity extends EatOnActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    private User user;
    private BottomNavigationView navigation;
    @Override
    protected void onEatOnCreate(Bundle savedInstanceState) {
        super.onEatOnCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setupToolbarAndBackButton();

        navigation = findViewById(R.id.search_bottom_navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        requestUser(new RequestListener<User>() {
            @Override
            public void success(User user) {
                SearchActivity.this.user = user;
                onNavigationItemSelected(R.id.search_navigation_delivery_only);
            }

            @Override
            public void failure(Object response, Throwable reason) {
                showNoDataView();
                handleRequestFailure(response, reason);
            }
        });
    }

    private Map<String, Object> getSearchParams(){
        return (user != null) ? user.getSearchParams() : new HashMap<String, Object>();
    }

    private void setSearchParam(String name, Object value){
        if(user != null){
            user.setSearchParam(name, value);
            setUserParams(user);
        }
    }

    private void showMenuItems(){
        DataProvider.get().requestMenuItems(new RequestListener<List<hr.nss.eaton.model.menu.MenuItem>>() {
            @Override
            public void success(List<hr.nss.eaton.model.menu.MenuItem> menuItems) {
                showMenuItemsFragment(menuItems);
            }
            public void failure(Object response, Throwable reason) {
                showNoDataView();
                handleRequestFailure(response, reason);
            }
        },getSearchParams());
    }


    private void showCompanies(){
        DataProvider.get().requestCompanies(new RequestListener<List<Company>>() {
            @Override
            public void success(List<Company> companies) {
                showCompaniesFragment(companies);
            }
            public void failure(Object response, Throwable reason) {
                showNoDataView();
                handleRequestFailure(response, reason);
            }
        }, getSearchParams());

    }

    private void showCompaniesFragment(List<Company> companies){
        if(companies == null || companies.isEmpty())showNoDataView();
        else setFragment(CompanyFragment.newInstance(companies));

    }
    private void showMenuItemsFragment(List<hr.nss.eaton.model.menu.MenuItem> menuItems){
        if(menuItems == null || menuItems.isEmpty())
            showNoDataView();
        else
            setFragment(MenuItemFragment.newInstance(menuItems));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        final SearchView search = (SearchView)toolbar.getMenu().findItem(R.id.search_bar_search).getActionView();

        if (getSearchParams().get(User.SEARCH_PARAM) != null) {
            search.onActionViewExpanded();
            search.setQuery(getSearchParams().get(User.SEARCH_PARAM).toString(), false);

        }

        int width = toolbar.getMenu().findItem(R.id.search_bar_search).getIcon().getIntrinsicWidth();
        search.setMaxWidth(toolbar.getWidth() - 7*width);

        search.setSubmitButtonEnabled(true);

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                 setSearchParam(User.SEARCH_PARAM, query);
                search.clearFocus();
                query();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });

        search.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b && (search.getQuery() == null || search.getQuery().toString().isEmpty())){
                    setSearchParam(User.SEARCH_PARAM, null);
                    query();
                    search.onActionViewCollapsed();
                }
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.search_bar_map) {

            LocationsOnMapActivity.show(this);
            return true;

        }else if (id == R.id.search_bar_tune) {
            editSearchParams();
            return true;

        }else if (id == R.id.search_bar_search) {
            showToast("Action search clicked");
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    public void editSearchParams() {
        DialogFragment newFragment = new SearchParamsDialogFragment();
        newFragment.show(getSupportFragmentManager(), "search_params");
    }


    @Override
    protected int getFragmentContainer() {
        return R.id.search_fragment_container;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return onNavigationItemSelected(item.getItemId());
    }

    private boolean onNavigationItemSelected(int id){
        switch (id) {
            case R.id.search_navigation_delivery_only:
                setSearchParam(User.SEARCH_PARAM_DELIVERY_MANDATORY, true);
                showMenuItems();
                return true;

            case R.id.search_navigation_all_places:
                setSearchParam(User.SEARCH_PARAM_DELIVERY_MANDATORY, false);
                showMenuItems();
                return true;

            case R.id.search_navigation_companies:
                showCompanies();
                return true;
        }
        return false;

    }

    @Override
    public void dataChanged(List newData) {
        super.dataChanged(newData);
        if(user != null)setUserParams(user);
        query();
    }

    private void query(){
        onNavigationItemSelected(navigation.getSelectedItemId());
    }

}
