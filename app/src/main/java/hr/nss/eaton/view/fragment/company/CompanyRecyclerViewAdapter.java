package hr.nss.eaton.view.fragment.company;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import hr.nss.eaton.R;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.view.fragment.company.CompanyFragment.OnListFragmentInteractionListener;
import hr.nss.eaton.view.utils.ComponentUtils;

import java.util.List;

public class CompanyRecyclerViewAdapter extends RecyclerView.Adapter<CompanyRecyclerViewAdapter.ViewHolder> {

    private final List<Company> mValues;
    private final OnListFragmentInteractionListener mListener;

    public CompanyRecyclerViewAdapter(List<Company> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_company, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Company c = mValues.get(position);

        holder.mItem = c;
        holder.mNameView.setText(c.getName());
        holder.mDescriptionView.setText(c.getDescription());
        /*
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK) - 1; // -1 jer zapocinje sa nedjeljom
        if(day == 0) day = 7;

        holder.mWorkingHoursiew.setText(c.getOpeningHours().getForDay(day).getTimeRange());
        */
        holder.mWorkingHoursiew.setText(c.getForToday().getTimeRange());

        ComponentUtils.setImageView(holder.mPhotoView, c.getPhoto());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });

        holder.mWorkingHoursiew.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mListener.onWorkingHoursInteraction(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final TextView mDescriptionView;
        public final Button mWorkingHoursiew;
        public final ImageView mPhotoView;
        public Company mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = view.findViewById(R.id.company_item_header);
            mDescriptionView = view.findViewById(R.id.company_item_description);
            mWorkingHoursiew = view.findViewById(R.id.company_item_working_hours);
            mPhotoView = view.findViewById(R.id.company_item_photo);
        }

    }
}
