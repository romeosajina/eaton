package hr.nss.eaton.view.fragment.order;


import android.content.Context;

import java.util.List;

import hr.nss.eaton.R;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.view.fragment.base.EatOnFragment;

/**
 * Created by Robert on 30-May-18.
 */

public class OrderListFragment extends EatOnFragment<Order,OrderRecyclerViewAdapter > {

    private OrderActionListener orderActionListener;
    public OrderListFragment(){
        super(R.layout.fragment_order_item_list, R.layout.fragment_order_item);
    }

    public static OrderListFragment newInstance(List<Order> orders) {
        return newInstance(OrderListFragment.class, orders);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OrderActionListener) {
            orderActionListener = (OrderActionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        orderActionListener = null;
    }

    @Override
    protected void onCreateAdapter() {
        adapter.setOrderActionListener(orderActionListener);
    }

    public interface OrderActionListener {
        boolean onOrderAction(Order order);
    }

}
