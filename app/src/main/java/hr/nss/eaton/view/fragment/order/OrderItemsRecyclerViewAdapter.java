package hr.nss.eaton.view.fragment.order;


import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import hr.nss.eaton.R;
import hr.nss.eaton.model.menu.MenuItemOptionSupplement;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.transaction.OrderItem;
import hr.nss.eaton.model.transaction.OrderItemSupplement;
import hr.nss.eaton.view.fragment.base.EatOnAdapter;
import hr.nss.eaton.view.utils.FormatUtils;

/**
 * Created by Robert on 30-May-18.
 */

public class OrderItemsRecyclerViewAdapter extends EatOnAdapter<OrderItem,OrderItemsRecyclerViewAdapter.ViewHolder> {


    private String buildSupplementsInfo(OrderItem orderItem){
        String info = "";

        if (orderItem.getMenuItemOption() != null && orderItem.getMenuItem().getMenuItemOptions().size() > 1) {
            info += getString(R.string.option) + ": " + orderItem.getMenuItemOption().getName() + "\n";
        }
        if (orderItem.hasOrderItemSupplements()) {
            info += getString(R.string.supplements) + "\n";

            for (OrderItemSupplement orderItemSupplement : orderItem.getOrderItemSupplements()) {

                MenuItemOptionSupplement mios = orderItemSupplement.getMenuItemOptionSupplement();

                info += "   " + mios.getName() + " (+" + FormatUtils.format(mios.getPrice()) + ")\n";

            }
        }
        return info.trim();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mOrderItem = data.get(position);
        final OrderItem orderItem = holder.mOrderItem;

        holder.mItemNameView.setText(orderItem.getAmount() + "x " + orderItem.getMenuItem().getName());

        holder.mSupplementsView.setText(buildSupplementsInfo(orderItem));

        holder.mItemPriceView.setText(FormatUtils.format(orderItem.getTotal()));

        bindImage(holder.mItemImageView, orderItem.getMenuItem().getPhoto());

        holder.mRemoveView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Order o = orderItem.getOrder();
                o.removeOrderItem(orderItem);
                notifyDataChanged();
            }
        });
        holder.mRemoveView.setVisibility(orderItem.getOrder().canRemoveOrderItem() ? View.VISIBLE : View.GONE);


        if (null != mListener) {
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onListItemClick(holder.mOrderItem);
                }
            });
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mItemNameView;
        public final TextView mItemPriceView;
        public final ImageView mItemImageView;
        public final TextView mSupplementsView;
        public final View mRemoveView;
        public OrderItem mOrderItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mItemNameView = view.findViewById(R.id.fragment_order_detail_item_name);
            mItemPriceView = view.findViewById(R.id.fragment_order_detail_item_price);
            mItemImageView = view.findViewById(R.id.fragment_order_detail_item_img);
            mSupplementsView = view.findViewById(R.id.fragment_order_detail_item_supplements);
            mRemoveView = view.findViewById(R.id.fragment_order_detail_item_remove_btn);
        }

    }
}
