package hr.nss.eaton.view.fragment.menuitem;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import hr.nss.eaton.R;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.transaction.OrderItem;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.fragment.base.EatOnAdapter;


public class MenuItemRecyclerViewAdapter extends EatOnAdapter<MenuItem, MenuItemRecyclerViewAdapter.ViewHolder> {

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final MenuItem menuItem = data.get(position);

        holder.mMenuItem = menuItem;
        holder.mHeaderView.setText(menuItem.getName());
        holder.mDescriptionView.setText(menuItem.getDescription());
        holder.mPriceView.setText(menuItem.getPrice()+" "+getString(R.string.currency));


        bindImage(holder.mImageView, menuItem.getPhoto());

        OrderItem orderItem = User.get().findNewOrderItemByMenuItemId(menuItem.getId());
        holder.mOrderItem  = orderItem;
        setNumberOfOrders(holder);


        if (null != mListener) {
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        mListener.onListItemClick(holder.mMenuItem);
                }
            });
        }
    }

    private void setNumberOfOrders(ViewHolder holder){
     //   OrderItem orderItem = holder.mOrderItem;

     //   FormatUtils.formatAmount(holder.mNumberOfOrderedView, orderItem != null ? orderItem.getAmount() : 0);
        /*
        if(orderItem != null && orderItem.getAmount() > 0){

            holder.mNumberOfOrderedView.setText(orderItem.getAmount() > 9 ? orderItem.getAmount()+"" : "0"+orderItem.getAmount());
            holder.mNumberOfOrderedView.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorPrimary));

        }else{

            holder.mNumberOfOrderedView.setText("00");
            holder.mNumberOfOrderedView.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorSecondary));
        }
         */

    }
/*
    @Override
    public int getItemCount() {
        return mValues.size();
    }
    */

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView mHeaderView;
        public final ImageView mImageView;
        public final TextView mDescriptionView;
        public final TextView mPriceView;
        //public final TextView mNumberOfOrderedView;
        //     public final ImageButton mAddButton;
        //public final ImageButton mRemoveButton;
        public MenuItem mMenuItem;
        public OrderItem mOrderItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mHeaderView = view.findViewById(R.id.menu_item_header);
            mImageView = view.findViewById(R.id.menu_item_image);
            mDescriptionView = view.findViewById(R.id.menu_item_description);
            mPriceView = view.findViewById(R.id.menu_item_price);
            // mNumberOfOrderedView = view.findViewById(R.id.menu_item_number_of_ordered);
           // mAddButton = view.findViewById(R.id.add_menu_item);
          //  mRemoveButton = view.findViewById(R.id.remove_menu_item);
        }

    }
}
