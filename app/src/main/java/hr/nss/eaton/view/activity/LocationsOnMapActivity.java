package hr.nss.eaton.view.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.ui.IconGenerator;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.nss.eaton.R;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.data.RequestListener;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.activity.base.EatOnLocationProviderActivity;

public class LocationsOnMapActivity extends EatOnLocationProviderActivity implements OnMapReadyCallback {//LocationListener

    private GoogleMap mMap;
    //private LocationManager mLocationManager;
    private Marker currentLocation;

    private List<Marker> markers = new ArrayList<>();


    private static final long LOCATION_REFRESH_TIME = 3000;//milisecunds
    private static final long LOCATION_REFRESH_DISTANCE = 30;//meters
    private static final byte COMPANY_TAG = 1;
    private static final byte USER_CURRENT_TAG = 2;
    private static final byte USER_TAG = 3;

    private Map<String,Company> companyMarkers = new HashMap<>();

//    public static final int EATON_PERMISSIONS_REQUEST_LOCATION = 100;

    public static void show(Context context){
        Intent i = new Intent(context, LocationsOnMapActivity.class);
        context.startActivity(i);
    }

    @Override
    protected void onEatOnCreate(Bundle savedInstanceState) {
        super.onEatOnCreate(savedInstanceState);
        setContentView(R.layout.activity_locations_on_map);

        MapsInitializer.initialize(getApplicationContext());
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initLocationProvider();
        enableBackButton();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        requestUser(new RequestListener<User>() {
            @Override
            public void success(User user) {
                final LatLng uPos = new LatLng(user.getLatitude(), user.getLongitude());

                MarkerOptions userMarker = new MarkerOptions();
                userMarker.position(uPos);
                userMarker.title(getString(R.string.your_saved_location));
                userMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                //BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_person_pin);
                //userMarker.icon(icon);

                Marker marker = addMarker(userMarker);
                marker.setTag(USER_TAG);
                marker.showInfoWindow();

                moveCamera(uPos);

                DataProvider.get().requestCompanies(new RequestListener<List<Company>>() {
                    @Override
                    public void success(List<Company> companies) {

                        for(Company c : companies){

                            MarkerOptions mo = new MarkerOptions();
                            mo.position(new LatLng(c.getLatitude(), c.getLongitude()));
                            mo.title(c.getName());

                            Marker marker = addMarker(mo);
                            marker.setTag(COMPANY_TAG);
                            companyMarkers.put(marker.getId(), c);
                        }
                        moveCamera(uPos);
                    }

                    @Override
                    public void failure(Object response, Throwable reason) {
                        handleRequestFailure(response,reason);
                    }
                },user.getSearchParams());
            }

            @Override
            public void failure(Object response, Throwable reason) {
                handleRequestFailure(response, reason);
            }
        });



        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if((byte)marker.getTag() == COMPANY_TAG){

                    Company c = companyMarkers.get(marker.getId());
                    if(c!=null)
                        CompanyDetailsActivity.show(LocationsOnMapActivity.this, c);
                }
            }
        });
        ;
    }

    private Marker addMarker(MarkerOptions markerOptions){
        if(mMap != null){

            if(markerOptions.getIcon() == null){

                final TextView textView = new TextView(this);
                textView.setBackgroundResource(R.drawable.bubble_shape);
                textView.setText(markerOptions.getTitle());
                textView.setTextColor(getResources().getColor(R.color.colorPrimaryDark));


                ImageView imageView = new ImageView(this);
                imageView.setImageResource(R.drawable.restaurant);

                LinearLayout layout = new LinearLayout(this);
                layout.setOrientation(LinearLayout.VERTICAL);
                layout.addView(textView);
                layout.addView(imageView);

                IconGenerator generator = new IconGenerator(this);
                generator.setContentView(layout);
                generator.setBackground(null);

                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(generator.makeIcon()));
                markerOptions.title(getString(R.string.show_company_details));
            }

            Marker marker = mMap.addMarker(markerOptions);
          //  marker.showInfoWindow();

            markers.add(marker);
            return marker;
        }
        return null;
    }
    private void removeMarker(Marker marker){
        if(marker != null){
            markers.remove(marker);
            marker.remove();
            if((byte)marker.getTag() == COMPANY_TAG){
                companyMarkers.remove(marker.getId());
            }
        }
    }
    private void moveCamera(final LatLng pos){
        if(mMap == null)return;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();


        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
         //CameraUpdateFactory.newLatLngBounds(bounds, padding)
        mMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {focus(pos); }
            @Override
            public void onCancel() {focus(pos);}
        });

    }

    private void focus(final LatLng pos){

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    CameraUpdate cu = CameraUpdateFactory.newLatLng(pos);
                    mMap.animateCamera(cu);
                }
            },2000);

    }


    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);

        if(location != null && mMap != null){
            if(currentLocation != null){
                currentLocation.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
                if (currentLocation.isInfoWindowShown()) {
                   focus(currentLocation.getPosition());
                }
                return;
            }

          //  Toast.makeText(getApplicationContext(), "onLocationChanged: "+location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_LONG).show();

            MarkerOptions userMarker = new MarkerOptions();
            userMarker.position(new LatLng(location.getLatitude(), location.getLongitude()));
            userMarker.title(getString(R.string.your_location));
            userMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

            currentLocation = addMarker(userMarker);
            currentLocation.setTag(USER_CURRENT_TAG);
            currentLocation.showInfoWindow();

            moveCamera(currentLocation.getPosition());
        }
    }

}
