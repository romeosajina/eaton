package hr.nss.eaton.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedHashMap;
import java.util.List;

import hr.nss.eaton.R;
import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.data.RequestListener;
import hr.nss.eaton.model.menu.Menu;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.activity.base.EatOnActivity;
import hr.nss.eaton.view.base.EatOnBaseExpandableListAdapter;
import hr.nss.eaton.view.dialog.WorkingHoursDialogFragment;
import hr.nss.eaton.view.utils.ComponentUtils;
import hr.nss.eaton.view.utils.FormatUtils;

public class CompanyDetailsActivity extends EatOnActivity {

    public static final String COMPANY_ID = "company_id";

    private Company company;

    public static void show(Context context, Company company){
        Intent i = new Intent(context, CompanyDetailsActivity.class);
        i.putExtra(CompanyDetailsActivity.COMPANY_ID, company.getId());
        context.startActivity(i);
    }


    @Override
    protected void onEatOnCreate(Bundle savedInstanceState) {
        super.onEatOnCreate(savedInstanceState);
        setContentView(R.layout.activity_company_details);

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        enableBackButton();

        Long id = resolveParam(COMPANY_ID, savedInstanceState);

        if(id != null){
            DataProvider.get().requestCompany(new RequestListener<Company>() {
                @Override
                public void success(Company company) {
                    CompanyDetailsActivity.this.company = company;
                    showDetails();
                    showMenus();
                }


                public void failure(Object response, Throwable reason) {
                    showNoDataView();
                    handleRequestFailure(response, reason);
                }

            }, id);

        }

    }

    private void addToRecentCompanies(){
        User user = User.get();
        if(user != null){
            user.addRecentCompany(company);
            setRecentCompanies(user.getRecentCompanies());
        }
    }
    private void showDetails(){
        addToRecentCompanies();

        setTextView(R.id.company_details_name, company.getName());
        setTextView(R.id.company_details_address, company.getAddress());
        setTextView(R.id.company_details_description, company.getDescription());
        setImageView(R.id.company_details_photo, company.getPhoto(), true);

        setTextView(R.id.company_details_working_hours, company.getForToday().getTimeRange(), v -> showWorkingHours(company));

    }


    private LinkedHashMap<Menu, List<MenuItem>> menusToMap() {
        LinkedHashMap<Menu, List<MenuItem>> map = new LinkedHashMap<>();
        // company bi trebala imat menu-e, ovo bi trebalo bit samo za testiranje
        if(company.getMenus() != null){
            for (Menu menu : company.getMenus()) {
                map.put(menu, menu.getMenuItems());
            }
        }
        return map;
    }


    private void showMenus(){

        ExpandableListView expandableListView = findViewById(R.id.activity_menu_item_details_menu_list);
        expandableListView.setAdapter(new MenuExpandableListAdapter());
        expandableListView.setOnChildClickListener((expandableListView1, view, groupPos, childPos, id) -> {
            MenuItemDetailsActivity.show(CompanyDetailsActivity.this,
                    (MenuItem) expandableListView1.getExpandableListAdapter().getChild(groupPos, childPos));

            return true;
        });

    }

    private class MenuExpandableListAdapter extends EatOnBaseExpandableListAdapter<Menu, MenuItem> {

        public MenuExpandableListAdapter() {
            super(CompanyDetailsActivity.this.getApplicationContext(), CompanyDetailsActivity.this.menusToMap(),
                    R.layout.fragment_menu, R.layout.fragment_menu_item);
        }

        @Override
        protected void bindGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup viewGroup) {
            Menu menu = getGroup(groupPosition);
            EatOnImage photo = menu.getPhoto();
            if(photo == null || !photo.hasURL()){
                for (MenuItem menuItem : menu.getMenuItems()) {
                    if(menuItem.getPhoto() != null && menuItem.getPhoto().hasURL()){
                        photo = menuItem.getPhoto();
                        break;
                    }
                }
            }
            ComponentUtils.setImageView((ImageView)view.findViewById(R.id.fragment_menu_img),photo);
            ((TextView) view.findViewById(R.id.fragment_menu_name)).setText(menu.getType().name());
        }

        @Override
        protected void bindChildView(int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup viewGroup) {
            MenuItem menuItem = getChild(groupPosition, childPosition);

            ComponentUtils.setImageView(view.findViewById(R.id.menu_item_image), menuItem.getPhoto());

            ((TextView) view.findViewById(R.id.menu_item_header)).setText(menuItem.getName());
            ((TextView) view.findViewById(R.id.menu_item_description)).setText(menuItem.getDescription());
            ((TextView) view.findViewById(R.id.menu_item_price)).setText(FormatUtils.format(menuItem.getPrice()));

        }
    }

    @Override
    protected int getFragmentContainer() { return R.id.company_details_items_container; }

    private void showWorkingHours(Company company){
        WorkingHoursDialogFragment dialog = WorkingHoursDialogFragment.newInstance(company);
        dialog.show(getSupportFragmentManager(), "working_hours");
    }
}
