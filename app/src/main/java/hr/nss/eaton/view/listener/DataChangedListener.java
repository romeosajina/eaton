package hr.nss.eaton.view.listener;

import java.util.List;

/**
 * Created by Robert on 31-May-18.
 */

public interface DataChangedListener<E> {
    void dataChanged(List<E> newData);
}
