package hr.nss.eaton.view.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Switch;


import hr.nss.eaton.R;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.data.RequestListener;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.activity.base.EatOnLocationProviderActivity;


public class LoginActivity extends EatOnLocationProviderActivity {

    private EditText lEmailView;
    private EditText lPasswordView;


    private EditText sNameView;
    private EditText sSurnameView;
    private EditText sEmailView;
    private EditText sCityView;
    private EditText sAddressView;
    private EditText sPhoneView;
    private EditText sPassword1View;
    private EditText sPassword2View;
    private EditText sLocationView;

    private Switch   sShowPhoneView;


    private View loginView;
    private View signinView;



    public static void show(Context context){
        Intent i = new Intent(context, LoginActivity.class);
        context.startActivity(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    protected boolean checkUserLoggedIn() {
        //Login aktivnost ne provjerava dali je korisnik logiran
        return true;
    }

    @Override
    protected void onEatOnCreate(Bundle savedInstanceState) {
        super.onEatOnCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginView = findViewById(R.id.activity_login_login_form);
        lEmailView = findViewById(R.id.activity_login_email);
        lPasswordView = findViewById(R.id.activity_login_password);


        signinView = findViewById(R.id.activity_login_sign_in_form);
        sNameView = findViewById(R.id.activity_login_sign_in_name);
        sSurnameView = findViewById(R.id.activity_login_sign_in_surname);
        sEmailView = findViewById(R.id.activity_login_sign_in_email);
        sCityView = findViewById(R.id.activity_login_sign_in_city);
        sAddressView = findViewById(R.id.activity_login_sign_in_address);
        sLocationView = findViewById(R.id.activity_logint_sign_in_location);
        sPhoneView = findViewById(R.id.activity_login_sign_in_phone);
        sPassword1View = findViewById(R.id.activity_login_sign_in_password1);
        sPassword2View = findViewById(R.id.activity_login_sign_in_password2);
        sShowPhoneView = findViewById(R.id.activity_login_sign_in_show_number);

        findViewById(R.id.activity_login_log_in_btn).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        findViewById(R.id.activity_login_sign_in_btn).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSignin();
            }
        });
        findViewById(R.id.activity_login_go_to_sign_in_btn).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                changeVisibility(loginView, signinView);
            }
        });
        findViewById(R.id.activity_login_back_to_login_btn).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                changeVisibility(loginView, signinView);
            }
        });



        loginView.setVisibility(View.GONE);
        changeVisibility(loginView, signinView);

    }

    private void changeVisibility(final View v1, View v2){

        int v1Vis = v1.getVisibility();
        v1.setVisibility(v2.getVisibility());
        v2.setVisibility(v1Vis);

        int shortAnimTime = getResources().getInteger(android.R.integer.config_mediumAnimTime);
        v1.animate().setDuration(shortAnimTime).alpha(v1.getVisibility() == View.VISIBLE ? 1 : 0);
        v2.animate().setDuration(shortAnimTime).alpha(v2.getVisibility() == View.VISIBLE ? 1 : 0);

    }

    private boolean validateLoginFields(){

        // validacije se pozivaju sljedno i cim prva validacija padne ostale se ne pozivaju
        // zbog && konjukcije
        return validateRequiredField(lEmailView) &&
                validateRequiredField(lPasswordView) &&
                validateField(lEmailView, isEmailValid(lEmailView.getText().toString()), R.string.invalid_email);
    }

    private boolean validateSigninFields(){
        return
                validateRequiredField(sNameView) &&
                        validateRequiredField(sSurnameView) &&
                        validateRequiredField(sEmailView) &&
                        validateRequiredField(sCityView) &&
                        validateRequiredField(sAddressView) &&
                        validateRequiredField(sLocationView) &&
                        validateRequiredField(sPhoneView) &&
                        validateRequiredField(sPassword1View) &&
                        validateRequiredField(sPassword2View) &&
                        validateField(sEmailView, isEmailValid(sEmailView.getText().toString()), R.string.invalid_email) &&
                        validateField(sLocationView, isLocationValid(sLocationView.getText().toString()), R.string.invalid_location) &&
                        validateField(sPassword1View, sPassword1View.getText().toString().equals(sPassword2View.getText().toString()), R.string.password_mismatch) &&
                        validateField(sPassword1View, isPasswordValid(sPassword1View.getText().toString()), R.string.password_to_short);
    }


    private void attemptLogin() {

        if(!validateLoginFields())return;

        showProgressDialog(R.string.logging_in);

        DataProvider.get().requestUser(new RequestListener<User>() {
            @Override
            public void success(User user) {
                dismissProgressDialog();
                if (user == null) {
                    lEmailView.setError(getString(R.string.incorrect_email_or_password));
                    lPasswordView.setError(getString(R.string.incorrect_email_or_password));
                } else {
                    setCredentials(user);
                    //  EatOnObject.log(user+" " + getUserEmail()+" " + getUserPassword());
                    goToMainActivity();
                }
            }

            public void failure(Object response, Throwable reason) {
                dismissProgressDialog();
                handleRequestFailure(response, reason);
            }
        }, lEmailView.getText().toString(), encrypt(lPasswordView.getText().toString()));

    }
    private void goToMainActivity(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void attemptSignin(){
        sEmailView.setError(null);

        if(!validateSigninFields()) return;
        showProgressDialog(R.string.signing_in);

        User u = new User();

        u.setName(sNameView.getText().toString());
        u.setSurname(sSurnameView.getText().toString());
        u.setEmail(sEmailView.getText().toString());
        u.setCity(sCityView.getText().toString());
        u.setAddress(sAddressView.getText().toString());

        String loc[] = sLocationView.getText().toString().split(", ");
        u.setLatitude(Double.parseDouble(loc[0]));
        u.setLongitude(Double.parseDouble(loc[1]));

        u.setPhone(sPhoneView .getText().toString());
        u.setPassword(encrypt(sPassword1View.getText().toString()));
        u.setShowNumber(sShowPhoneView.isChecked());


        DataProvider.get().registerUser(u, new RequestListener<User>() {
            @Override
            public void success(User user) {
                dismissProgressDialog();
                 if(user.getId() == 0){
                     sEmailView.setError(getString(R.string.email_already_in_use));
                     sEmailView.requestFocus();
                 }
                 else{
                     setCredentials(user);
                     goToMainActivity();
                 }
            }
            @Override
            public void failure(Object response, Throwable reason) {
                dismissProgressDialog();
                handleRequestFailure(response, reason);
            }
        });

    }

    private boolean isEmailValid(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }


    /*
void test(){
    sNameView.setText("robert");
    sSurnameView.setText("sajina");
    sEmailView.setText("romeo.sajina@gmail.com");
    sCityView.setText("pula");
    sAddressView.setText("address");
    sPassword1View.setText("aaaaa");
    sPassword2View.setText("aaaaa");
    sPhoneView.setText("098933212");
}*/

    public void onLocationClick(View view) {
        requestLocation();
        showNotification(R.string.searching_location_started);
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location != null){
            sLocationView.setText(location.getLatitude() + ", " + location.getLongitude());
        }
    }

    private String encrypt(String s){
        return DataProvider.encrypt(s);
    }


}

