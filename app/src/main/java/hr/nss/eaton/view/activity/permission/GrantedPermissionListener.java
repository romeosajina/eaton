package hr.nss.eaton.view.activity.permission;

public interface GrantedPermissionListener {
    void granted();
}
