package hr.nss.eaton.view.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import hr.nss.eaton.R;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.data.RequestListener;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.activity.base.EatOnLocationProviderActivity;
import hr.nss.eaton.view.payment.EtherPayment;
import hr.nss.eaton.view.payment.Payment;
import hr.nss.eaton.view.payment.PaymentListener;

public class SettingsActivity extends EatOnLocationProviderActivity {

    private EditText mNameView;
    private EditText mSurnameView;
    private EditText mCityView;
    private EditText mAddressView;
    private EditText mLocationView;
    private EditText mPhoneView;
    private Switch mShowNumberView;

    private EditText mEthAddressView;
    private View mGenerateEthAddress;
    private View mCopyEthAdressToClipboard;

    
    private User user;

    private Payment ethPayment;

    @Override
    protected void onEatOnCreate(Bundle savedInstanceState) {
        super.onEatOnCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setupToolbarAndBackButton();

        mNameView = findViewById(R.id.settings_name);
        mSurnameView = findViewById(R.id.settings_surname);
        mCityView = findViewById(R.id.settings_city);
        mAddressView = findViewById(R.id.settings_address);
        mLocationView = findViewById(R.id.settings_location);
        mPhoneView = findViewById(R.id.settings_phone);
        mShowNumberView = findViewById(R.id.settings_show_number);
        mEthAddressView = findViewById(R.id.activity_settings_eth_address);

        mCopyEthAdressToClipboard = findViewById(R.id.activity_settings_copy_address_to_clipboard);

        mCopyEthAdressToClipboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(getString(R.string.etherium_address), mEthAddressView.getText());
                clipboard.setPrimaryClip(clip);
                showToast(R.string.copied_to_clipboard);
            }
        });

        mGenerateEthAddress = findViewById(R.id.activity_settings_generate_eth_acc);
        mGenerateEthAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateEtheriumAccount();
            }
        });

        requestUser(new RequestListener<User>() {
            @Override
            public void success(User user) {
                showUserInfo(user);
            }

            @Override
            public void failure(Object response, Throwable reason) {
                handleRequestFailure(response, reason);
            }
        });


        ethPayment = EtherPayment.getInstance(this);
        ethPayment.loadCredentials(new PaymentListener() {
            @Override
            public void success() {
                setEthAddress();
            }
            @Override
            public void fail(Exception reason) {
                 etaAddressExist(false);
            }
        });
    }

    private void etaAddressExist(boolean exist){
        mGenerateEthAddress.setVisibility(exist ? View.GONE : View.VISIBLE);
        mCopyEthAdressToClipboard.setVisibility(exist ? View.VISIBLE : View.GONE);

    }
    private void setEthAddress(){
            mEthAddressView.setText(ethPayment.getAddress());
            etaAddressExist(true);
    }

    private void generateEtheriumAccount(){
        showNotification(R.string.creating_new_etherium_wallet);
        ethPayment.createCredentials(new PaymentListener() {
            @Override
            public void success() {
                setEthAddress();
            }
            @Override
            public void fail(Exception reason) {
                showNotification(getString(R.string.unable_to_create_etherium_wallet)
                        + ": " + reason.getMessage());
            }
        });

    }

    private void showUserInfo(User u){
    
        user = u;

        mNameView.setText(user.getName());
        mSurnameView.setText(user.getSurname());
        mCityView.setText(user.getCity());
        mAddressView.setText(user.getAddress());
        mLocationView.setText(user.getLatitude() + ", " + user.getLongitude());
        mPhoneView.setText(user.getPhone());
        mShowNumberView.setChecked(user.isShowNumber());
    }


    private boolean validateFields(){
        return
                validateRequiredField(mNameView) &&
                        validateRequiredField(mSurnameView) &&
                        validateRequiredField(mCityView) &&
                        validateRequiredField(mAddressView) &&
                        validateRequiredField(mLocationView) &&
                        validateRequiredField(mPhoneView) &&
                        validateField(mLocationView, isLocationValid(mLocationView.getText().toString()), R.string.invalid_location);
    }


    public void onOkClick(View view) {

        if(user == null)return;
        if(!validateFields())return;

        showProgressDialog(R.string.saving);


        user.setName(mNameView.getText().toString());
        user.setSurname(mSurnameView.getText().toString());
        user.setCity(mCityView.getText().toString());
        user.setAddress(mAddressView.getText().toString());

        String loc = mLocationView.getText().toString();
        loc = loc.replace(" ", "");
        String [] latlng = loc.split(",");

        user.setLatitude(Double.parseDouble(latlng[0]));
        user.setLongitude(Double.parseDouble(latlng[1]));

        user.setPhone(mPhoneView.getText().toString());
        user.setShowNumber(mShowNumberView.isChecked());

        DataProvider.get().updateUser(user, new RequestListener<User>() {
            @Override
            public void success(User user) {
                dismissProgressDialog();
                finish();
            }

            @Override
            public void failure(Object response, Throwable reason) {
                dismissProgressDialog();
                handleRequestFailure(response, reason);

            }
        });
    }

    public void onCancelClick(View view) {
        onBackPressed();
    }


    public void onLocationClick(View view) {

        requestLocation();
        showNotification(R.string.searching_location_started);
    }


    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        if(location != null){
            mLocationView.setText(location.getLatitude() + ", " + location.getLongitude());
        }
    }
}
