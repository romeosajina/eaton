package hr.nss.eaton.view.listener;

/**
 * Created by Robert on 04-Jan-19.
 */

public interface EatOnObjectListener<T> {
    void changed(T t);
}
