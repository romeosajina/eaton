package hr.nss.eaton.view.fragment.order;

import hr.nss.eaton.R;
import hr.nss.eaton.model.transaction.OrderItem;
import hr.nss.eaton.view.fragment.base.EatOnFragment;

import java.util.List;

public class OrderItemFragment extends EatOnFragment<OrderItem,OrderItemsRecyclerViewAdapter> {


    public OrderItemFragment() {
        super(R.layout.fragment_order_detail_item_list, R.layout.fragment_order_detail_item);
    }

    public static OrderItemFragment newInstance(List<OrderItem> data) {
        return newInstance(OrderItemFragment.class, data);
    }



}
