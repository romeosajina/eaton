package hr.nss.eaton.view.notification;

import android.os.Handler;
import android.os.Looper;

import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;

import java.util.ArrayList;
import java.util.List;

import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.view.listener.EatOnObjectListener;

public class NotificationPusher {

    //    private static final String APP_ID = "app_id";
    private static final String APP_KEY = "c0a53cea58f28c26b294";
    // private static final String APP_SECRET = "app_id";
    private static final String APP_CLUSTER = "eu";
    private static final String ORDER_CHANNEL = "user-orders-channel";
    private static final String ORDER_EVENT = "on-changed-event";

    private static Pusher pusher;


    private static List<EatOnObjectListener<Order>> orderlisteners;


    private NotificationPusher(){

    }
    public static boolean isConnected(){
        return pusher != null;
    }

    public static void connect() {
        ensurePusher();
    }

    private static void ensurePusher(){

        if(!isConnected()){

            PusherOptions options = new PusherOptions();
            options.setCluster(APP_CLUSTER);
            options.setEncrypted(true);

            pusher = new Pusher(APP_KEY, options);

            pusher.connect();

           // registerOrderListener();
        }
    }

    private static void runOnUiThread(Runnable runnable){
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    public static void bindOrdersChannel(int id, EatOnObjectListener<Order> listener) {
        if (pusher.getChannel(ORDER_CHANNEL) == null) {
            final Channel channel = pusher.subscribe(ORDER_CHANNEL+"-"+id);
            channel.bind(ORDER_EVENT, (channelName, eventName, data) -> {
                //  EatOnObject.loge(channelName + " " + eventName + " " + data);
                Order o = DataProvider.parseObject(data, Order.class);
                listener.changed(o);

                //handleOrderEvent((Order) DataProvider.parseObject(data, Order.class));
            });
        }

    }


    public static void registerOrderListener(EatOnObjectListener<Order> orderListener){
        if(orderlisteners == null) orderlisteners = new ArrayList<>();
        orderlisteners.add(orderListener);
    }

    public static void unregisterOrderListener(EatOnObjectListener<Order> orderListener) {
        if (orderlisteners != null) {
            orderlisteners.remove(orderListener);
        }
    }

    public static void callOrderListeners(final Order order){
        if(orderlisteners != null && !orderlisteners.isEmpty()){
            runOnUiThread(() -> {
                for (EatOnObjectListener<Order> orderlistener : orderlisteners) {
                    orderlistener.changed(order);
                }
            });
        }
    }

}

