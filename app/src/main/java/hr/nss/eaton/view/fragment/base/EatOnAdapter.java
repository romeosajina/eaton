package hr.nss.eaton.view.fragment.base;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;

import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.utils.ReflectionUtils;
import hr.nss.eaton.view.listener.DataChangedListener;
import hr.nss.eaton.view.listener.ListItemListener;
import hr.nss.eaton.view.utils.ComponentUtils;

/**
 * Created by Robert on 01-Nov-18.
 */

public abstract class EatOnAdapter<T  extends EatOnObject, V extends  RecyclerView.ViewHolder> extends  RecyclerView.Adapter<V>  {


    protected ListItemListener<T> mListener;
    protected DataChangedListener<T> dataChangedListener;

    protected List<T> data;

    private int inflateLayoutId;

    private Context context;
    public EatOnAdapter() {
    }

    public void setData(List<T> mValues) {
        if (mValues == null) mValues = Collections.emptyList();
        this.data = mValues;
        notifyDataChanged();
    }
    public void notifyDataChanged(){
        notifyDataSetChanged();
        if (dataChangedListener != null) dataChangedListener.dataChanged(data);

    }




    public ListItemListener<T> getItemListener() {
        return mListener;
    }

    public void setItemListener(ListItemListener<T> mListener) {
        this.mListener = mListener;
    }

    public DataChangedListener<T> getDataChangedListener() {
        return dataChangedListener;
    }

    public void setDataChangedListener(DataChangedListener<T> dataChangedListener) {
        this.dataChangedListener = dataChangedListener;
    }

    public List<T> getData() {
        return data;
    }

    public int getInflateLayoutId() {
        return inflateLayoutId;
    }

    public void setInflateLayoutId(int inflateLayoutId) {
        this.inflateLayoutId = inflateLayoutId;
    }



    @Override
    public V onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(inflateLayoutId, parent, false);

        context = parent.getContext();

        try {
            Class<V> viewHolderClass = (ReflectionUtils.getGenericClassArgumentType(getClass(), 1));
         //   EatOnObject.log("ViewHolderclass "+viewHolderClass+"  "+java.util.Arrays.toString( viewHolderClass.getConstructors()));
     //       return viewHolderClass.getConstructor(View.class).newInstance(view);
            return (V) viewHolderClass.getConstructors()[0].newInstance(this, view);
        } catch (InstantiationException | IllegalAccessException  | InvocationTargetException e) {
           EatOnObject.log("ViewHolderClass nema konstruktor "+getClass()+" " + e.getClass().getSimpleName()+"  "+e.getMessage());
        }
        return null;
    }

    @Override
    public abstract void onBindViewHolder(V holder, int position);



    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    protected String getString(int id){
        return context.getString(id);
    }

    protected void bindImage(ImageView imageView, EatOnImage eatOnImage){
        ComponentUtils.setImageView(imageView, eatOnImage);
    }

}
