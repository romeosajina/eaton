package hr.nss.eaton.view.payment;


import hr.nss.eaton.model.transaction.Order;

public interface Payment {

    boolean isConnected();

    void connect();

    boolean hasCredentials();

    void loadCredentials(PaymentListener paymentListener);

    void createCredentials(PaymentListener paymentListener);

    void saveCredentials(String privateKey, PaymentListener paymentListener);

    String getAddress();

    String currency();

    String convertToCurrency(Double num);

    String toCurrencyTotal(Order order);

    boolean hasFunds(Double value);

    void pay(Order order,PaymentListener paymentListener);

    boolean canCancelOrder(Order order);

    void cancel(Order order, PaymentListener paymentListener);
}
