package hr.nss.eaton.view.listener;

/**
 * Created by Robert on 31-May-18.
 */

public interface ListItemListener<E> {
    void onListItemClick(E o);
}
