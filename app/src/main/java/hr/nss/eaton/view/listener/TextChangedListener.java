package hr.nss.eaton.view.listener;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Robert on 13-Nov-18.
 */

public abstract class TextChangedListener implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable s) {
        this.onTextChanged();
    }

    public abstract void onTextChanged();
}