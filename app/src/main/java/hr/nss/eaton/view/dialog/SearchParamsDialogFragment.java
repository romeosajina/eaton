package hr.nss.eaton.view.dialog;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.warkiz.widget.IndicatorSeekBar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.nss.eaton.R;
import hr.nss.eaton.model.menu.MenuType;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.listener.DataChangedListener;

public class SearchParamsDialogFragment extends DialogFragment {

    private static final List<String> sortBys = Arrays.asList(User.SORT_BY_DELIVERY_TIME, User.SORT_BY_RECOMMENDED, User.SORT_BY_MARK, User.SORT_BY_PRICE);
    private static final List<String> foodTypes;// = Arrays.asList(User.FOOD_TYPE_PIZZA, User.FOOD_TYPE_ALL);

    static {
        foodTypes = new ArrayList<>();
        for (MenuType menuType : MenuType.values()) {
            foodTypes.add(menuType.name());
        }

    }


    private Spinner sortBySpinner;
    private Spinner foodTypeSpinner;
    private IndicatorSeekBar maxDistanceSeekBar;
    private IndicatorSeekBar maxDeliveryTimeSeekBar;
    //private Switch deliverySwitch;

    private DataChangedListener dataChangedListener;

    public SearchParamsDialogFragment() {
    }

    private void fillFoodTypes(){
        ArrayAdapter<String> foodTypeAdapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_item);
        foodTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        foodTypeAdapter.add(getString(R.string.all));
        foodTypeAdapter.addAll(foodTypes);

        foodTypeSpinner.setAdapter(foodTypeAdapter);
    }

    private String resolveFoodType(){
        String val = foodTypeSpinner.getSelectedItem().toString();

        if(getString(R.string.all).equals(val))return User.FOOD_TYPE_ALL;

        return val;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_search_params, null);

        sortBySpinner = view.findViewById(R.id.search_params_sort_by);
        foodTypeSpinner = view.findViewById(R.id.search_params_food_type);
        fillFoodTypes();

        maxDistanceSeekBar = view.findViewById(R.id.search_params_max_distance);
        maxDeliveryTimeSeekBar = view.findViewById(R.id.search_params_max_delivery_time);
        //deliverySwitch = view.findViewById(R.id.search_params_delivery_mantatory);

        User u = User.get();
        sortBySpinner.setSelection(sortBys.indexOf(u.getSearchParam(User.SEARCH_PARAM_SORT_BY)));
        foodTypeSpinner.setSelection(foodTypes.indexOf(u.getSearchParam(User.SEARCH_PARAM_FOOD_TYPE))+1);
        maxDistanceSeekBar.setProgress((float)u.getSearchParam(User.SEARCH_PARAM_MAX_DISTANCE)/1000);
        maxDeliveryTimeSeekBar.setProgress((float)u.getSearchParam(User.SEARCH_PARAM_MAX_DELIVERY_TIME));
        //deliverySwitch.setChecked((boolean)u.getSearchParam(User.SEARCH_PARAM_DELIVERY_MANDATORY));

        builder.setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        User u = User.get();
                        u.setSearchParam(User.SEARCH_PARAM_SORT_BY, sortBys.get(sortBySpinner.getSelectedItemPosition()));
                        u.setSearchParam(User.SEARCH_PARAM_FOOD_TYPE, resolveFoodType());
                        u.setSearchParam(User.SEARCH_PARAM_MAX_DISTANCE, maxDistanceSeekBar.getProgressFloat()*1000);
                        u.setSearchParam(User.SEARCH_PARAM_MAX_DELIVERY_TIME, maxDeliveryTimeSeekBar.getProgressFloat());
                        //u.setSearchParam(User.SEARCH_PARAM_DELIVERY_MANDATORY, deliverySwitch.isChecked());
                        if(dataChangedListener != null) dataChangedListener.dataChanged(null);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) { SearchParamsDialogFragment.this.getDialog().cancel(); }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof DataChangedListener){
            dataChangedListener = (DataChangedListener) context;
        }
    }

    @Override
    public void onDetach() {
        dataChangedListener = null;
        super.onDetach();
    }
}
