package hr.nss.eaton.view.payment;

public interface PaymentListener{
    void success();
    void fail(Exception reason);
}
