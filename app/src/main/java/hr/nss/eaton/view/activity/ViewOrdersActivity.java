package hr.nss.eaton.view.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import hr.nss.eaton.R;
import hr.nss.eaton.model.data.RequestListener;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.activity.base.EatOnActivity;
import hr.nss.eaton.view.fragment.order.OrderListFragment;
import hr.nss.eaton.view.listener.EatOnObjectListener;
import hr.nss.eaton.view.listener.ListItemListener;
import hr.nss.eaton.view.properties.OrderStatusProperties;

public class ViewOrdersActivity extends EatOnActivity implements ListItemListener<Order>,
                                                                 BottomNavigationView.OnNavigationItemSelectedListener,
                                                                 OrderListFragment.OrderActionListener,
        EatOnObjectListener<Order>{

    private OrderListFragment orderListFragment;
    private BottomNavigationView navigation;

    private User user;
    
    @Override
    protected void onEatOnCreate(Bundle savedInstanceState) {
        super.onEatOnCreate(savedInstanceState);
        setContentView(R.layout.activity_view_orders);

        setupToolbarAndBackButton();

        navigation = findViewById(R.id.orders_bottom_navigation);
        navigation.setOnNavigationItemSelectedListener(this);


        requestUser(new RequestListener<User>() {
            @Override
            public void success(User u) {
               user = u;
               showOrders(getPendingSorted());
            }

            @Override
            public void failure(Object response, Throwable reason) {
                handleRequestFailure(response, reason);
            }
        });

    }

    private List<Order> getPendingSorted() {
        if(user == null)return Collections.emptyList();
        List<Order> orders = user.getPendingOrders();
        Collections.sort(orders, (order, t1) -> {
            int diff = order.getStatus().getValue() - t1.getStatus().getValue();
            if(diff == 0){
                return t1.getDateTime().compareTo(order.getDateTime());
            }
            return diff;
        });
        return orders;

    }
    private List<Order> getCompletedSorted(){
        if(user == null)return Collections.emptyList();

        List<Order> orders = user.getCompletedOrders();
        Collections.sort(orders, (order, t1) -> order.getDateTime().compareTo(t1.getDateTime()));
        return orders;
    }
    private void showOrders(List<Order> orders) {
        if(orders == null || orders.isEmpty())showNoDataView();
        else{
            orderListFragment = OrderListFragment.newInstance(orders);
            setFragment(orderListFragment);
        }
    }



    @Override
    protected int getFragmentContainer() {
        return R.id.orders_activity_fragment_container;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_view_orders_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.activity_view_orders_menu_info: showStatusInfo();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showStatusInfo(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        int m  = 15;
        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(800, 100+2*m);
        LinearLayout.LayoutParams iParams = new LinearLayout.LayoutParams(lParams.height-2*m, lParams.height-2*m);
        iParams.setMargins(m,m,m,m);

        LinearLayout ly = new LinearLayout(this);
        ly.setOrientation(LinearLayout.VERTICAL);
        ly.setDividerPadding(20);

        for (OrderStatusProperties osp : OrderStatusProperties.values()) {

            LinearLayout row = new LinearLayout(this);
            row.setOrientation(LinearLayout.HORIZONTAL);
            row.setLayoutParams(lParams);

            ImageView iv = new ImageView(this);
            iv.setImageResource(osp.icon);
            iv.setColorFilter(osp.color);
            iv.setLayoutParams(iParams);

            TextView tw = new TextView(this);
            tw.setTextColor(getResources().getColor(R.color.colorSecondary));
            tw.setText(osp.text);
            tw.setTextSize(18);
            tw.setHeight(lParams.height);
            tw.setGravity(Gravity.CENTER_VERTICAL);


            row.addView(iv);
            row.addView(tw);

            ly.addView(row);
        }


        alertDialog.setView(ly);
        alertDialog.show();


    }

    @Override
    public void onListItemClick(Order o) {
        OrderDetailsActivity.show(this, o);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if(navigation.getSelectedItemId() == item.getItemId()) return false;

        return showOrders(item.getItemId());
    }

    private boolean showOrders(int id){
        switch (id) {
            case R.id.orders_navigation_pending:
                showOrders(getPendingSorted());
                return true;

            case R.id.orders_navigation_history:
                showOrders(getCompletedSorted());
                return true;
        }
        return false;
    }

    @Override
    public boolean onOrderAction(final Order order) {
        switch (order.getStatus()) {
            case INITIALIZED:
            case ORDERED:

                OrderDetailsActivity.removeOrder(this,order, new RequestListener<Order>() {

                    @Override
                    public void success(Order order) {
                        showNotification(R.string.order_removed);

                        List<Order> pending = getPendingSorted();
                        if(!pending.isEmpty())
                            orderListFragment.setData(pending);
                        else showNoDataView();
                    }

                    @Override
                    public void failure(Object response, Throwable reason) {
                        handleRequestFailure(response, reason);
                        if(response == null  && reason == null){
                            orderListFragment.notifyDataChanged();
                            showNotification(R.string.unable_to_delete_order_already_confirmed);
                        }
                    }
                });

                break;
            case DELIVERED:
                order.copyToNewOrders();
                // poziva se onSelectItem pa se kambijiva automatski u pregled pending
                navigation.setSelectedItemId(R.id.orders_navigation_pending);

                showNotification(R.string.order_created);
                break;
        }

        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerOrderListener(this);
        if (navigation != null) showOrders(navigation.getSelectedItemId());
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterOrderListener(this);
    }

    @Override
    public void changed(Order order) {
        showOrders(navigation.getSelectedItemId());
    }
}
