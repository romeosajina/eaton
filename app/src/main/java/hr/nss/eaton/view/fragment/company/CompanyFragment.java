package hr.nss.eaton.view.fragment.company;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hr.nss.eaton.R;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.view.activity.CompanyDetailsActivity;
import hr.nss.eaton.view.dialog.WorkingHoursDialogFragment;

import java.util.List;

public class CompanyFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;

    private List<Company> items;

    public CompanyFragment() {
    }

    @SuppressWarnings("unused")
    public static CompanyFragment newInstance(List<Company> items) {
        CompanyFragment fragment = new CompanyFragment();
     //   Bundle args = new Bundle();
    //    fragment.setArguments(args);

        fragment.items = items;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_company_list, container, false);

        mListener = new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(Company company) {
                CompanyDetailsActivity.show(getActivity(), company);

                /*
                Intent i = new Intent(getContext(), CompanyDetailsActivity.class);
                i.putExtra(CompanyDetailsActivity.COMPANY_ID, company.getId());
                startActivity(i);
                */

            }

            @Override
            public void onWorkingHoursInteraction(Company company) {
                showWorkingHours(company);
            }
        };

        if (view instanceof RecyclerView) {
            //Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            //recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new CompanyRecyclerViewAdapter(items, mListener));
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            //throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Company company);
        void onWorkingHoursInteraction(Company company);
    }

    private void showWorkingHours(Company company){
        WorkingHoursDialogFragment dialog = WorkingHoursDialogFragment.newInstance(company);
        dialog.show(getActivity().getSupportFragmentManager(), "working_hours");
    }

}
