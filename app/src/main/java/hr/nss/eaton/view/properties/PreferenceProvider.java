package hr.nss.eaton.view.properties;

import android.content.Context;
import android.content.SharedPreferences;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PreferenceProvider {


    private static SharedPreferences prefs;
    public static final String PREFS ="EatOn";


    private PreferenceProvider(){

    }

    public static void putInt(Context c, String key, int value){
        ensurePrefs(c);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key,value);
        editor.apply();
    }

    public static void putString(Context c, String key, String value){
        ensurePrefs(c);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key,value);
        editor.apply();
    }

    public static void ensurePrefs(Context c){
        if(prefs == null){
            prefs = c.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        }
    }
    public static String getString(Context c, String key){

        return getString(c,key,null);
    }
    public static String getString(Context c, String key, String defaultValue){
        ensurePrefs(c);
        return prefs.getString(key,defaultValue);
    }

    public static Float getFloat(Context c, String key){

        return getFloat(c,key,null);
    }
    public static Float getFloat(Context c, String key, Float defaultValue){
        ensurePrefs(c);
        return prefs.getFloat(key,defaultValue);
    }

    public static Integer getInt(Context c, String key){

        return getInt(c,key,0);
    }
    public static Integer getInt(Context c, String key, Integer defaultValue){
        ensurePrefs(c);
        if(!prefs.contains(key))return null;
        return prefs.getInt(key,defaultValue);
    }




    public static Boolean getBoolean(Context c, String key){

        return getBoolean(c,key,null);
    }
    public static Boolean getBoolean(Context c, String key, Boolean defaultValue){
        ensurePrefs(c);
        return prefs.getBoolean(key,defaultValue);
    }


    public static void put(Context c, Map<String, Object> map){
        ensurePrefs(c);
        SharedPreferences.Editor editor = prefs.edit();

        for (Map.Entry<String, Object> entry : map.entrySet()) {

            if(entry.getValue() instanceof Float)
                editor.putFloat(entry.getKey(), (Float) entry.getValue());
            else if(entry.getValue() instanceof Boolean)
                editor.putBoolean(entry.getKey(), (Boolean) entry.getValue());
            else editor.putString(entry.getKey(), (String) entry.getValue());

        }
        editor.apply();


    }

    public static void saveList(Context c, List list, String key){
        ensurePrefs(c);

        SharedPreferences.Editor editor = prefs.edit();
        if(list == null || list.isEmpty()){
            editor.remove(key);
        }
        else{
            Set<String> set = new LinkedHashSet<>();
            for (Object o : list) {
                set.add(String.valueOf(o));
            }
            editor.putStringSet(key, set);
        }

        editor.apply();
    }
    public static List getSavedList(Context c, String key, Class elementClass){
        ensurePrefs(c);

        Set<String> set = prefs.getStringSet(key, null);

        if(set == null || set.isEmpty()) return null;

        List list = new ArrayList();

        for (String s : set) {
            try {
                Object value = elementClass.getConstructor(String.class).newInstance(s);
                list.add(value);
            } catch (InstantiationException | IllegalAccessException |
                    InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return list;
    }



}
