package hr.nss.eaton.view.payment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.gas.DefaultGasProvider;
import org.web3j.utils.Convert;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.security.Provider;
import java.security.Security;
import java.util.concurrent.ExecutionException;


import androidx.annotation.NonNull;
import hr.nss.eaton.R;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.data.RequestListener;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.view.activity.permission.GrantedPermissionListener;
import hr.nss.eaton.view.activity.permission.PermissionUtils;
import hr.nss.eaton.view.notification.AndroidNotification;
import hr.nss.eaton.view.payment.contract.EatOnContract;
import hr.nss.eaton.view.properties.PreferenceProvider;
import hr.nss.eaton.view.utils.CurrencyAPI;
import hr.nss.eaton.view.utils.EtherscanAPI;
import hr.nss.eaton.view.utils.FormatUtils;

public class EtherPayment implements Payment {
// getDefaultKeyDirectory() getMainnetKeyDirectory() getRinkebyKeyDirectory() getTestnetKeyDirectory()
// /.ethereum /.ethereum/keystore /.ethereum/rinkeby/keystore /.ethereum/testnet/keystore

    private static final String WALLET_NAME = "wallet_name";
    private static final String NETWORK_URL = "https://ropsten.infura.io/v3/d2af14a34bd94ed2adfea234108d025e";
    private static final String PASSWORD = EtherPayment.class.getSimpleName();

    private static final String EAT_ON_CONTRACT_ADDRESS = "0x8a4d4404a3bc409e10a7dd7763287cc38857e0f0";

    private Context context;
    private Web3j web3;
    private Credentials credentials;
    private EatOnContract contract;


    private static EtherPayment instance;

    public static EtherPayment getInstance(Context c) {
        if (instance == null) instance = new EtherPayment(c);
        return instance;
    }

    private EtherPayment(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public boolean isConnected() {
        return web3 != null;
    }

    @Override
    public void connect() {
        if (isConnected()) return;
        web3 = Web3j.build(new HttpService(NETWORK_URL));
    }


    @Override
    public boolean hasCredentials() {
        return getFromPrefs(WALLET_NAME) != null;
    }


    private boolean credentialsLoaded() {
        return credentials != null;
    }

    @Override
    public String getAddress() {
        if (hasCredentials() && credentialsLoaded()) {
            EatOnObject.loge(credentials.getAddress());
            return credentials.getAddress();
        }
        return null;
    }

    @Override
    public String currency() {
        return context.getString(R.string.eth_currency);
    }

    private String weiCurrency() {
        return context.getString(R.string.wei_currency);
    }

    private BigDecimal hrToUsd(BigDecimal val) {
        return val.divide(USD_HRK, MATH_CTX);
    }

    private BigDecimal convertToEth(Double num) {
        BigDecimal val = BigDecimal.valueOf(num);

        BigDecimal usd = hrToUsd(val);

        BigDecimal value = usd.divide(ETHER_VALUE, MATH_CTX).multiply(BigDecimal.ONE);

        return value;
    }

    private BigDecimal weiToEth(BigInteger wei) {
        return Convert.fromWei(BigDecimal.valueOf(wei.longValue()), Convert.Unit.ETHER);
    }

    private BigInteger ethToWei(BigDecimal eth) {
        return Convert.toWei(eth, Convert.Unit.ETHER).toBigInteger();
    }

    private EatOnContract getContract(){
        if(contract == null)
            contract = EatOnContract.load(EAT_ON_CONTRACT_ADDRESS, web3, credentials, new DefaultGasProvider());
        return contract;
    }

    @Override
    public String convertToCurrency(Double num) {
        BigDecimal value = convertToEth(num);

        return FormatUtils.format(value) + " " + currency();
    }

    @Override
    public String toCurrencyTotal(Order order) {
        if(order.isNew()){
            return convertToCurrency(order.getTotal());
        }


        String companyAddress = order.getCompany().getEthAddress();
        BigInteger id = BigInteger.valueOf(order.getId());

        try {
            Boolean rewarded = getContract().isOrderRewarded(id, companyAddress)
                    .sendAsync()
                    .get();

            if(rewarded){
                return convertToCurrency(Double.valueOf(0));
            }

        } catch (Exception e) {
            EatOnObject.loge(e);
        }

        return convertToCurrency(order.getTotal());
    }

    @Override
    public boolean hasFunds(Double num) {
        BigDecimal value = convertToEth(num);
        try {

            BigInteger balance = web3.ethGetBalance(getAddress(), DefaultBlockParameterName.LATEST)
                    .sendAsync()
                    .get()
                    .getBalance()
                    .subtract(DefaultGasProvider.GAS_LIMIT);

            BigDecimal eth = weiToEth(balance);

            return eth.compareTo(value) >= 0;
        } catch (Exception e) {
            EatOnObject.loge(e);
        }

        return false;
    }


    @Override
    public void pay(Order order, PaymentListener paymentListener) {

        doAsync(() -> {
            BigDecimal value = convertToEth(order.getTotal());

            BigInteger wei = ethToWei(value);

            String companyAddress = order.getCompany().getEthAddress();
            BigInteger id = BigInteger.valueOf(order.getId());

            try {
                EatOnObject.loge("PAY: "+id +" " + companyAddress);
                RemoteCall<TransactionReceipt> placeOrder = getContract().placeOrder(id, companyAddress, wei);
                // placanje se radi asinkrono
                // ako korisnik nakon ca posalje ovu transakciju, posalje drugu s manjin iznoson ali
                // s boljin gas-on, moguce je overrideat ovu transakciju
                placeOrder.sendAsync().whenCompleteAsync((transactionReceipt, throwable) -> {
                    if(throwable != null)checkForRewardedOrder(order);
                });

                success(paymentListener);
            } catch (Exception e) {
                EatOnObject.loge("FAIL PAY: "+id +" " + companyAddress);
                fail(paymentListener,e);
            }
        });

    }
    private void checkForRewardedOrder(Order order){

        String companyAddress = order.getCompany().getEthAddress();
        BigInteger id = BigInteger.valueOf(order.getId());

        getContract().isOrderRewarded(id, companyAddress)
                .sendAsync()
                .whenCompleteAsync((rewarded, throwable) -> {
                    EatOnObject.loge("REWARDED ORDER: "+rewarded+" " + throwable);
                    if(throwable != null && rewarded){
                        runOnUiThread( () -> {
                            //NOTIFIY!
                            AndroidNotification.showOrderNotification(context, (int) order.getId(),
                                    order.getCompany().getName() + " " + context.getString(R.string.rewards),
                                    context.getString(R.string.free_order_won));
                        });
                    }
                });

    }
    @Override
    public boolean canCancelOrder(Order order) {

        BigInteger id = BigInteger.valueOf(order.getId());
        String companyAddress = order.getCompany().getEthAddress();

        try {
            return getContract().hasOrder(id, companyAddress).sendAsync().get();
        } catch (Exception e) {
            EatOnObject.loge(e);
        }
        return false;
    }

    @Override
    public void cancel(Order order, PaymentListener paymentListener) {

        doAsync(() -> {

            BigInteger id = BigInteger.valueOf(order.getId());
            String companyAddress = order.getCompany().getEthAddress();

            try {
                EatOnObject.loge("CANCEL: "+id +" " + companyAddress);
                RemoteCall<TransactionReceipt> cancelOrder = getContract().cancelOrder(id, companyAddress);
                // ponistavanje se ceka nakon ca se provjeri dali se narudzba nalazi na etherumu
                // ceka se da se korisniku prikaze dali je ili ni ponistena
                cancelOrder.sendAsync().get();

                success(paymentListener);
            } catch (Exception e) {
                EatOnObject.loge("FAIL CANCEL: "+id +" " + companyAddress);
                fail(paymentListener, e);
            }
        });




    }

    @Override
    public void loadCredentials(PaymentListener paymentListener) {
        if (hasCredentials() && credentialsLoaded()) {
            paymentListener.success();
            return;
        }

        requestPermission(() -> {
            try {
                credentials = WalletUtils.loadCredentials(PASSWORD, getFilePath());
                success(paymentListener);

                // EatOnObject.log("Credentrials "+credentials);
            } catch (IOException | CipherException e) {
                fail(paymentListener, e);
            }

        });
    }

    @Override
    public void createCredentials(PaymentListener paymentListener) {
        requestPermission(() -> {
            setupBouncyCastle();
            try {
                String fileName = WalletUtils.generateNewWalletFile(PASSWORD, ensureFile(getDirPath()), false);

                saveToPrefs(WALLET_NAME, fileName);

                credentials = WalletUtils.loadCredentials(PASSWORD, getFilePath());
                success(paymentListener);

                EatOnObject.log("Credentrials EatOnContract" + credentials);
            } catch (Exception e) {
                fail(paymentListener, e);
            }
        });
    }


    @Override
    public void saveCredentials(String privateKey, PaymentListener paymentListener) {
        if (!WalletUtils.isValidPrivateKey(privateKey)) {
            paymentListener.fail(new Exception("Invalid private key"));
            return;
        }
        ;
        requestPermission(() -> {
            setupBouncyCastle();

            try {
                Credentials c = Credentials.create(privateKey);
                WalletUtils.generateWalletFile(PASSWORD, c.getEcKeyPair(), ensureFile(getDirPath()), false);
                credentials = c;
                success(paymentListener);
            } catch (Exception e) {
                fail(paymentListener, e);
            }
        });
    }

    private void success(PaymentListener paymentListener) {
        runOnUiThread(() -> paymentListener.success());
    }

    private void fail(PaymentListener paymentListener, Exception e) {
        EatOnObject.loge(e);
        runOnUiThread(() -> paymentListener.fail(e));
    }

    private static void runOnUiThread(Runnable runnable) {
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    private void saveToPrefs(String key, String value) {
        PreferenceProvider.putString(context, key, value);

    }

    private String getFromPrefs(String key) {
        return PreferenceProvider.getString(context, key);
    }


    private String getDirPath() {
        return context.getApplicationInfo().dataDir + WalletUtils.getTestnetKeyDirectory();
    }

    private String getFilePath() {
        return getDirPath() + "/" + getFromPrefs(WALLET_NAME);
    }

    private void requestPermission(GrantedPermissionListener permissionListener) {

        PermissionUtils.requestPermissions((Activity) context,
                () -> doAsync(permissionListener),
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

    }

    private void doAsync(GrantedPermissionListener permissionListener) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                permissionListener.granted();
                return null;
            }
        }.execute();
    }

    private static void setupBouncyCastle() {

        final Provider provider = Security.getProvider(BouncyCastleProvider.PROVIDER_NAME);
        if (provider == null) {
            // Web3j will set up the provider lazily when it's first used.
            return;
        }
        if (provider.getClass().equals(BouncyCastleProvider.class)) {
            // BC with same package name, shouldn't happen in real life.
            return;
        }
        // Android registers its own BC provider. As it might be outdated and might not include
        // all needed ciphers, we substitute it with a known BC bundled in the app.
        // Android's BC has its package rewritten to "com.android.org.bouncycastle" and because
        // of that it's possible to have another BC implementation loaded in VM.
        Security.removeProvider(BouncyCastleProvider.PROVIDER_NAME);
        Security.insertProviderAt(new BouncyCastleProvider(), 1);
    }


    private static final File ensureFile(String path) {
        File destination = new File(path);

        if (!destination.exists()) {
            if (!destination.mkdirs()) {
                EatOnObject.loge("UNABLE TO CREATE DIRS");
            }
        }
        return destination;
    }


    private static BigDecimal ETHER_VALUE = BigDecimal.valueOf(100);
    private static BigDecimal USD_HRK = BigDecimal.valueOf(6.5);
    private static final MathContext MATH_CTX = new MathContext(20, RoundingMode.HALF_UP);

    public static void init() {
        getEtherPrice();
        getConversion();
    }

    private static void getEtherPrice() {
        EtherscanAPI.getInstance().getEtherPrice(new RequestListener<BigDecimal>() {
            @Override
            public void success(BigDecimal value) {
                ETHER_VALUE = value;
                EatOnObject.loge(ETHER_VALUE);
            }

            @Override
            public void failure(Object response, Throwable reason) {
                EatOnObject.loge(response + " " + reason);
                getEtherPrice();
            }
        });
    }

    private static void getConversion() {
        CurrencyAPI.getInstance().usdToHrk(new RequestListener<BigDecimal>() {
            @Override
            public void success(BigDecimal v) {
                USD_HRK = v;
                EatOnObject.loge("USD_HRK: " + USD_HRK);
            }

            @Override
            public void failure(Object response, Throwable reason) {
                EatOnObject.loge("Failed to get conversion " + response + " " + reason);
                   getConversion();
            }
        });
    }


}

