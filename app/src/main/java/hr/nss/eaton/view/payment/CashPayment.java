package hr.nss.eaton.view.payment;

import android.content.Context;

import hr.nss.eaton.R;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.view.utils.FormatUtils;

public class CashPayment implements Payment {

    private static CashPayment instance;
    public static CashPayment getInstance(Context c){
        if(instance == null) instance = new CashPayment(c);
        return instance;
    }

    private Context context;

    private CashPayment(Context c){
        context = c;
    }

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public void connect() {
    }

    @Override
    public boolean hasCredentials() {
        return true;
    }

    @Override
    public void loadCredentials(PaymentListener paymentListener) {
            paymentListener.success();
    }

    @Override
    public void createCredentials(PaymentListener paymentListener) {
        paymentListener.success();
    }

    @Override
    public void saveCredentials(String privateKey, PaymentListener paymentListener) {
        paymentListener.success();
    }

    @Override
    public String getAddress() {
        return null;
    }

    @Override
    public String currency() {
        return context.getString(R.string.currency);
    }

    @Override
    public String convertToCurrency(Double num) {
        return FormatUtils.format(num) + " " + currency();
    }

    @Override
    public String toCurrencyTotal(Order order) {
        return convertToCurrency(order.getTotal());
    }

    @Override
    public boolean hasFunds(Double value) {
        return true;
    }

    @Override
    public void pay(Order order, PaymentListener paymentListener) {
        paymentListener.success();
    }

    @Override
    public boolean canCancelOrder(Order order) {
        return true;
    }

    @Override
    public void cancel(Order order, PaymentListener paymentListener) {
        paymentListener.success();
    }

}
