package hr.nss.eaton.view.utils;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import java.util.Arrays;

import hr.nss.eaton.R;
import hr.nss.eaton.model.common.EatOnImage;

/**
 * Created by Robert on 01-Nov-18.
 */

public class ComponentUtils {

    public static void setImageView(final ImageView imageView, EatOnImage eatOnImage, final boolean canOpenInFullScreen) {


        imageView.setImageResource(R.drawable.no_image);
        if(eatOnImage == null) return;


        eatOnImage.loadImage(bitmap -> {
            imageView.setImageBitmap(bitmap);
            if(canOpenInFullScreen){
                setOpenInFullScreenListener(imageView,bitmap);
            }
        });
    }

    public static void setImageView(ImageView imageView, EatOnImage eatOnImage){
        setImageView(imageView, eatOnImage, false);
    }

    private static void setOpenInFullScreenListener(final ImageView imageView, final Bitmap bitmap){
        imageView.setOnClickListener(view -> new StfalconImageViewer.Builder<>(imageView.getContext(), Arrays.asList(bitmap), new ImageLoader<Bitmap>() {
            @Override
            public void loadImage(ImageView imageView1, Bitmap image) {
                imageView1.setImageBitmap(image);
            }
        }).allowSwipeToDismiss(true)
                .allowZooming(true)
                .show());

    }

}
