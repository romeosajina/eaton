package hr.nss.eaton.view.fragment.menuitem;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import hr.nss.eaton.R;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.view.activity.MenuItemDetailsActivity;
import hr.nss.eaton.view.fragment.base.EatOnFragment;
import hr.nss.eaton.view.listener.ListItemListener;


public class MenuItemFragment extends EatOnFragment<MenuItem, MenuItemRecyclerViewAdapter> {


    public MenuItemFragment() {
        super(R.layout.fragment_menu_item_list, R.layout.fragment_menu_item);

        mListener = new ListItemListener<MenuItem>() {
            @Override
            public void onListItemClick(MenuItem item) {
                MenuItemDetailsActivity.show(getActivity(), item);
            }
        };

    }

    public static MenuItemFragment newInstance(List<MenuItem> items) {
        return newInstance(MenuItemFragment.class, items);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        return null;
    }




}
