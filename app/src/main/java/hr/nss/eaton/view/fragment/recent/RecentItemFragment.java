package hr.nss.eaton.view.fragment.recent;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import hr.nss.eaton.R;
import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.view.activity.CompanyDetailsActivity;
import hr.nss.eaton.view.activity.MenuItemDetailsActivity;
import hr.nss.eaton.view.utils.ComponentUtils;

public class RecentItemFragment extends Fragment {

    private EatOnObject item;

    public RecentItemFragment() {
    }

    public static RecentItemFragment newInstance(EatOnObject o) {
        RecentItemFragment fragment = new RecentItemFragment();
        fragment.item = o;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recent_item, container, false);

        final ImageView imageView = view.findViewById(R.id.recent_item_image);
        TextView textView = view.findViewById(R.id.recent_item_text);

        String name = item instanceof MenuItem? ((MenuItem)item).getName() : ((Company)item).getName();
        textView.setText(name);

        EatOnImage image = item instanceof MenuItem? ((MenuItem)item).getPhoto() : ((Company)item).getPhoto();

        ComponentUtils.setImageView(imageView, image);

        View v = view.findViewById(R.id.recent_item_container);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(item instanceof MenuItem){
                    MenuItemDetailsActivity.show(getActivity(), (MenuItem)item);

                }else {
                    CompanyDetailsActivity.show(getActivity(), (Company)item);
                }
            }
        });

        return view;
    }


}
