package hr.nss.eaton.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import hr.nss.eaton.R;
import hr.nss.eaton.view.activity.OrderDetailsActivity;
import hr.nss.eaton.view.payment.Payment;
import hr.nss.eaton.view.properties.PaymentProperties;


public class ChoosePaymentTypeDialogFragment extends DialogFragment {

    private RadioGroup radioGroup;
    private int selectedPaymentMethodIndex;
    public ChoosePaymentTypeDialogFragment() {
    }
    public static ChoosePaymentTypeDialogFragment newInstance(Payment payment) {
        ChoosePaymentTypeDialogFragment fragment = new ChoosePaymentTypeDialogFragment();
        fragment.selectedPaymentMethodIndex = PaymentProperties.indexOf(payment);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_choose_payment_type, null);
        builder.setView(view);

        radioGroup = view.findViewById(R.id.dialog_choose_payment_type_types);
        addPaymentTypes();
        setDefaultPaymentType();

        builder.setCancelable(true);
        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> dismiss());
        builder.setPositiveButton(R.string.confirm, (dialogInterface, i) -> {
            //TODO: 2. DRUGAJCI NACIN OBAVJESTAVANJA AKTIVNOSTI
            ((OrderDetailsActivity) getActivity()).setPaymentProperties(PaymentProperties.at(getCheckedPaymentType()));
            dismiss();
        });
        return builder.create();
    }

    private void addPaymentTypes(){
        Context ctx = getActivity();
        RadioGroup.LayoutParams lp = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        for (PaymentProperties pp : PaymentProperties.getAll()) {
            RadioButton rb = new RadioButton(ctx);
            rb.setLayoutParams(lp);
            rb.setText(pp.nameId);

            radioGroup.addView(rb);
        }
    }
    private void setDefaultPaymentType(){
        radioGroup.check(radioGroup.getChildAt(selectedPaymentMethodIndex).getId());
    }
    private int getCheckedPaymentType(){
        return radioGroup.indexOfChild(radioGroup.findViewById(radioGroup.getCheckedRadioButtonId()));
    }

}
