package hr.nss.eaton.view.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.regex.Pattern;

import hr.nss.eaton.model.data.EatOnJsonHttpResponseHandler;
import hr.nss.eaton.model.data.HttpUtils;
import hr.nss.eaton.model.data.RequestListener;

public class CurrencyAPI {

    private static final String PATH = "https://free.currencyconverterapi.com/api/v6/convert?q=";
    private static final String COMPACT = "compact=y";


    private static CurrencyAPI instance;

    public static CurrencyAPI getInstance(){
        if(instance == null) instance = new CurrencyAPI();
        return instance;
    }

    private CurrencyAPI(){

    }

    public void usdToHrk(RequestListener<BigDecimal> requestListener){
        convert("USD", "HRK", requestListener);
    }

    private void convert(String from, String to, RequestListener<BigDecimal> requestListener){
        String key = from +"_"+to;
        HttpUtils.get2(PATH + key +"&"+COMPACT, new EatOnJsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    Double val = response.getJSONObject(key).getDouble("val");
                    requestListener.success(BigDecimal.valueOf(val));

                } catch (JSONException e) {
                    requestListener.failure(response, e);
                }
            }

            @Override
            public void onFailure(Object response, Throwable reason) {
                requestListener.failure(response,reason);
            }
        });
    }
}
