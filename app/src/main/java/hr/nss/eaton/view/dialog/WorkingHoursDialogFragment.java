package hr.nss.eaton.view.dialog;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import hr.nss.eaton.R;
import hr.nss.eaton.model.user.Company;


public class WorkingHoursDialogFragment extends DialogFragment {

     private Company company;
 //   private List<OpeningHour> openingHours;

    public static WorkingHoursDialogFragment newInstance(Company company){
        WorkingHoursDialogFragment whdf = new WorkingHoursDialogFragment();
      //  whdf.openingHours = openingHours;
        whdf.company = company;
        return whdf;
    }

    public WorkingHoursDialogFragment() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_working_hours, null);

        builder.setView(view).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        WorkingHoursDialogFragment.this.getDialog().cancel();
                    }
                });

        //builder.setCancelable(false);

        setWorkingHoursForParams(view, R.id.working_hours_monday, 1);
        setWorkingHoursForParams(view, R.id.working_hours_tuesday, 2);
        setWorkingHoursForParams(view, R.id.working_hours_wednesday, 3);
        setWorkingHoursForParams(view, R.id.working_hours_thursday, 4);
        setWorkingHoursForParams(view, R.id.working_hours_friday, 5);
        setWorkingHoursForParams(view, R.id.working_hours_saturday, 6);
        setWorkingHoursForParams(view, R.id.working_hours_sunday, 7);

        return builder.create();
    }


    private void setWorkingHoursForParams(View view, int id, int day){
        TextView text = view.findViewById(id);
        text.setText(company.getForDay(day).getTimeRange());
    }
}
