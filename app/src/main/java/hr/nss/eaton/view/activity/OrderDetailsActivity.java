package hr.nss.eaton.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import hr.nss.eaton.R;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.data.RequestListener;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.transaction.OrderItem;
import hr.nss.eaton.model.transaction.Status;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.activity.base.EatOnActivity;
import hr.nss.eaton.view.dialog.ChoosePaymentTypeDialogFragment;
import hr.nss.eaton.view.fragment.order.OrderItemFragment;
import hr.nss.eaton.view.listener.EatOnObjectListener;
import hr.nss.eaton.view.listener.ListItemListener;
import hr.nss.eaton.view.payment.Payment;
import hr.nss.eaton.view.payment.PaymentListener;
import hr.nss.eaton.view.properties.OrderStatusProperties;
import hr.nss.eaton.view.properties.PaymentProperties;

public class OrderDetailsActivity extends EatOnActivity implements ListItemListener<OrderItem> , EatOnObjectListener<Order>{
    public static final String ORDER_ID_ARG = "ord_id";
    private static final String LAST_PAYMENT_METHOD = "last_payment_method";

    protected OrderItemFragment orderItemFragment;
    protected Order order;

    protected Payment payment;

    public static void show(Context context, Order order){
        Intent intent = new Intent(context, OrderDetailsActivity.class);
        intent.putExtra(OrderDetailsActivity.ORDER_ID_ARG, order.getId());
        context.startActivity(intent);
    }


    @Override
    protected void onEatOnCreate(Bundle savedInstanceState) {
        super.onEatOnCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        setupToolbarAndBackButton();

        Long orderId = resolveParam(ORDER_ID_ARG, savedInstanceState);
        if(orderId != null){
            resolveOrder(orderId);
        }

    }

    @Override
    protected int getFragmentContainer() {
        return R.id.order_details_menu_items_container;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_order_details_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.activity_order_details_wallet: showPaymentMethods();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void resolveOrder(final Long id){
        Order o = Order.findById(id);
        if(o != null)showDetailsForOrder(o);
        else{
            requestUser(new RequestListener<User>() {
                @Override
                public void success(User user) {
                    showDetailsForOrder( Order.findById(id));
                }

                public void failure(Object response, Throwable reason) {
                    handleRequestFailure(response, reason);
                }
            });
        }

    }

    private void showDetailsForOrder(Order o) {
        order = o;
        Company company = order.getCompany();

        setTextView(R.id.order_details_name, company.getName());
        setTextView(R.id.order_details_address, company.getAddress());
        setTextView(R.id.order_details_number, company.getPhone());

        setTextView(R.id.order_details_date, order.getDateTime().toDateTimeString());
        setTextView(R.id.order_details_delivery_date, order.getDeliveryTime().toDateTimeString());
        setTextView(R.id.order_details_remark, order.getRemark());


        findViewById(R.id.order_details_btn_right).setOnClickListener(view -> confirmOrder());

        View cancel = findViewById(R.id.order_details_btn_left);

        if (order.isCancelable()) {
            cancel.setVisibility(View.VISIBLE);

            cancel.setOnClickListener(view -> removeOrder());
        }
        else {
            cancel.setVisibility(View.INVISIBLE);
        }

        OrderStatusProperties.setForOrder(order, findViewById(R.id.order_details_status_img));

        resolvePaymentMethod();

        showOrderItems();
    }


    private void showOrderItems(){
        orderItemFragment = OrderItemFragment.newInstance(order.getOrderItems());
        setFragment(orderItemFragment);
    }

    protected void calculateTotal(){

//        String text = FormatUtils.format(order.getTotal()) + " " + getString(R.string.currency);
        String text = payment.toCurrencyTotal(order);
        if(order.getStatus() == Status.INITIALIZED)
            text = getString(R.string.confirm)+ "   "  + text;

        setTextView(R.id.order_details_btn_right, text);
    }



    @Override
    protected void onResume() {
        super.onResume();

        // ako se vrati iz menu itema i ako ga je narucia onda se mora refreshat
        if(orderItemFragment != null && order != null){
            orderItemFragment.setData(order.getOrderItems());
            calculateTotal();
        }

        registerOrderListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterOrderListener(this);

    }

    @Override
    public void onListItemClick(OrderItem o) {
        MenuItemDetailsActivity.show(this, o.getMenuItem());
    }

    @Override
    public void dataChanged(List newData) {
        super.dataChanged(newData);
        // ako se makne orderItem onda makni i narudzbu ako je to bi zadnji artikal
        if (newData == null || newData.isEmpty()) {
            removeOrder();
        }

        calculateTotal();
    }

    @Override
    public void changed(Order o) {
        //samo ako se prikazuje order koja je updejtana onda updejtaj
        if (order != null && order.getId() == o.getId()) {
            showDetailsForOrder(o);
        }
    }


    private boolean canOrderNow(){
        return ( order != null && order.getCompany().canOrderNow());
    }

    private boolean canConfirmOrder(){

        if(!order.isNew()) return false;

        if(!canOrderNow()){
            showNotification(R.string.unable_to_order_company_currently_closed);
            return false;
        }

        return true;
    }

    private void confirmOrder(){

        if(order != null &&  canConfirmOrder()){

            payment.loadCredentials(new PaymentListener() {
                @Override
                public void success() {
                    if (payment.hasFunds(order.getTotal()))
                        placeOrder();
                    else
                        showNotification(R.string.insufficient_funds);
                }
                @Override
                public void fail(Exception reason) {
                    showNotification(reason.getMessage());
                }
            });
        }
    }

    private void resolvePaymentMethod(){
        PaymentProperties pp = getLastPaymentProperties();
        if(pp == null)
            pp = PaymentProperties.getDefault();
        setPaymentProperties(pp);
    }

    private PaymentProperties getLastPaymentProperties(){
        Integer lastIndex  = getPrefsInt(LAST_PAYMENT_METHOD);

        if(lastIndex != null)
            return PaymentProperties.at(lastIndex);

        return null;
    }

    private void setLastPaymentMethod(){
        if(payment != null){
            int index = PaymentProperties.indexOf(payment);
            setPrefsInt(LAST_PAYMENT_METHOD,index);
        }
    }


    private void showPaymentMethods(){
        ChoosePaymentTypeDialogFragment.newInstance(payment)
                .show(getSupportFragmentManager(),"payment");
    }

    public void setPaymentProperties(PaymentProperties pp){
        Payment temp = pp.getPayment(this);

        if(temp.hasCredentials()){
            payment = temp;

            setLastPaymentMethod();

            payment.connect();

            calculateTotal();
        }
        else{
            showNotification(R.string.credentials_for_payment_not_set);
        }
    }



    private void placeOrder(){
        showProgressDialog(R.string.wait, R.string.order_processing);

        order.confirm();

        order.setPaymentMethod(PaymentProperties.getPaymentMethod(payment));

        postOrder();

    }

    private void postOrder(){

        DataProvider.get().postOrder(order, new RequestListener<Order>() {
            @Override
            public void success(Order order) {
                payOrder(order);
            }

            @Override
            public void failure(Object response, Throwable reason) {
                dismissProgressDialog();
                order.setStatus(Status.INITIALIZED);
                handleRequestFailure(response, reason);
                // ako se ne navigira ca iz ekrana pri obradi greske
                // onda ce se prikazat greska
                if(response != null)
                    showNotification(response.toString());
                else
                    showNotification(R.string.error_try_again);
            }
        });
    }

    private void payOrder(Order order){

        payment.pay(order, new PaymentListener() {
            @Override
            public void success() {
                showDetailsForOrder(order);
                showNotification(R.string.order_confirmed);
                dismissProgressDialog();
            }

            @Override
            public void fail(Exception reason) {
                dismissProgressDialog();

                String msg = getString(R.string.error_try_again) +
                        (reason != null ? reason.getMessage() : "");
                showNotification(msg);

                handleFailPayment(order);
            }
        });
    }

    private void handleFailPayment(Order order){

        DataProvider.get().deleteOrder(order, new RequestListener<Order>() {
            @Override
            public void success(Order o) {
                order.setStatus(Status.INITIALIZED);
                EatOnObject.eatOnObjectAdded(o);
            }
            @Override
            public void failure(Object response, Throwable reason) {}
        });
    }




    private void removeOrder(){
        removeOrder(this,order, new RequestListener<Order>() {
            @Override
            public void success(Order order) {
                showToast(R.string.order_removed);
                finish();
            }

            @Override
            public void failure(Object response, Throwable reason) {
                handleRequestFailure(response, reason);
                if (response == null && reason == null) {
                    showDetailsForOrder(order);
                    showNotification(R.string.unable_to_delete_order_already_confirmed);
                }
                else if(response != null){
                    showNotification(response.toString());
                }
            }
        });
    }




    private static boolean canCancelPayment(Context context, Order order){
        return getOrderPayment(context, order).canCancelOrder(order);
    }


    private static void cancelPayment(Context context,Order order, PaymentListener paymentListener){
        getOrderPayment(context, order).cancel(order, paymentListener);
    }

    private static Payment getOrderPayment(Context context,Order order){

        PaymentProperties paymentProperties = PaymentProperties.getPaymentProperties(order.getPaymentMethod());

        Payment payment = paymentProperties.getPayment(context);

        return payment;
    }




    /**
     * Brisanje ordera
     * Ako je tek kreirana onda brise instantno
     * A ako je već poslana na server onda se provjerava stanje ordera
     * ako je vec potvrdena od strane company onda vraca gresku
     * inace salje brisanje ordera, te ako se uspjesno izbrise
     * onda se poziva success
     * @param context
     * @param order
     * @param requestListener
     */
    public static void removeOrder(Context context, final Order order, final RequestListener<Order> requestListener){

        if (order.getStatus() == Status.INITIALIZED) {
            order.getUser().removeOrder(order);
            requestListener.success(order);
        }

        else if(order.getStatus() == Status.ORDERED){

            final ProgressDialog dialog = ProgressDialog.show(context,
                    context.getString(R.string.wait), context.getString(R.string.order_canceling), true);

            DataProvider.get().requestOrder(order.getId(), new RequestListener<Order>() {
                @Override
                public void success(Order o) {

                    if (o.getStatus() == Status.ORDERED) {

                        if(canCancelPayment(context, order)){
                            deleteOrder(context,order,requestListener, dialog);
                        }
                        else{
                            dialog.dismiss();
                            requestListener.failure(
                                    context.getString(R.string.unable_to_delete_order_payment_not_confirmed),null);
                        }

                    } else {
                        order.setStatus(o.getStatus());
                        dialog.dismiss();
                        requestListener.failure(null, null);
                    }
                }

                @Override
                public void failure(Object response, Throwable reason) {
                    dialog.dismiss();
                    requestListener.failure(response, reason);
                }
            });
        }
    }

    private static void deleteOrder(Context context, Order order,
                                    RequestListener<Order> requestListener, ProgressDialog dialog){

        DataProvider.get().deleteOrder(order, new RequestListener<Order>() {
            @Override
            public void success(Order order1) {
                cancelPayment(context, order, new PaymentListener() {
                    @Override
                    public void success() {
                        dialog.dismiss();
                        order.getUser().removeOrder(order);
                        requestListener.success(order);
                    }

                    @Override
                    public void fail(Exception reason) {
                        dialog.dismiss();
                        requestListener.failure(null, reason);
                    }
                });
            }

            @Override
            public void failure(Object response, Throwable reason) {
                dialog.dismiss();
                requestListener.failure(response, reason);
            }
        });
    }


}
