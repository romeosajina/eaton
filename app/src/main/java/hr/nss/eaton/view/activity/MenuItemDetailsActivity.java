package hr.nss.eaton.view.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.LinkedHashMap;
import java.util.List;

import hr.nss.eaton.R;
import hr.nss.eaton.model.common.PricedEatOnObject;
import hr.nss.eaton.model.data.RequestListener;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.menu.MenuItemOption;
import hr.nss.eaton.model.menu.MenuItemOptionSupplement;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.transaction.OrderItem;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.activity.base.EatOnActivity;
import hr.nss.eaton.view.base.EatOnBaseExpandableListAdapter;
import hr.nss.eaton.view.dialog.OrderMenuItemDialogFragment;
import hr.nss.eaton.view.utils.FormatUtils;

public class MenuItemDetailsActivity extends EatOnActivity {

    public static final String MENU_ITEM_ID = "menuItemId";
    public static final String CALLING_ACTIVITY = "callingActivity";
    private OrderItem orderItem;
    private MenuItem menuItem;
    private Order order;

    private String callingActivity;

    //private EditText numberOfOrdersEditText;
    private Button addToCartButton;
    private android.view.MenuItem  showCartView;
    private MenuItemOptionsExpandAdapter adapter;

    private User user;

    @Override
    protected void onEatOnCreate(final Bundle savedInstanceState) {
        super.onEatOnCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_item_details);

        //numberOfOrdersEditText = findViewById(R.id.menu_item_details_number_of_ordered);

        setupToolbarAndBackButton();

        requestUser(new RequestListener<User>() {
            @Override
            public void success(User u) {
                user = u;
                Long menuItemId = resolveParam(MENU_ITEM_ID, savedInstanceState);
                if (menuItemId != null) {
                    resolveMenuItem(menuItemId);
                }
            }

            @Override
            public void failure(Object response, Throwable reason) {
                handleRequestFailure(response, reason);
            }
        });



        callingActivity = resolveParam(CALLING_ACTIVITY, savedInstanceState);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu_item_details_menu, menu);
        showCartView = toolbar.getMenu().findItem(R.id.activity_menu_item_details_menu_cart);

        resolveOrder();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {

        if(item.getItemId() == R.id.activity_menu_item_details_menu_cart){

            MenuItemDetailsActivity.this.discardOrderIfNeeded();
            showOrderDetails();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        discardOrderIfNeeded();
    }

    private void discardOrderIfNeeded() {
        // ako je orderItem kreiran a ni dodan u kosaricu kroz dialog
        // onda ga se mice
        // ako je orderItem dodan kroz dialog on ce bit null
        // osim ako korisnik ni opet klika po opcijama i supplementima
        // pa se kreira novi orderItem
        if (orderItem != null) {
          //  if (orderItem.getAmount() == 0) {
                user.removeOrderItem(orderItem);
                orderItem = null;
           // }
            // order more bit maknuta pa onda nema ordera
            if(!order.hasOrderItems())order = null;
        }
    }

    private OrderItem ensureOrderItem(MenuItem menuItem) {
        if (orderItem == null){
            orderItem = user.createOrderItem(menuItem);
        }
        resolveOrder();
        return orderItem;
    }


    void resolveOrder(){

        if(order == null && menuItem != null){
            order = user.findNewOrder(menuItem.getMenu().getCompany());
        }
        boolean visible = ((order != null && order.hasOrderItems() && orderItem == null) ||
                            (order != null && order.getOrderItems().size() > 1 && orderItem != null));

        showCartView.setVisible(visible);
    }


    private void optionChanged(MenuItemOption menuItemOption) {
        ensureOrderItem(menuItemOption.getMenuItem()).setMenuItemOption(menuItemOption);
        calculateTotal();
    }

    private void supplementChanged(MenuItemOptionSupplement menuItemOptionSupplement, boolean added) {
        if (added) {
            ensureOrderItem(menuItemOptionSupplement.getMenuItemOption().getMenuItem())
                    .ensureOrderItemSupplement(menuItemOptionSupplement);
        } else {
            if (orderItem != null) {
                orderItem.removeOrderItemSupplement(menuItemOptionSupplement);
            }
        }
        calculateTotal();
    }

    private void calculateTotal() {

        Number price = orderItem != null ? orderItem.getTotal() :
                !menuItem.hasMenuItemOptionsOptions() ? menuItem.getPrice() : menuItem.getMenuItemOptions().get(0).getPrice();

        String text = getString(R.string.add_to_cart)
                + "         " + FormatUtils.format(price)
                + " " + getString(R.string.currency);

        addToCartButton.setText(text);

    }

    private void resolveMenuItem(Long menuItemId){
       MenuItem mi = MenuItem.findById(menuItemId);
       if(mi != null)
           show(mi);
       // ako ne more nac menu item znaci da ni loadan
       // onda se vraca na pocetnu aktivnost
       else
           returnToMainActivity();
    }

    private void show(MenuItem mi) {
        menuItem = mi;

        user.addRecentMenuItem(mi);
        setRecentMenuItems(user.getRecentMenuItems());

        ExpandableListView expandableListView = findViewById(R.id.menu_item_details_expandable_list);
        adapter = new MenuItemOptionsExpandAdapter(this, menuItem);
        expandableListView.setAdapter(adapter);

        setTextView(R.id.menu_item_details_name, menuItem.getName());
        setTextView(R.id.menu_item_details_price, FormatUtils.format(menuItem.getPrice()));
        setTextView(R.id.menu_item_details_description, menuItem.getDescription());

        setTextView(R.id.menu_item_details_company_name, menuItem.getMenu().getCompany().getName(), view -> {
            if(CompanyDetailsActivity.class.getName().equals(callingActivity)){
                finish();
            }
            else{
                CompanyDetailsActivity.show(MenuItemDetailsActivity.this, menuItem.getMenu().getCompany());
            }
        });


        setImageView(R.id.menu_item_details_img, menuItem.getPhoto(), true);

        addToCartButton = findViewById(R.id.menu_item_details_add_to_cart_btn);

        addToCartButton.setOnClickListener(view -> {
            OrderMenuItemDialogFragment f = OrderMenuItemDialogFragment.newInstance(ensureOrderItem(menuItem));
            f.show(getSupportFragmentManager(), "order_dialog");
        });

        calculateTotal();

    }

    public void orderItemConfirmed(){


        int resId = order != null && order.getOrderItemCount() == 1 ? R.string.order_created_item_added_to_cart : R.string.item_added_to_cart;

        Snackbar.make(addToCartButton, resId, Snackbar.LENGTH_LONG)
        .setAction(R.string.show_cart, view -> showOrderDetails()).show();

        orderItem = null;
        adapter.setGroupItems(buildGroups(menuItem));

        calculateTotal();
        resolveOrder();
    }

    private void showOrderDetails(){
        returnToMainActivity();
        OrderDetailsActivity.show(MenuItemDetailsActivity.this, order);
    }



    public static void show(Activity context, MenuItem item) {
        Intent intent = new Intent(context, MenuItemDetailsActivity.class);
        intent.putExtra(MENU_ITEM_ID, item.getId());
        intent.putExtra(CALLING_ACTIVITY, context.getClass().getName());
        context.startActivity(intent);
    }


    private static LinkedHashMap<Integer, List<PricedEatOnObject>> buildGroups(MenuItem menuItem) {
        LinkedHashMap groupItems = new LinkedHashMap<>();

        if (menuItem.hasMenuItemOptionsOptions()) {
            groupItems.put(R.string.options, menuItem.getMenuItemOptions());
            groupItems.put(R.string.supplements, menuItem.getMenuItemOptions().get(0).getMenuItemOptionSupplements());

        }

        return groupItems;
    }

    private void setCheckboxSafely(CompoundButton cb, boolean checked, MenuItemOption menuItemOption) {
        // ako se nebi stavia listener na null, onda bi se nakon mijenjanja pozva pa bi stvaralo probleme
        // a zapravo se samo treba osvjezit view
        cb.setOnCheckedChangeListener(null);
        cb.setChecked(checked);
        cb.setOnCheckedChangeListener(new OptionOnCheckedChangeListener( menuItemOption));
    }

    private class MenuItemOptionsExpandAdapter extends EatOnBaseExpandableListAdapter<Integer, PricedEatOnObject> {


        public MenuItemOptionsExpandAdapter(Context context, MenuItem menuItem) {
            super(context, buildGroups(menuItem), R.layout.fragment_menu_item_details_list_group,
                    R.layout.fragment_menu_item_details_list_group_item);
        }

        @Override
        protected void bindGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup viewGroup) {
            ((TextView) view.findViewById(R.id.menu_item_details_list_group)).setText(getGroup(groupPosition));
        }

        @Override
        protected void bindChildView(final int groupPosition, final int childPosition,
                                     boolean isLastChild, final View view, ViewGroup viewGroup) {

            PricedEatOnObject pricedEatOnObject = getChild(groupPosition, childPosition);

            final CheckBox checkBox = view.findViewById(R.id.menu_item_details_list_group_item_text);
            checkBox.setText(pricedEatOnObject.getName());

            ((TextView) view.findViewById(R.id.menu_item_details_list_group_item_price))
                    .setText(FormatUtils.format(pricedEatOnObject.getPrice()));

            OrderItem oi = MenuItemDetailsActivity.this.orderItem;

            if (groupPosition == 0) {

                boolean checked = ((oi == null && childPosition == 0) ||
                        (oi != null && pricedEatOnObject.equals(oi.getMenuItemOption())));

                setCheckboxSafely(checkBox, checked, (MenuItemOption) pricedEatOnObject);

            } else {

                final MenuItemOptionSupplement supplement =
                        (MenuItemOptionSupplement) getChild(groupPosition, childPosition);

                checkBox.setChecked(oi != null && oi.findOrderItemSupplement(supplement) != null);

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean selected) {
                        supplementChanged(supplement, selected);
                    }
                });
            }
        }
    }



    private class OptionOnCheckedChangeListener implements CompoundButton.OnCheckedChangeListener {
 //       private MenuItemOptionsExpandAdapter adapter;
        private MenuItemOption menuItemOption;

        public OptionOnCheckedChangeListener(MenuItemOption menuItemOption) {
   //         this.adapter = adapter;
            this.menuItemOption = menuItemOption;
        }

        @Override
        public void onCheckedChanged(CompoundButton cb, boolean checked) {
            if (!checked) {
                setCheckboxSafely(cb, true, menuItemOption);
            } else {
                optionChanged(menuItemOption);
                adapter.setGroupItems(R.string.supplements, menuItemOption.getMenuItemOptionSupplements());
            }
        }
    }
}

