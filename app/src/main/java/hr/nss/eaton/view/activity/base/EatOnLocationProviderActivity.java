package hr.nss.eaton.view.activity.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import hr.nss.eaton.model.common.EatOnObject;


/**
 * Created by ASUS on 3.6.2018..
 */

public class EatOnLocationProviderActivity extends EatOnActivity implements LocationListener {

    private static final int GPS_ENABLE_REQUEST = 3;
    private LocationManager mLocationManager;


    private AlertDialog mGPSDialog;

    protected static final long LOCATION_REFRESH_DISTANCE = 30;//meters
    protected static final long LOCATION_REFRESH_TIME = 3000;//milisecunds

    public static final int EATON_PERMISSIONS_REQUEST_LOCATION = 100;
    public static final int EATON_PERMISSIONS_REQUEST_LAST_LOCATION = 101;


    protected boolean isLocationProviderInited() {
        return mLocationManager != null;
    }

    private void makeLocationManager(){
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

    }
    private void removeLocationManager(){
        if (isLocationProviderInited()) {
            mLocationManager.removeUpdates(this);
        }
    }
    @SuppressLint("MissingPermission")
    protected void initLocationProvider() {
        makeLocationManager();

        if(requestLocationPermission(EATON_PERMISSIONS_REQUEST_LOCATION)){
            requestLocationUpdates();
            onLocationChanged(getLastKnowLocation());
        }
    }

    @SuppressLint("MissingPermission")
    private void requestLocationUpdates(){
        if(isLocationProviderInited()){
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, this);
        }

    }
    private boolean requestLocationPermission(int code) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, code);
            return false;
        }
        return true;
    }


    @SuppressLint("MissingPermission")
    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == EATON_PERMISSIONS_REQUEST_LOCATION && PackageManager.PERMISSION_GRANTED == grantResults[0]) {
            requestLocationUpdates();
        } else if (requestCode == EATON_PERMISSIONS_REQUEST_LAST_LOCATION && PackageManager.PERMISSION_GRANTED == grantResults[0]) {
            onLocationChanged(getLastKnowLocation());
        }

    }
    protected void requestLocation() {
        if (!isLocationProviderInited()) {
            initLocationProvider();
        } else {
            //requestLocationPermission();
            requestLastKnownLocation();
        }
    }

    @SuppressLint("MissingPermission")
    private Location getLastKnowLocation(){
        if(isLocationProviderInited())
            return mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        return null;
    }



    @SuppressLint("MissingPermission")
    private void requestLastKnownLocation() {

        if (requestLocationPermission(EATON_PERMISSIONS_REQUEST_LAST_LOCATION)) {
            onLocationChanged(getLastKnowLocation());
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        EatOnObject.loge("LOCATION "+location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        EatOnObject.loge("LOCATION STATUS CHANGED " + provider + " " + status + " " + extras);

    }

    @Override
    public void onProviderEnabled(String provider) {
        if(LocationManager.GPS_PROVIDER.equals(provider)){
            if(mGPSDialog != null){
                mGPSDialog.dismiss();
                mGPSDialog = null;
            }
            initLocationProvider();
            requestLocationUpdates();
        }
   }

    @Override
    public void onProviderDisabled(String provider) {
        if(LocationManager.GPS_PROVIDER.equals(provider)){
            removeLocationManager();
            mLocationManager = null;
            showGPSDisabledDialog();
        }
    }

    protected Double parseDouble(String num){
        try {
            return Double.parseDouble(num);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    protected boolean isLocationValid(String loc){

        return loc != null && loc.contains(", ") && loc.split(", ").length == 2 &&
                parseDouble(loc.split(", ")[0]) != null &&
                parseDouble(loc.split(", ")[1]) != null;
    }

    private void showGPSDisabledDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("GPS Disabled");
        builder.setMessage("Gps is disabled, in order to use the application properly you need to enable GPS of your device");
        builder.setPositiveButton("Enable GPS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), GPS_ENABLE_REQUEST);
            }
        }).setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        mGPSDialog = builder.create();
        mGPSDialog.show();
    }


    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == GPS_ENABLE_REQUEST)
        {
            if (!isLocationProviderInited())
            {
                makeLocationManager();
            }

            if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            {
                showGPSDisabledDialog();
            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            requestLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

           removeLocationManager();
        }
    }

}
