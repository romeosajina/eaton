package hr.nss.eaton.view.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import hr.nss.eaton.R;
import hr.nss.eaton.view.activity.OrderDetailsActivity;

public class AndroidNotification {
    private static final String CHANNEL_ID = "EatOn";


    private AndroidNotification(){

    }
    public static void showOrderNotification(Context context, int id, String title, String content){

        Intent intent = new Intent(context, OrderDetailsActivity.class)
                .putExtra(OrderDetailsActivity.ORDER_ID_ARG, id);
        showNotification(context, id, title, content, intent);

    }

    public static  void showNotification(Context context, int id, String title, String content, Intent intent){
        createNotificationChannel(context);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.eat_on_logo)
                .setColor(context.getResources().getColor(R.color.colorPrimary))
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        if(intent != null){

            PendingIntent pendingIntent =
                    PendingIntent.getActivity(context.getApplicationContext(), 0, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            // Set the intent that will fire when the user taps the notification
            mBuilder.setContentIntent(pendingIntent).setAutoCancel(true);
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

// notificationId is a unique int for each notification that you must define
        notificationManager.notify(id, mBuilder.build());
    }

    private static void createNotificationChannel(Context context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

}
