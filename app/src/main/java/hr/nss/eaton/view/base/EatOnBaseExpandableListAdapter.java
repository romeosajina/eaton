package hr.nss.eaton.view.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import hr.nss.eaton.model.common.EatOnObject;

/**
 * Created by Robert on 07-Nov-18.
 */

public abstract class EatOnBaseExpandableListAdapter<G,  I> extends BaseExpandableListAdapter {

    private Context context;

    private LinkedHashMap<G, List<I>> groupItems;
    private Object[] groups;
    private int groupLayoutId;
    private int itemLayoutId;
    private LayoutInflater inflater;

 //   private View views[][];

    public EatOnBaseExpandableListAdapter(Context context, LinkedHashMap<G, List<I>> groupItems, int groupLayoutId, int itemLayoutId) {
        this.context = context;
        this.groupLayoutId = groupLayoutId;
        this.itemLayoutId = itemLayoutId;
        setGroupItems(groupItems);

        /*
        views = new View[groups.length][];
        for(int i = 0; i  < groups.length; i++){
            views[i] = new View[getChildrenCount(i)+1];
        }
        */

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public LinkedHashMap<G, List<I>> getGroupItems() {
        return groupItems;
    }

    public void setGroupItems(LinkedHashMap<G, List<I>> groupItems) {
        this.groupItems = groupItems;
        this.groups = groupItems.keySet().toArray();
        notifyDataSetChanged();
    }
    public void setGroupItems(G key, List<? extends I> items){
        groupItems.put(key, new ArrayList<>(items));
        setGroupItems(groupItems);
    }
/*
    private View retrieveView(int groupPos, int childPos){
        childPos++;
        if ( groupPos < views.length && views.length > 0 && views[groupPos].length > childPos && childPos >= 0) {
            return views[groupPos][childPos];
        }

        return null;
    }
    public View getView(int groupPos){
        return retrieveView(groupPos, -1);
    }
    public View getView(int groupPos, int childPos){
        return retrieveView(groupPos, childPos);
    }
    private void setView(int groupPos, int childPos, View view){
        views[groupPos][childPos] = view;
    }
    */
    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return groupItems.get(groups[i]).size();
    }

    @Override
    public G getGroup(int i) {
        return (G) groups[i];
    }

    @Override
    public I getChild(int groupPosition, int childPosition) {
        return groupItems.get(groups[groupPosition]).get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition * groupPosition + childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public final View getGroupView(final int groupPosition, boolean isExpanded, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(groupLayoutId, null);
     //       setView(groupPosition, 0, view);
/*
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onGroupClick(view, groupPosition, getGroup(groupPosition));
                }
            });
            */
        }

        bindGroupView(groupPosition, isExpanded, view, viewGroup);
        return view;
    }

    protected abstract void bindGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup viewGroup);

    protected abstract void bindChildView(int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup viewGroup);

    @Override
    public final View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(itemLayoutId,  null);
    //        setView(groupPosition, childPosition + 1, view);

            /*
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onChildClick(view, groupPosition, childPosition, getChild(groupPosition, childPosition));
                }
            });
            */

        }
        bindChildView(groupPosition, childPosition, isLastChild, view, viewGroup);
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    /*
    protected void onGroupClick(View view, int groupPosition, G group){

    }

    protected void onChildClick(View view, int groupPosition, int childPosition, I item){

    }
    */
}
