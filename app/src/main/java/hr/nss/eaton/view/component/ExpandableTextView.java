package hr.nss.eaton.view.component;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Robert on 03-Jun-18.
 */

@SuppressLint("AppCompatCustomView")
public class ExpandableTextView extends TextView implements View.OnClickListener{

    private int defaultMaxLines;

    public ExpandableTextView(Context context) {
        super(context);
        init();
    }

    public ExpandableTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExpandableTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ExpandableTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){

        defaultMaxLines = getMaxLines();
        setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(getMaxLines() == defaultMaxLines) setMaxLines(Integer.MAX_VALUE);
        else setMaxLines(defaultMaxLines);

    }

}
