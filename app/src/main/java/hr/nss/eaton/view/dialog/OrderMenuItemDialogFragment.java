package hr.nss.eaton.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.core.math.MathUtils;
import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import hr.nss.eaton.R;
import hr.nss.eaton.model.transaction.OrderItem;
import hr.nss.eaton.view.activity.MenuItemDetailsActivity;
import hr.nss.eaton.view.listener.TextChangedListener;
import hr.nss.eaton.view.utils.FormatUtils;


public class OrderMenuItemDialogFragment extends DialogFragment {

    private OrderItem orderItem;
    private EditText quantityEditText;
    private EditText remarkEditText;
    private Button addToCartButton;

    public OrderMenuItemDialogFragment() {
    }
    public static OrderMenuItemDialogFragment newInstance(OrderItem orderItem) {
        OrderMenuItemDialogFragment fragment = new OrderMenuItemDialogFragment();
        fragment.orderItem = orderItem;
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_order_menu_item, null);
        builder.setView(view);

        quantityEditText = view.findViewById(R.id.dialog_order_menu_item_quantity);

        quantityEditText.addTextChangedListener(new TextChangedListener(){
            @Override
            public void onTextChanged() {
                int num = parseQuantity();
                if(num < 0){
                    quantityChanged(0);
                }
                else{
                    quantityChanged(num);
                }
            }
        });

        remarkEditText = view.findViewById(R.id.dialog_order_menu_item_remark);

        (view.findViewById(R.id.dialog_order_menu_item_remove_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeQuantity(-1);
            }
        });

        (view.findViewById(R.id.dialog_order_menu_item_add_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeQuantity(1);
            }
        });

        addToCartButton = view.findViewById(R.id.dialog_order_menu_item_btn);

        addToCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(parseQuantity()<1) {
                    changeQuantity(1);
                }
                else{
                    //CONFIRM ORDER
                    //Spremi remark
                    String text = remarkEditText.getText().toString();
                    if(text != null && text.trim().isEmpty())text = null;
                    orderItem.setRemark(text);

                    //TODO: NAPRAVIT PREKO LISTENERA
                    Activity activity = getActivity();
                    if(activity instanceof MenuItemDetailsActivity){
                        ((MenuItemDetailsActivity) activity).orderItemConfirmed();
                    }
                    dismiss();
                }
            }
        });


        // ako je quantity bila 0 pokle ovega promjenit ce se na jedan jer je min 1
        changeQuantity(0);
        return builder.create();
    }
    int parseQuantity(){
        String text = quantityEditText.getText().toString();
        if(text.isEmpty())return  -1;
        int textNum = Integer.parseInt(text);
        return textNum;
    }
    private void changeQuantity(int amount){
        int textNum = parseQuantity();
        amount = MathUtils.clamp(amount + textNum, 1, 99);
        FormatUtils.formatAmount(quantityEditText, amount);
        quantityChanged(amount);
    }

    void quantityChanged(int amount){

        orderItem.setAmount(amount);
        updateButtonText();
    }

    void updateButtonText(){

        String text = getString(R.string.add_to_cart)
                + "         " + FormatUtils.format(orderItem.getTotal())
                + " " + getString(R.string.currency);

        addToCartButton.setText(text);
    }

}
