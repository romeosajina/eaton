package hr.nss.eaton.view.properties;

import android.graphics.Color;
import android.widget.ImageView;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import hr.nss.eaton.R;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.transaction.Status;
import hr.nss.eaton.view.activity.ViewOrdersActivity;

/**
 * Created by Robert on 01-Nov-18.
 */

public class OrderStatusProperties {

    public int text;
    public int icon;
    public int btnText;
    public boolean buttonVisible;
    public int color;

    public OrderStatusProperties(int text, int icon, int btnText, int color) {
        this.text = text;
        this.icon = icon;
        this.btnText = btnText;
        this.buttonVisible = true;
        this.color = color;
    }

    public OrderStatusProperties(int text, int icon, int color) {
        this.text = text;
        this.icon = icon;
        this.buttonVisible = false;
        this.color = color;
    }

    public static OrderStatusProperties getForStatus(Status status){
        return ordersStatusProperties.get(status);
    }
    public static Collection<OrderStatusProperties> values(){
        return ordersStatusProperties.values();
    }

    private static final Map<Status, OrderStatusProperties> ordersStatusProperties = new LinkedHashMap<>();

    static {

        ordersStatusProperties.put(Status.INITIALIZED, new OrderStatusProperties(R.string.order_created, R.drawable.ic_new_releases,R.string.cancel, Color.parseColor("#ffdb70")));
        ordersStatusProperties.put(Status.ORDERED, new OrderStatusProperties(R.string.order_ordered, R.drawable.ic_done,R.string.cancel, Color.parseColor("#4cffdb")));
        ordersStatusProperties.put(Status.CONFIRMED, new OrderStatusProperties(R.string.order_confirmed, R.drawable.ic_done_all, Color.parseColor("#5fff67")));
        ordersStatusProperties.put(Status.PROCESSING, new OrderStatusProperties(R.string.order_processing, R.drawable.ic_autorenew, Color.parseColor("#ff2d37")));
        ordersStatusProperties.put(Status.FINISHED, new OrderStatusProperties(R.string.order_finished, R.drawable.ic_assignment_turned_in, Color.parseColor("#4cffdb")));
        ordersStatusProperties.put(Status.DELIVERED, new OrderStatusProperties(R.string.order_delivered, R.drawable.ic_drive_eta, R.string.reorder, Color.parseColor("#189e00")));

    }



    public static void setForOrder(Order order, ImageView img) {
        OrderStatusProperties osp = OrderStatusProperties.getForStatus(order.getStatus());
        img.setImageResource(osp.icon);
        img.setColorFilter(osp.color);
    }


}
