package hr.nss.eaton.view.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import hr.nss.eaton.R;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.data.RequestListener;
import hr.nss.eaton.model.data.TestDataProvider;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.activity.base.EatOnActivity;
import hr.nss.eaton.view.fragment.recent.RecentItemFragment;

public class MainActivity extends EatOnActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final int MAX_RECENT_ITEMS = 5;

    private DrawerLayout drawer;

    @Override
    protected void onEatOnCreate(Bundle savedInstanceState) {
        super.onEatOnCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setupToolbar();

        /*FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


      //  MenuItem item = navigationView.getMenu().findItem(R.id.nav_recommend_to_friends);

        //((ShareActionProvider) item.getActionProvider()).setShareIntent(intent);


        test();

        requestUser(new RequestListener<User>() {
            @Override
            public void success(User user) {

                View v = navigationView.getHeaderView(0);
                ((TextView) v.findViewById(R.id.nav_header_main_username)).setText(user.getName() + " " + user.getSurname());
                ((TextView) v.findViewById(R.id.nav_header_main_email)).setText(user.getEmail());

                addRecentOrRecommended(user);
            }


            public void failure(Object response, Throwable reason) {
                handleRequestFailure(response, reason);
            }
        });


    }



    private void addRecentOrRecommended(User user){

        addMenuItems(user);
        addCompanies(user);

    }
    private void addMenuItems(User user) {

        if(user.getRecentMenuItems().size() >= MAX_RECENT_ITEMS){
            addRecentToContainer(R.id.recent_food_container, user.getRecentMenuItems());
        }
        else{

            setTextView(R.id.activity_main_recent_food, getString(R.string.suggested_menu_items));

            DataProvider.get().requestMenuItems(new RequestListener<List<hr.nss.eaton.model.menu.MenuItem>>() {
                @Override
                public void success(List<hr.nss.eaton.model.menu.MenuItem> menuItems) {
                    addRecentToContainer(R.id.recent_food_container, menuItems);
                }
                @Override
                public void failure(Object response, Throwable reason) {}
            }, user.getSearchParams());
        }
    }
    private void addCompanies(User user) {

        if (user.getRecentCompanies().size() >= MAX_RECENT_ITEMS) {
            addRecentToContainer(R.id.recent_company_container, user.getRecentCompanies());
        } else {

            setTextView(R.id.activity_main_recent_places, getString(R.string.suggested_companies));

            DataProvider.get().requestCompanies(new RequestListener<List<Company>>() {
                @Override
                public void success(List<Company> companies) {
                    addRecentToContainer(R.id.recent_company_container, companies);
                }
                @Override
                public void failure(Object response, Throwable reason) {}
            }, user.getSearchParams());
        }
    }





    private void test() {
        TestDataProvider.setContext(this);
    }

    @Override
    public void onBackPressed() {
   //     DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
             //super.onBackPressed();
            finishAffinity();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void setToolbarAssets(int layout, int icon){
/*        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), icon);
        toolbar.setOverflowIcon(drawable);

        Drawable wrappedDrawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(R.color.white));
*/
        toolbar.getMenu().clear();
        getMenuInflater().inflate(layout, toolbar.getMenu());
/*
        SearchView search = (SearchView)toolbar.getMenu().findItem(R.id.search_bar_search).getActionView();
        search.setSubmitButtonEnabled(true);
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                showToast("Search submit clicked");
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });

*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
/*
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showToast("Action settings clicked");
            return true;

        }else if (id == R.id.search_bar_map) {
            showToast("Action map clicked");
            return true;

        }else if (id == R.id.search_bar_tune) {
            showToast("Action tune clicked");
            return true;

        }else if (id == R.id.search_bar_search) {
            showToast("Action search clicked");
            return true;
        }*/


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            //setFragment(HomeFragment.newInstance());
            //setToolbarAssets(R.menu.main, R.drawable.ic_search);

        } else if (id == R.id.nav_search) {
            //setFragment(SearchFragment.newInstance());
            //setToolbarAssets(R.menu.search_menu, R.drawable.ic_tune);
            Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_settings) {
            //setFragment(SettingsFragment.newInstance());
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_orders) {
            //setFragment(OrdersFragment.newInstance());
            Intent intent = new Intent(getApplicationContext(), ViewOrdersActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_recommend_to_friends) {

            String sAux = "\n" + getString(R.string.share_message) + "\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=" + getPackageName() + " \n\n";

            Intent intent = new Intent()
                    .setAction(Intent.ACTION_SEND)
                    .putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
                    .putExtra(Intent.EXTRA_TEXT, sAux)
                    .setType("text/plain");

            intent = Intent.createChooser(intent, getString(R.string.share_with));
            startActivity(intent);
        } else if (id == R.id.nav_rate) {
            rateApp();
        }else if (id == R.id.nav_log_out) {
            clearCredentials();
            checkUserLoggedIn();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void rateApp()
    {
        try
        {
            Intent rateIntent = rateIntentForUrl("market://details");
            startActivity(rateIntent);
        }
        catch (ActivityNotFoundException e)
        {
            Intent rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details");
            startActivity(rateIntent);
        }
    }

    private Intent rateIntentForUrl(String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21)
        {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        }
        else
        {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }


    /*@Override
    protected int getFragmentContainer() {
        return R.id.main_fragment_container;
    }*/


    private  void addRecentToContainer(int container, List<? extends EatOnObject> items){

        if(items == null)return;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
      //  transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.setCustomAnimations(R.anim.slide, R.anim.slide);


        int i = 0;

        for(EatOnObject m : items){
            /*
            RecentItemFragment f;
            boolean isMenuItem = m instanceof hr.nss.eaton.model.menu.MenuItem;

            if(isMenuItem)
                f = RecentItemFragment.newInstance(((hr.nss.eaton.model.menu.MenuItem)m).getName(), ((hr.nss.eaton.model.menu.MenuItem)m).getPhoto());
            else
                f = RecentItemFragment.newInstance(((Company)m).getName(), ((Company)m).getPhoto());
            */

            RecentItemFragment f = RecentItemFragment.newInstance(m);

            transaction.add(container, f, "recent_item_tag_"+i);
            // ako se ne doda to back stack onda na back clicked gre van s aktivnosti
            // a ako ne onda se svaki posebno mice iz aktivnosti
       //     transaction.addToBackStack(null);

            i++;
            if(i >= MAX_RECENT_ITEMS) break;
        }

        transaction.commit();
    }
}
