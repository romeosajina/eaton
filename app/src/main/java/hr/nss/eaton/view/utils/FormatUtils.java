package hr.nss.eaton.view.utils;

import androidx.core.content.ContextCompat;
import android.widget.EditText;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import hr.nss.eaton.R;

/**
 * Created by Robert on 03-Jun-18.
 */

public class FormatUtils {
    //  private static DecimalFormat decimalFormatter = (DecimalFormat) DecimalFormat.getInstance();
    private static DecimalFormat decimalFormatter = new DecimalFormat("#,##0.00");
    private static DecimalFormat decimalFormatter6 = new DecimalFormat("#,##0.000000");


    private static NumberFormat numberFormat = new DecimalFormat("00");

    private FormatUtils() {
    }

    public static String format(Number number){
        return decimalFormatter.format(number);
    }
    public static String format(BigDecimal number){
        return decimalFormatter6.format(number);
    }

    public static void formatAmount(TextView textView, int amount){
        textView.setText(numberFormat.format(amount));

        if(amount == 0){
            textView.setTextColor(ContextCompat.getColor(textView.getContext(), R.color.colorSecondary));
        }
        else{
            textView.setTextColor(ContextCompat.getColor(textView.getContext(), R.color.colorPrimary));
        }

        if(textView instanceof EditText){
            ((EditText) textView).setSelection(textView.getText().length());
        }

    }





}
