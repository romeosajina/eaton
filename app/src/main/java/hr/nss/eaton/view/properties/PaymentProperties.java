package hr.nss.eaton.view.properties;

import android.content.Context;

import java.util.Arrays;
import java.util.List;

import hr.nss.eaton.R;
import hr.nss.eaton.model.transaction.PaymentMethod;
import hr.nss.eaton.view.payment.CashPayment;
import hr.nss.eaton.view.payment.EtherPayment;
import hr.nss.eaton.view.payment.Payment;

public class PaymentProperties {

    public static void init(){
        EtherPayment.init();
    }
    private static List<PaymentProperties> properties = Arrays.asList(
            new PaymentProperties(R.string.cash,PaymentMethod.CASH),
            new PaymentProperties(R.string.etherium_payment_type, PaymentMethod.ETH)
    );


    public final int nameId;
    public final PaymentMethod paymentMethod;

    private PaymentProperties(int nameId, PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
        this.nameId = nameId;
    }

    public Payment getPayment(Context c){
        if(nameId == R.string.etherium_payment_type)
            return EtherPayment.getInstance(c);
        return CashPayment.getInstance(c);
    }


    public static int indexOf(Payment p){

        if(p instanceof CashPayment) return (0);
        else if(p instanceof EtherPayment) return (1);
        return -1;
    }


    public static List<PaymentProperties> getAll() {
        return properties;
    }
    public static PaymentProperties at(int index){
        return getAll().get(index);
    }

    public static PaymentProperties getDefault(){
        return at(0);
    }

    public static PaymentMethod getPaymentMethod(Payment payment){
        return at(indexOf(payment)).paymentMethod;
    }
    public static PaymentProperties getPaymentProperties(PaymentMethod paymentMethod){
        if(paymentMethod == PaymentMethod.CASH) return at(0);
        if(paymentMethod == PaymentMethod.ETH) return at(1);
        return null;
    }


}
