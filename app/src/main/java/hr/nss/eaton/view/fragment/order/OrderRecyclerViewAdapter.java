package hr.nss.eaton.view.fragment.order;


import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import hr.nss.eaton.R;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.view.fragment.base.EatOnAdapter;
import hr.nss.eaton.view.properties.OrderStatusProperties;
import hr.nss.eaton.view.utils.FormatUtils;

/**
 * Created by Robert on 30-May-18.
 */

public class OrderRecyclerViewAdapter extends EatOnAdapter<Order, OrderRecyclerViewAdapter.ViewHolder> {

    private OrderListFragment.OrderActionListener orderActionListener;

    public void setOrderActionListener(OrderListFragment.OrderActionListener orderActionListener) {
        this.orderActionListener = orderActionListener;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mOrder = data.get(position);
        final Order order = holder.mOrder;
        OrderStatusProperties osp = OrderStatusProperties.getForStatus(order.getStatus());

        holder.mStatusView.setImageResource(osp.icon);
        holder.mStatusView.setColorFilter(osp.color);

        holder.mPlaceView.setText(order.getCompany().getName());

        holder.mDateView.setText(order.getDateTime().toDateTimeString());

        String total = FormatUtils.format(order.getTotal()) + " " + getString(R.string.currency);
        holder.mTotalView.setText(total);


        int size = order.getOrderItemCount();
        String itemsCount = size + " ";

        if(size == 1) size = R.string.item;
        else if(size < 5 ) size = R.string.items;
        else size = R.string.items1;

        itemsCount += getString(size);
        holder.mItemsCountView.setText(itemsCount);


        int visibility = View.INVISIBLE;
        if(osp.buttonVisible) {
            visibility = View.VISIBLE;
            holder.mActionButton.setText(osp.btnText);
            if(orderActionListener != null){
                holder.mActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(orderActionListener.onOrderAction(order)){
                            OrderRecyclerViewAdapter.this.notifyDataSetChanged();
                        }
                    }
                });
            }
        }

        holder.mActionButton.setVisibility(visibility);

        if (null != mListener) {
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onListItemClick(holder.mOrder);
                }
            });
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final ImageView mStatusView;
        public final TextView mPlaceView;
        public final TextView mDateView;
        public final TextView mTotalView;
        public final TextView mItemsCountView;
        public final Button   mActionButton;
        public Order mOrder;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mStatusView = view.findViewById(R.id.order_item_status_img);
            mPlaceView = view.findViewById(R.id.order_item_place_name);
            mDateView = view.findViewById(R.id.order_item_order_date);
            mTotalView = view.findViewById(R.id.order_item_total);
            mItemsCountView = view.findViewById(R.id.order_item_item_count);
            mActionButton = view.findViewById(R.id.order_item_action_btn);
        }

    }
}
