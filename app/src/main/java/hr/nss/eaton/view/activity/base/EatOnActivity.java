package hr.nss.eaton.view.activity.base;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.client.HttpResponseException;
import cz.msebera.android.httpclient.conn.ConnectTimeoutException;
import hr.nss.eaton.R;
import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.data.HttpUtils;
import hr.nss.eaton.model.data.RequestListener;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.view.activity.LoginActivity;
import hr.nss.eaton.view.activity.MainActivity;
import hr.nss.eaton.view.activity.NoInternetActivity;
import hr.nss.eaton.view.activity.OrderDetailsActivity;
import hr.nss.eaton.view.fragment.base.NoDataFragment;
import hr.nss.eaton.view.listener.DataChangedListener;
import hr.nss.eaton.view.listener.EatOnObjectListener;
import hr.nss.eaton.view.notification.AndroidNotification;
import hr.nss.eaton.view.properties.OrderStatusProperties;
import hr.nss.eaton.view.properties.PaymentProperties;
import hr.nss.eaton.view.properties.PreferenceProvider;
import hr.nss.eaton.view.notification.NotificationPusher;
import hr.nss.eaton.view.utils.ComponentUtils;

/**
 * Created by ASUS on 31.5.2018..
 */

public class EatOnActivity extends AppCompatActivity implements DataChangedListener {

    public static final String USER_EMAIL = "UserEmail";
    public static final String USER_PASSWORD = "UserPassword";
    public static final String RECENT_MENU_ITEMS = "RecentMenuItems";
    public static final String RECENT_COMPANIES = "RecentCompanies";


    private boolean backButtonVisible = false;

    protected Toolbar toolbar = null;



    static {
        PaymentProperties.init();
    }


    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(isInternetConnectionAvailable()){
            if (checkUserLoggedIn()) {
                onEatOnCreate(savedInstanceState);
            }

        }
        else
            showNoInternetActivity();

    }
    protected void onEatOnCreate(Bundle savedInstanceState){
    }


    protected String getPrefsString(String key){
        return getPrefsString(key, null);
    }

    protected String getPrefsString(String key, String defaultValue){
        return PreferenceProvider.getString(this,key, defaultValue);
    }


    protected Float getPrefsFloat(String key){
        return getPrefsFloat(key,null);
    }
    protected Float getPrefsFloat(String key, Float defValue){
        return PreferenceProvider.getFloat(this,key, defValue);
    }
    protected Boolean getPrefsBoolean(String key, Boolean defValue){
        return PreferenceProvider.getBoolean(this,key, defValue);
    }

    protected void setPrefsInt(String key, int value){
        PreferenceProvider.putInt(this, key, value);
    }
    protected Integer getPrefsInt(String key){
        return PreferenceProvider.getInt(this, key);
    }


    protected String getUserEmail(){
        return getPrefsString(USER_EMAIL);
    }
    protected String getUserPassword(){
        return getPrefsString(USER_PASSWORD);
    }

    protected boolean checkUserLoggedIn(){

        if(!hasCredentials()){
            LoginActivity.show(this);
            return false;
        }
        else{
            HttpUtils.setAuth(getUserEmail(), getUserPassword());
        }
        return true;
    }
    protected boolean hasCredentials(){
        return getUserEmail() != null && getUserPassword() != null;
    }
    protected void setCredentials(User user){
        PreferenceProvider.putString(this,USER_EMAIL, user.getEmail());
        PreferenceProvider.putString(this,USER_PASSWORD, user.getPassword());
    }

    protected void clearCredentials(){
        PreferenceProvider.putString(this,USER_EMAIL, null);
        PreferenceProvider.putString(this,USER_PASSWORD, null);
    }



    protected void setUserParams(User user){
        Map<String, Object> params = user.getSearchParams();
        PreferenceProvider.put(this,params);
    }


    private List<Long> getRecentMenuItems(){
        return PreferenceProvider.getSavedList(this,RECENT_MENU_ITEMS, Long.class);
    }

    private List<Long> getRecentCompanies(){
        return PreferenceProvider.getSavedList(this,RECENT_COMPANIES, Long.class);
    }

    private List<Long>  eatOnObjectsListIdList(List<? extends EatOnObject> eatOnObjects){
        if(eatOnObjects == null || eatOnObjects.isEmpty())return null;

        List<Long> list = new ArrayList<>();

        for(int i = eatOnObjects.size()-1; i>=0 && eatOnObjects.size()-i<=MainActivity.MAX_RECENT_ITEMS ; i--) {
            list.add(eatOnObjects.get(i).getId());
        }
        return list;
    }

    protected void setRecentMenuItems(List<MenuItem> recentMenuItems) {
        PreferenceProvider.saveList(this,eatOnObjectsListIdList(recentMenuItems), RECENT_MENU_ITEMS);

    }
    protected void setRecentCompanies(List<Company> recentCompanies){
        PreferenceProvider.saveList(this, eatOnObjectsListIdList(recentCompanies), RECENT_COMPANIES);
    }

    private Map<String, Object> getUserSearchParams(){
        Map<String, Object> params = new HashMap<>();

        params.put(User.SEARCH_PARAM_MAX_DISTANCE, getPrefsFloat(User.SEARCH_PARAM_MAX_DISTANCE,500f));
        params.put(User.SEARCH_PARAM_SORT_BY, getPrefsString(User.SEARCH_PARAM_SORT_BY, User.SORT_BY_DELIVERY_TIME));
        params.put(User.SEARCH_PARAM_FOOD_TYPE, getPrefsString(User.SEARCH_PARAM_FOOD_TYPE, User.FOOD_TYPE_ALL));
        params.put(User.SEARCH_PARAM_DELIVERY_MANDATORY, getPrefsBoolean(User.SEARCH_PARAM_DELIVERY_MANDATORY, true));
        params.put(User.SEARCH_PARAM_MAX_DELIVERY_TIME, getPrefsFloat(User.SEARCH_PARAM_MAX_DELIVERY_TIME, 30f));
        params.put(User.SEARCH_PARAM, getPrefsString(User.SEARCH_PARAM, null));

        return  params;
    }

    private void setUserRecentCompanies(User user){

        if (user.getRecentCompanies().isEmpty()) {
            List<Long> cid = getRecentCompanies();
            if(cid != null){
                List<Company> companies = new ArrayList<>();
                for (Long aLong : cid) {
                    Company c = Company.findById(aLong);
                    if(c != null) companies.add(c);
                }
                if (!companies.isEmpty()) {
                    user.setRecentCompanies(companies);
                }
            }
        }
    }
    private void setUserRecentMenuItems(User user){

        if (user.getRecentMenuItems().isEmpty()) {
            List<Long> cid = getRecentMenuItems();
            if(cid != null){
                List<MenuItem> menuItems = new ArrayList<>();
                for (Long aLong : cid) {
                    MenuItem m = MenuItem.findById(aLong);
                    if(m != null) menuItems.add(m);
                }
                if (!menuItems.isEmpty()) {
                    user.setRecentMenuItems(menuItems);
                }
            }
        }
    }

    protected void requestUser(final RequestListener<User> requestListener){
        DataProvider.get().requestUser(new RequestListener<User>() {
            @Override
            public void success(User user) {

                if(user == null){
                    clearCredentials();
                    checkUserLoggedIn();
                    return;
                }

                if(user.getSearchParams() == null)
                    user.setSearchParams(getUserSearchParams());
                setUserRecentCompanies(user);
                setUserRecentMenuItems(user);

                ensurePusher(user);


                requestListener.success(user);
            }

            @Override
            public void failure(Object response, Throwable reason) {
                requestListener.failure(response, reason);
            }
        }, getUserEmail(), getUserPassword());
    }


    protected void showNoInternetActivity(){
        startActivity(new Intent(this, NoInternetActivity.class));
    }

    protected boolean isInternetConnectionAvailable() {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }


    protected void setupToolbarAndBackButton(){
        setupToolbar();
        enableBackButton();
    }

    protected void setupToolbar(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    protected void enableBackButton(){
        backButtonVisible = true;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @Override
    public boolean onSupportNavigateUp() {
        if(backButtonVisible)onBackPressed();
        return super.onSupportNavigateUp();
    }

    public void showToast(String msg){
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }


    protected void setFragment(Fragment f){

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        //transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);

        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        transaction.replace(getFragmentContainer(), f);

//        transaction.addToBackStack(null);

        transaction.commit();
    }

    protected int getFragmentContainer(){
        throw new IllegalArgumentException("Method must be implemented if using method setFragment");
    }

    protected <T extends Object> T resolveParam(String key, Bundle savedInstanceState) {

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras != null && extras.containsKey(key)) {
               return (T)extras.get(key);
            }

        } else if(savedInstanceState.containsKey(key)){
            return (T)savedInstanceState.get(key);
        }
        return null;
    }

    @Override
    public void dataChanged(List newData) {

        /*
        if(noDataView != null){
            if(newData != null && newData.size()>0) noDataView.setVisibility(View.INVISIBLE);
            else noDataView.setVisibility(View.VISIBLE);
        }
        */
    }

    protected void showNoDataView(){
        setFragment(NoDataFragment.newInstance());
    }

    protected void setImageView(ImageView imageView, EatOnImage eatOnImage, boolean openFullScreen){
        ComponentUtils.setImageView(imageView, eatOnImage, openFullScreen);

    }
    protected void setImageView(ImageView imageView, EatOnImage eatOnImage){
        setImageView(imageView, eatOnImage, false);
    }
    protected void setImageView(int id, EatOnImage eatOnImage){
        setImageView(id, eatOnImage, false);
    }
    protected void setImageView(int id, EatOnImage eatOnImage, boolean openFullScreen){
        setImageView((ImageView) findViewById(id), eatOnImage,openFullScreen);
    }

    protected void setTextView(int id, String text){
        ((TextView) findViewById(id)).setText(text);
    }

    protected void setTextView(int id, String text, View.OnClickListener  onClickListener){
        TextView view = findViewById(id);
        view.setText(text);
        view.setOnClickListener(onClickListener);
    }

    protected void returnToMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    protected void showNotification(int resId){
        showNotification(getString(resId));
    }
    protected void showNotification(String text){
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
        Snackbar.make(viewGroup, text, Snackbar.LENGTH_LONG).show();
    }


    protected void showToast(int resId){
        Toast.makeText(this, resId, Toast.LENGTH_LONG).show();
    }

    protected void handleRequestFailure(Object response, Throwable reason){
        EatOnObject.loge("HANDLING REQUEST FAILURE " + response + " " + reason);

        if(reason instanceof ConnectTimeoutException || reason instanceof ConnectException
                 || reason instanceof HttpResponseException
                || (reason != null && reason.getClass().getSimpleName().contains("Connect"))){
            showNotification(R.string.unable_to_establish_connection_to_server);
        }


        //TODO: handle request failure



    }

    protected void showProgressDialog(int msg){
        showProgressDialog(R.string.wait, msg);
    }
    private ProgressDialog dialog;
    protected void showProgressDialog(int title, int msg){
        dismissProgressDialog();

        dialog = ProgressDialog.show(this,
                getString(title), getString(msg), true);

    }
    protected void dismissProgressDialog(){
        if(dialog != null)dialog.dismiss();
        dialog = null;
    }



    private void ensurePusher(User user){
        if(!NotificationPusher.isConnected()){
            NotificationPusher.connect();
            NotificationPusher.bindOrdersChannel((int) user.getId(), order -> handleOrderEvent(order));
        }
    }
    protected void registerOrderListener(EatOnObjectListener<Order> orderListener){
        NotificationPusher.registerOrderListener(orderListener);
    }

    protected void unregisterOrderListener(EatOnObjectListener<Order> orderListener) {
        NotificationPusher.unregisterOrderListener(orderListener);
    }


    private void handleOrderEvent(final Order order){
        requestUser(new RequestListener<User>() {
            @Override
            public void success(User user) {
                Order o = user.findOrderById(order.getId());

                if(o != null){
                    o.setStatus(order.getStatus());
                    o.setDeliveryTime(order.getDeliveryTime());
                    showOrderNotification(o);
                    NotificationPusher.callOrderListeners(o);
                }
            }
            @Override
            public void failure(Object response, Throwable reason) {
                handleRequestFailure(response,reason);
            }
        });
    }

    private void showOrderNotification(final Order order){
        runOnUiThread(() -> {
            String ct = order.getCompany().getName() + " - " +
                    getString(OrderStatusProperties.getForStatus(order.getStatus()).text);

            AndroidNotification.showOrderNotification(this, (int) order.getId(),
                    getString(R.string.order_updated), ct);
        });
    }







    protected boolean validateField(EditText field, boolean valid, int resId){
        if(valid){
            field.setError(null);
        }
        else{
            field.setError(getString(resId));
            field.requestFocus();
        }
        return valid;
    }
    protected boolean validateRequiredField(EditText field){
        return validateField(field, !TextUtils.isEmpty(field.getText()), R.string.required_field);
    }



}
