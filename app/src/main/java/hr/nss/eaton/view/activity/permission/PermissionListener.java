package hr.nss.eaton.view.activity.permission;

public interface PermissionListener extends GrantedPermissionListener{
    void denied();
}
