package hr.nss.eaton.model.menu;

/**
 * Created by Robert on 31-Oct-18.
 */

public enum MenuType {

    VEGAN(0, "V"),
    MEAT(1, "M"),
    PIZZA(2, "P"),
    BURGER(3,"B"),
    SIDES(4,"S"),
    DRINKS(5, "D"),
    PANCAKES(6, "PC"),
    DESERT(7, "DST"),
    SALAD(8, "SLD"),
    FISH(9, "F"),
    CHINESE(10, "CHS"),
    CHINESE_WITH_CHICKEN(10, "CHSWC"),
    CHINESE_WITH_PORK(10, "CHSWP"),
    CHINESE_WITH_BEEF(10, "CHSWB"),
    CHINESE_WITH_VEGETABLES(10, "CHSWVEG"),
    SOUP(11, "SP"),
    MEXICAN(11, "MXN"),
    GYROS(11, "GY"),
    HAMBURGER(1, "HMB"),
    LASAGNE(1, "LSG"),
    SANDWICH(1, "SNW");


    private int value;
    private String letter;
    MenuType(int val, String let){
        this.value = val;
        this.letter = let;
    }

    public int getValue() {
        return value;
    }

    public static MenuType find(String letter) {
        for (MenuType menuType : values()) {
            if(menuType.letter.equals(letter))return menuType;
        }
        return VEGAN;
    }

    public static MenuType valueOf(int value){
        MenuType[] values = values();
        if(values.length>value && value >= 0){
            return values[value];
        }
        return values[0];
    }

}
