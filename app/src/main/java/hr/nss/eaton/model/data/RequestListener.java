package hr.nss.eaton.model.data;

/**
 * Created by Robert on 13-Dec-18.
 */

public interface RequestListener<T> {

    void success(T t);
    void failure(Object response, Throwable reason);

}
