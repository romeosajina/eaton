package hr.nss.eaton.model.data;

import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Robert on 24-Dec-18.
 */

public class EatOnJsonHttpResponseHandler extends JsonHttpResponseHandler {


    static void log(Object value ){
        Log.d("JsonHandler", value + "");
    }
    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
        log("1. " + statusCode + " " + response);
        onSuccess(response);
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {

        log("2. " + statusCode + " " + response);
        try {
            JSONObject j = new JSONObject();
            j.put("items", response);
            onSuccess(j);
        } catch (JSONException e) {
            onSuccess(null);
        }
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String responseString) {
        log("3. " + statusCode + " " + responseString);
        try {
            onSuccess(new JSONObject(responseString));
        } catch (JSONException e) {
            onSuccess(null);
        }

    }

    public void onSuccess(JSONObject response){

    }

    public void onFailure(Object response, Throwable reason){
        log("Failure: " + response + " " + reason.getClass().getSimpleName() + ": " + reason.getMessage());
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
        log("4. " + statusCode + " " + errorResponse + " " + " " + throwable.getClass().getSimpleName() + ": " + throwable.getMessage());
        onFailure(errorResponse, throwable);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        log("5. " + statusCode + " " + responseString + " " + " " + throwable.getClass().getSimpleName() + ": " + throwable.getMessage());
        onFailure(responseString, throwable);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        log("6. " + statusCode + " " + errorResponse + " " + " " + throwable.getClass().getSimpleName() + ": " + throwable.getMessage());
        onFailure(errorResponse, throwable);
    }
}
