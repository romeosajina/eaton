package hr.nss.eaton.model.menu;

import java.util.List;

import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.common.PricedEatOnObject;

/**
 * Created by Robert on 01-Nov-18.
 */

public class MenuItemOption extends PricedEatOnObject {


    public static final MenuItemOption findById(long id){
        return findById(MenuItemOption.class, id);
    }



    protected MenuItem menuItem;
    protected long menuItemId;

    protected List<MenuItemOptionSupplement> menuItemOptionSupplements;


    public MenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
        refObjectSet(MenuItem.class, menuItem);
    }

    public long getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(long menuItemId) {
        this.menuItemId = menuItemId;
        refObjectSet(MenuItem.class, menuItemId);
    }

    public List<MenuItemOptionSupplement> getMenuItemOptionSupplements() {
        return menuItemOptionSupplements;
    }

    public void setMenuItemOptionSupplements(List<MenuItemOptionSupplement> menuItemOptionSupplements) {
        this.menuItemOptionSupplements = menuItemOptionSupplements;
    }
}
