package hr.nss.eaton.model.menu;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.common.PricedEatOnObject;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.transaction.OrderItem;

/**
 * Created by Robert on 28-May-18.
 */

public class MenuItem extends PricedEatOnObject {

    public static final MenuItem findById(long id){
        return findById(MenuItem.class, id);
    }



    protected long menuId;
    protected float rate;
    protected double discount;
    protected String description;

    protected Menu menu;
    protected List<MenuItemOption> menuItemOptions;

    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
        refObjectSet(Menu.class, menuId);
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
        refObjectSet(Menu.class, menu);
    }

    public List<MenuItemOption> getMenuItemOptions() {
        if(menuItemOptions == null) menuItemOptions = new ArrayList<>();
        return menuItemOptions;
    }

    public void setMenuItemOptions(List<MenuItemOption> menuItemOptions) {
        this.menuItemOptions = menuItemOptions;
    }

    public boolean hasMenuItemOptionsOptions(){
        return getMenuItemOptions().size() > 0;
    }
    public List<MenuItemOptionSupplement> getMenuItemOptionSupplements(MenuItemOption menuItemOption){
        //if(!hasMenuItemOptionsOptions())
         //   return getMenuItemOptions().get(0).getMenuItemOptionSupplements();
        return menuItemOption.getMenuItemOptionSupplements();
    }




}
