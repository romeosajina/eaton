package hr.nss.eaton.model.data;

import androidx.appcompat.view.menu.MenuItemImpl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Map;

import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.menu.Menu;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.transaction.Reservation;
import hr.nss.eaton.model.transaction.Status;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.EatOnEntity;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.utils.ReflectionUtils;

/**
 * Created by Robert on 29-May-18.
 */

public class RESTDataProvider extends DataProvider {

    @Override
    public User getUser() {
        return userInstance;
    }

    @Override
    public List<Company> getCompanies() {
        return null;
    }

    @Override
    public void requestCompanies(final RequestListener<List<Company>> requestListener) {

        // EatOnObject.log("requestCompanies ");

        if(EatOnObject.getAllInstances(Company.class) != null){
            requestListener.success((List<Company>) EatOnObject.getAllInstances(Company.class));
            return;
        }

        HttpUtils.get(HttpUtils.COMPANIES, null, new EatOnJsonHttpResponseHandler(){
            @Override
            public void onSuccess(JSONObject response) {
                requestListener.success((List<Company>) parseJSON(Company.class,response));
            }

            @Override
            public void onFailure(Object response, Throwable reason) {
                requestListener.failure(response, reason);
            }
        });
    }

    @Override
    public void requestCompanies(final RequestListener<List<Company>> requestListener, final Map<String, Object> params) {

        // EatOnObject.log("requestCompanies params");

        requestCompanies(new RequestListener<List<Company>>() {
            @Override
            public void success(List<Company> companies) {
                requestListener.success(filterCompanies(companies,params));
            }

            @Override
            public void failure(Object response, Throwable reason) {
                requestListener.failure(response,reason);
            }
        });
    }

    @Override
    public void requestCompany(final RequestListener<Company> requestListener, final Long id) {

        // EatOnObject.log("requestCompany ");

        Company c = Company.findById(id);
        if(c != null) requestListener.success(c);
        else{
            requestCompanies(new RequestListener<List<Company>>() {
                @Override
                public void success(List<Company> companies) {
                    Company c = Company.findById(id);
                    if(c != null) requestListener.success(c);
                    else requestListener.failure(null, null);
                }

                @Override
                public void failure(Object response, Throwable reason) {
                    requestListener.failure(response, reason);
                }
            });


        }
    }

    @Override
    public void requestMenus(RequestListener<List<Menu>> requestListener, Company company) {

    }

    @Override
    public void requestOrders(final RequestListener<List<Order>> requestListener) {

        // EatOnObject.log("requestOrders ");


        if(EatOnObject.getAllInstances(Order.class) != null){
            requestListener.success((List<Order>) EatOnObject.getAllInstances(Order.class));
            return;
        }

        HttpUtils.get(HttpUtils.ORDERS, null,new EatOnJsonHttpResponseHandler(){
            @Override
            public void onSuccess(JSONObject response) {
                List<Order> orders = (List<Order>) parseJSON(Order.class, response);
                requestListener.success(orders);
            }

            @Override
            public void onFailure(Object response, Throwable reason) {
                requestListener.failure(response, reason);
            }
        });
    }

    @Override
    public void requestMenuItems(final RequestListener<List<MenuItem>> requestListener, final Map<String, Object> params) {


        // EatOnObject.log("requestMenuItems params ");

        requestCompanies(new RequestListener<List<Company>>() {
            @Override
            public void success(List<Company> companies) {
                requestListener.success(
                        filterMenuItems((List<MenuItem>) EatOnObject.getAllInstances(MenuItem.class),
                                params));
            }

            @Override
            public void failure(Object response, Throwable reason) {
                requestListener.failure(response,reason);
            }
        });
    }

    @Override
    public void requestReservations(RequestListener<List<Reservation>> requestListener, EatOnEntity eatOnEntity) {

    }


    @Override
    public void requestUser(final RequestListener<User> requestListener, final String email, final String password) {

        // EatOnObject.log("requestUser " + email + " " + password);


        if(userInstance != null){
            requestListener.success(userInstance);
            return;
        }
        try {
         //   final String password = encrypt(pass);

            JSONObject j = new JSONObject();

            j.put("email", email);
            j.put("password", password);

            HttpUtils.post(HttpUtils.AUTH, j, new EatOnJsonHttpResponseHandler() {
                @Override
                public void onSuccess(JSONObject response) {
                    if (response == null || response.names() == null) {
                        requestListener.success(null);
                    } else {
                        userInstance = parseObject(response, User.class);
                        userInstance.setEmail(email);
                        userInstance.setPassword(password);

                        HttpUtils.setAuth(email, password);

                        requestCompanies(new RequestListener<List<Company>>() {
                            @Override
                            public void success(List<Company> companies) {

                                requestOrders(new RequestListener<List<Order>>() {
                                    @Override
                                    public void success(List<Order> orders) {
                                        userInstance.setOrders(orders);
                                        requestListener.success(userInstance);
                                    }

                                    @Override
                                    public void failure(Object response, Throwable reason) {
                                        requestListener.failure(response, reason);
                                    }
                                });

                            }

                            @Override
                            public void failure(Object response, Throwable reason) {
                                requestListener.failure(response, reason);
                            }
                        });

                    }
                }

                @Override
                public void onFailure(Object response, Throwable reason) {
                    requestListener.failure(response, reason);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
            EatOnObject.loge(e.getClass().getSimpleName()+": "+e.getMessage());
            requestListener.failure(null, e);
        }
    }

    static JSONObject userToJSON(User user){
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("id", user.getId());
            jsonObject.put("name", user.getName());
            jsonObject.put("surname", user.getSurname());
            jsonObject.put("email", user.getEmail());
            jsonObject.put("city", user.getCity());
            jsonObject.put("address", user.getAddress());
            jsonObject.put("latitude", user.getLatitude());
            jsonObject.put("longitude", user.getLongitude());
            jsonObject.put("phone", user.getPhone());
            jsonObject.put("showNumber", user.isShowNumber());
            jsonObject.put("password", user.getPassword());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
    @Override
    public void registerUser(final User user, final RequestListener<User> requestListener) {


        // EatOnObject.log("registerUser " );


        user.setPassword(encrypt(user.getPassword()));

        HttpUtils.post(HttpUtils.USER,userToJSON(user), new EatOnJsonHttpResponseHandler(){
            @Override
            public void onSuccess(JSONObject response) {

                User u = parseObject(response, User.class);

                if(u != null && u.getId() != 0){
                    userInstance = u;
                    u.setEmail(user.getEmail());
                    u.setPassword(user.getPassword());
                    HttpUtils.setAuth(user.getEmail(), user.getPassword());
                }

                requestListener.success(u);
            }

            @Override
            public void onFailure(Object response, Throwable reason) {
                requestListener.failure(response,reason);
            }
        });

    }

    @Override
    public void updateUser(final User user, final RequestListener<User> requestListener) {

        // EatOnObject.log("updateUser " );

        HttpUtils.patch(HttpUtils.USER_BY_ID+user.getId(), userToJSON(user), new EatOnJsonHttpResponseHandler(){
            @Override
            public void onSuccess(JSONObject response) {
                requestListener.success(user);
            }

            @Override
            public void onFailure(Object response, Throwable reason) {
                requestListener.failure(response, reason);
            }
        });
    }

    @Override
    public void deleteOrder(final Order order, final RequestListener<Order> requestListener) {

        // EatOnObject.log("deleteOrder " );

        String url = HttpUtils.ORDERS_BY_ID + order.getId() + HttpUtils.DELETE_ALL_CHILDREN;

        HttpUtils.delete(url, new EatOnJsonHttpResponseHandler(){
            @Override
            public void onSuccess(JSONObject response) {
                EatOnObject.eatOnObjectRemoved(order);
                requestListener.success(order);
            }

            @Override
            public void onFailure(Object response, Throwable reason) {
                requestListener.failure(response,reason);
            }
        });
    }

    @Override
    public void postOrder(final Order order, final RequestListener<Order> requestListener) {

        // EatOnObject.log("postOrder " );

        JSONObject post = buildPost(order, OPERATION_CREATE);

        if(post == null)
            requestListener.failure(null, null);
        else{
            HttpUtils.post(HttpUtils.BATCH, post,new EatOnJsonHttpResponseHandler(){
                @Override
                public void onSuccess(JSONObject response) {
                    String error = getBatchError(response);

                    if(error != null) requestListener.failure(error, null);
                    else{

                        Long id = getMasterId(response);
                        if(id == null) requestListener.failure(null, null);
                        else{
                            //id-evi orderitema i supplementa
                            // se ne popunjavaju ali za sad ni ne rabe
                            order.setId(id);
                            requestListener.success(order);
                        }
                    }
                }

                @Override
                public void onFailure(Object response, Throwable reason) {
                    requestListener.failure(response, reason);
                }
            });
        }
    }


    @Override
    public void requestOrder(long id, final RequestListener<Order> requestListener) {
        String url = HttpUtils.ORDERS_BY_ID + id;

        HttpUtils.get(url, null, new EatOnJsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject response) {
                requestListener.success((Order) parseObject(response, Order.class));
            }

            @Override
            public void onFailure(Object response, Throwable reason) {
                requestListener.failure(response, reason);
            }
        });

    }

    @Override
    public String getServerUrlPrefix() {
        return HttpUtils.BASE_URL;
    }

    @Override
    public List<Company> getCompanies(Map<String, Object> params) {
        return null;
    }

    @Override
    public Company getCompany(long id) {
        return null;
    }

    @Override
    public List<Menu> getMenus(Company company) {
        return null;
    }

    @Override
    public List<Reservation> getReservations(EatOnEntity eatOnEntity) {
        return null;
    }

    @Override
    public List<Order> getOrders(EatOnEntity eatOnEntity) {
        return null;
    }

    @Override
    public List<MenuItem> getMenuItems(Map<String, Object> params) { return null;}



    static String getPath(Class clazz) {
        return HttpUtils.BASE_URL+classCreatePath.get(clazz);
    }

    static String getBatchError(JSONObject response){
        try {
            JSONArray array = response.getJSONArray("parts");

            for (int i = 0; i < array.length(); i++) {

                JSONObject item = array.getJSONObject(i);
                if(!item.isNull("error")) return item.getString("error");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
    static Long getMasterId(JSONObject response){

        try {
            JSONArray array = response.getJSONArray("parts");
            return array.getJSONObject(0).getJSONObject("payload").getLong("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    static void buildPart(EatOnObject eoo, String operation,JSONArray jsonArray,Integer parentPartId) throws JSONException {

        Class clazz = eoo.getClass();

        JSONObject part = new JSONObject();
        jsonArray.put(part);

        Integer partId = parentPartId == null ? 0 : parentPartId + 1;
        part.put("id", partId.toString());
        part.put("operation", operation);
        part.put("path", getPath(clazz));

        JSONObject payload = new JSONObject();
        part.put("payload", payload);


        for (Field field : ReflectionUtils.getFields(clazz)) {

            if (Modifier.isStatic(field.getModifiers())) continue;

            Object value = ReflectionUtils.getFieldValue(eoo, field);


            if (value instanceof EatOnObject) continue;

            String name = field.getName();
            Class fieldClass = field.getType();


            //ako je to detalj onda se refId na master stavi na -1
            // id-evi se isto stavljaju na -1
            if ((name.equals("id") || name.equals(masterDetailClasses.get(clazz)) &&
                    OPERATION_CREATE.equals(operation))) {
                value = -1;
            } else if (fieldClass.isEnum()) {
                value = value.toString();
            }

            if (value instanceof List) {
                for (Object o : ((List) value)) {
                    buildPart((EatOnObject) o, operation, jsonArray, partId);
                }
            } else {
                payload.put(name, value);
            }


            // EatOnObject.log("POST: " + name + " = " + value);
        }


        part.put("parentPartId", parentPartId == null ? null : parentPartId.toString());

    }
    static JSONObject buildPost(EatOnObject eoo, String operation){

        try {
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            jsonObject.put("parts", jsonArray);

            buildPart(eoo, operation, jsonArray,null);

            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
            EatOnObject.loge(e.getClass().getSimpleName() + ": " + e.getMessage());
        }
        return null;
    }

}
