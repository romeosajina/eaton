package hr.nss.eaton.model.menu;

import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.common.PricedEatOnObject;

/**
 * Created by Robert on 01-Nov-18.
 */

public class MenuItemOptionSupplement extends PricedEatOnObject {

    public static final MenuItemOptionSupplement findById(long id){
        return findById(MenuItemOptionSupplement.class, id);
    }

    protected long menuItemOptionId;
    protected MenuItemOption menuItemOption;

    public long getMenuItemOptionId() {
        return menuItemOptionId;
    }

    public void setMenuItemOptionId(long menuItemOptionId) {
        this.menuItemOptionId = menuItemOptionId;
        refObjectSet(MenuItemOption.class, menuItemOptionId);
    }

    public MenuItemOption getMenuItemOption() {
        return menuItemOption;
    }

    public void setMenuItemOption(MenuItemOption menuItemOption) {
        this.menuItemOption = menuItemOption;
        refObjectSet(MenuItemOption.class, menuItemOption);
    }
}
