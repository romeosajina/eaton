package hr.nss.eaton.model.transaction;

import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.menu.MenuItemOptionSupplement;

/**
 * Created by Robert on 01-Nov-18.
 */

public class OrderItemSupplement extends EatOnObject {

    protected long orderItemId;
    protected long menuItemOptionSupplementId;
    protected OrderItem orderItem;
    protected MenuItemOptionSupplement menuItemOptionSupplement;

    public long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(long orderItemId) {
        this.orderItemId = orderItemId;
        refObjectSet(OrderItem.class, orderItemId);
    }

    public long getMenuItemOptionSupplementId() {
        return menuItemOptionSupplementId;
    }

    public void setMenuItemOptionSupplementId(long menuItemOptionSupplementId) {
        this.menuItemOptionSupplementId = menuItemOptionSupplementId;
        refObjectSet(MenuItemOptionSupplement.class, menuItemOptionSupplementId);
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
        refObjectSet(OrderItem.class, orderItem);
    }

    public MenuItemOptionSupplement getMenuItemOptionSupplement() {
        return menuItemOptionSupplement;
    }

    public void setMenuItemOptionSupplement(MenuItemOptionSupplement menuItemOptionSupplement) {
        this.menuItemOptionSupplement = menuItemOptionSupplement;
        refObjectSet(MenuItemOptionSupplement.class, menuItemOptionSupplement);
    }
}
