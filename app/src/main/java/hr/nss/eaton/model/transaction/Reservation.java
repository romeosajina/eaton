package hr.nss.eaton.model.transaction;


import hr.nss.eaton.model.common.EatOnTime;
import hr.nss.eaton.model.user.Company;

/**
 * Created by Robert on 28-May-18.
 */

public class Reservation extends Transaction {

    public static final Reservation findById(Long id){
        return findById(Reservation.class, id);
    }

}
