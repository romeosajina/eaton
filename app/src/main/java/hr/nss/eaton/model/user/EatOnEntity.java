package hr.nss.eaton.model.user;

import java.util.ArrayList;
import java.util.List;

import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.transaction.Reservation;
import hr.nss.eaton.model.transaction.Status;

/**
 * Created by Robert on 28-May-18.
 */

public abstract class EatOnEntity extends EatOnObject {

    protected String name;
    protected String city;
    protected String address;
    protected double latitude;
    protected double longitude;
    protected String phone;


    protected String email;
    protected EatOnImage photo;

    protected String ethAddress;


    protected List<Reservation> reservations;
    protected List<Order> orders;


    public EatOnEntity(){

    }

    public EatOnEntity(Long id, String name,
                       String city, String address,
                       double latitude, double longitude,
                       String phone, EatOnImage photo,
                       List<Reservation> reservations, List<Order> orders) {
        super(id);
        this.name = name;
        this.city = city;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phone = phone;
        this.photo = photo;
        this.reservations = reservations;
        this.orders = orders;
    }

    public EatOnEntity(Long id, String name, String city, String address, double latitude, double longitude) {
       this(id, name, city, address, latitude, longitude, null, null, null, null);
    }

    public EatOnEntity(Long id, String name, String city, String address, double latitude, double longitude, String phoneNumber, EatOnImage photo) {
        this(id, name, city, address, latitude, longitude, phoneNumber, photo, null, null);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public EatOnImage getPhoto() {
        return photo;
    }

    public void setPhoto(EatOnImage photo) {
        this.photo = photo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getEthAddress() {
        return ethAddress;
    }

    public void setEthAddress(String ethAddress) {
        this.ethAddress = ethAddress;
    }

    public List<Reservation> getReservations() {
        if(reservations == null)
            reservations = DataProvider.get().getReservations(this);
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public List<Order> getOrders() {
        if(orders == null){
            orders = DataProvider.get().getOrders(this);
        }
        return orders;
    }
    public Order findOrderById(long id){
        for (Order order : getOrders()) {
            if(order.getId() == id) return order;
        }
        return null;
    }


    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void addOrder(Order order){
        getOrders().add(order);
    }
    public void addReservation(Reservation reservation){
        getReservations().add(reservation);
    }


    public List<Order> getPendingOrders(){
        List<Order> pending = new ArrayList<>();

        for (Order order : getOrders()) {
            Status status = order.getStatus();

            if (status == Status.INITIALIZED ||
                    status == Status.ORDERED ||
                    status == Status.CONFIRMED ||
                    status == Status.PROCESSING ||
                    status == Status.FINISHED) {
                pending.add(order);
            }
        }
        return pending;
    }
    public List<Order> getCompletedOrders(){
        List<Order> completed = new ArrayList<>();
        for (Order order : getOrders()) {
            if(order.getStatus() == Status.DELIVERED) completed.add(order);
        }
        return completed;
    }



 /*
    public OrderItem findOrderItemById(Long orderItemId){
        return OrderItem.findById(orderItemId);

       OrderItem item = null;

        for(Order o : getNewOrders()){
            item = o.findOrderItemById(orderItemId);
            if(item != null)
                break;
        }

        return item;
    }
        */




}

