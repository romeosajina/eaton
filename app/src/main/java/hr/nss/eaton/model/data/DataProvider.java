package hr.nss.eaton.model.data;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Ref;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.menu.Menu;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.menu.MenuType;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.transaction.OrderItem;
import hr.nss.eaton.model.transaction.OrderItemSupplement;
import hr.nss.eaton.model.transaction.Reservation;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.EatOnEntity;
import hr.nss.eaton.model.user.User;
import hr.nss.eaton.utils.ReflectionUtils;

/**
 * Created by Robert on 28-May-18.
 */

public abstract class DataProvider {

    public static final String OPERATION_CREATE="create";
    static User userInstance;
    private static DataProvider instance = new RESTDataProvider();

    public static DataProvider get() {
        return instance;
    }

    public abstract User getUser();

    public abstract List<Company> getCompanies();

    public abstract List<Company> getCompanies(Map<String, Object> params);

    public abstract Company getCompany(long id);

    public abstract List<Menu> getMenus(Company company);

    public abstract List<Reservation> getReservations(EatOnEntity eatOnEntity);

    public abstract List<Order> getOrders(EatOnEntity eatOnEntity);

    public abstract List<MenuItem> getMenuItems(Map<String, Object> params);


    /*                      RequestListener                           */
    public abstract void requestCompanies(RequestListener<List<Company>> requestListener);
    public abstract void requestCompanies(RequestListener<List<Company>> requestListener, Map<String, Object> params);
    public abstract void requestCompany(RequestListener<Company> requestListener, Long id);
    public abstract void requestMenus(RequestListener<List<Menu>> requestListener, Company company);
    public abstract void requestOrders(RequestListener<List<Order>> requestListener);
    public abstract void requestMenuItems(RequestListener<List<MenuItem>> requestListener, Map<String, Object> params);
    public abstract void requestReservations(RequestListener<List<Reservation>> requestListener, EatOnEntity eatOnEntity);
    public abstract void requestUser(RequestListener<User> requestListener, String email, String password);

    public abstract void registerUser(User user, RequestListener<User> requestListener);
    public abstract void updateUser(User user, RequestListener<User> requestListener);

    public abstract void deleteOrder(Order order, RequestListener<Order> requestListener);
    public abstract void postOrder(Order order, RequestListener<Order> requestListener);
    public abstract void requestOrder(long id, RequestListener<Order> requestListener);

    public String getServerUrlPrefix(){
        return "";
    }


    protected static List<? extends EatOnObject> parseJSON(Class clazz, String jsonText) {
        try {

            //Class clazz = Menu.class;
            JSONObject obj = new JSONObject(jsonText);


            return parseJSON(clazz, obj);
        } catch (JSONException e) {
            EatOnObject.loge(e.getClass().getSimpleName() + ": " + e.getMessage());
        }
        return null;
    }

    protected  static List<? extends EatOnObject> parseJSON(Class clazz, JSONObject obj){
        try {


            //  EatOnObject.log("Parsing json list: " + clazz);

            JSONArray items = obj.getJSONArray("items");
            return parseList(items, clazz);
        } catch (JSONException e) {
            EatOnObject.loge(e.getClass().getSimpleName() + ": " + e.getMessage());
        }
        return null;
    }


    protected static List parseList(JSONArray jsonArray, Class clazz) {
        List list = new ArrayList();

        try {
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject item = jsonArray.getJSONObject(i);
                list.add(parseObject(item, clazz));

            }
        } catch (JSONException e) {
            EatOnObject.loge(e.getClass().getSimpleName() + ": " + e.getMessage());
            return null;
        }
        //  EatOnObject.log("Parsed list: "+list);

        return list;
    }
    public static <T extends EatOnObject> T parseObject(String json, Class clazz){
        try {
            return parseObject(new JSONObject(json), clazz);
        } catch (JSONException e) {
            e.printStackTrace();
            EatOnObject.loge(e);
        }
        return null;
    }
    protected static <T extends EatOnObject> T parseObject(JSONObject jsonObject, Class clazz) {
        Object instance = null;
        try {

          //  EatOnObject.log("Parsing object: " + clazz+" " + jsonObject);

            instance = clazz.newInstance();

            ((EatOnObject) instance).setId((jsonObject.getLong("id")));
            for (Field field : ReflectionUtils.getFields(clazz)) {

                if (Modifier.isStatic(field.getModifiers())) continue;

                String name = field.getName();
                //  EatOnObject.loge("FIELD NAME " + name);

                if (!jsonObject.has(name) || jsonObject.isNull(name))
                    continue;

                Object jsonValue = jsonObject.get(name);

                //  EatOnObject.log(name + "  " + jsonValue.getClass());
                Object value;

                // Field field = clazz.getField(javaName);
                Class fieldClass = field.getType();

                if (jsonValue instanceof JSONArray) {
                    Class listTypeClass = ReflectionUtils.getParameterizedTypeClass(field);
                    value = parseList((JSONArray) jsonValue, listTypeClass);
                    //    EatOnObject.log(("LIST PARSED " + ((List) value).size()));

                } else if (fieldClass.isEnum()) {
                    value = fieldClass.getMethod("find", String.class)
                            .invoke(null, jsonValue.toString());
                } else {
                    if (fieldClass.isPrimitive()) {
                        fieldClass = wrapperClasses.get(fieldClass);
                        jsonValue = jsonValue != null ? jsonValue.toString() : null;
                    }
                    //      EatOnObject.log(fieldClass);
                    value = fieldClass.getConstructor(String.class).newInstance(jsonValue);
                }

                ReflectionUtils.setFieldValue(field, instance, value);

                //     EatOnObject.log(clazz.getSimpleName() + " " + name);

                if (name.endsWith("Id"))
                    bindRefObject(instance, name.replace("Id", ""), value);

                //      EatOnObject.log(name + "  " + field.get(instance));
            }

            return (T) instance;

        } catch (JSONException | IllegalAccessException | InstantiationException  | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
            EatOnObject.loge(e.getClass().getSimpleName() + ": " + e.getMessage());

            //   EatOnObject.log(clazz.getSimpleName() + " " + clazz.getDeclaredFields().length);
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            EatOnObject.loge("Exception: " + e.getClass()+" " + e.getMessage());
        }
        EatOnObject.eatOnObjectRemoved((EatOnObject)instance);
        //  EatOnObject.loge("RETURNING NULL " + clazz);

        return null;
    }
    /*
    protected static <T extends EatOnObject> T parseObject(JSONObject jsonObject, Class clazz) {
        Object instance = null;
        try {

            instance = clazz.newInstance();

            //EatOnObject.eatOnObjectAdded((EatOnObject) instance);
            ((EatOnObject) instance).setId((jsonObject.getLong("id")));

            Iterator<String> keys = jsonObject.keys();

            while (keys.hasNext()) {

                String name = keys.next();
                String javaName = toJavaName(name);

                Object jsonValue = jsonObject.get(name);

                if(jsonObject.isNull(name))continue;

                EatOnObject.log(javaName+"  "+ jsonValue.getClass());

                Object value;

               // Field field = clazz.getField(javaName);
                Field field = ReflectionUtils.getField(clazz, javaName);
                Class fieldClass = field.getType();

                if (jsonValue instanceof JSONArray) {
                    Class listTypeClass = ReflectionUtils.getParameterizedTypeClass(field);
                    value = parseList((JSONArray) jsonValue, listTypeClass);
                    //   EatOnObject.log(("LIST PARSED "+((List)value).size()));

                } else if (fieldClass.isEnum()) {
                    value = fieldClass.getMethod("find", String.class)
                            .invoke(null, jsonValue.toString());
                }
                else {
                    if(fieldClass.isPrimitive()){
                        fieldClass = wrapperClasses.get(fieldClass);
                        jsonValue  = jsonValue != null ? jsonValue.toString() : null;
                    }
                    EatOnObject.log(fieldClass);
                    value = fieldClass.getConstructor(String.class).newInstance(jsonValue);
                }

                field.setAccessible(true);
                field.set(instance, value);
                EatOnObject.log(clazz.getSimpleName()+" "+ name);

                if (javaName.endsWith("Id"))
                    bindRefObject(instance, javaName.replace("Id", ""), value);

                   EatOnObject.log(javaName + "  " + field.get(instance));
            }

            return (T) instance;

        } catch (JSONException | IllegalAccessException | InstantiationException | NoSuchFieldException | NoSuchMethodException | InvocationTargetException e) {
            EatOnObject.log(e.getClass().getSimpleName() + ": " + e.getMessage());

         //   EatOnObject.log(clazz.getSimpleName() + " " + clazz.getDeclaredFields().length);
            e.printStackTrace();
        } catch (Exception e) {
            EatOnObject.log("Exception: " + e.getMessage());
        }
        EatOnObject.eatOnObjectRemoved((EatOnObject)instance);
        return null;
    }

*/

    private static void bindRefObject(Object instance, String name, Object id) {
        //try
        {

            Class clazz = instance.getClass();
           // Field object = clazz.getField(name);

            Field field = ReflectionUtils.getField(clazz, name);

            EatOnObject eatOnObject = EatOnObject.findById((Class<EatOnObject>) field.getType(), (Long) id);

            if (eatOnObject != null) {
                ReflectionUtils.setFieldValue(field, instance, eatOnObject);
                //field.setAccessible(true);
                //field.set(instance, eatOnObject);
            } else {
                EatOnObject.loge("EatOnObject not find on binding " + name + " id = " + id);
            }

        }
        /*
        catch ( IllegalAccessException e) {

            EatOnObject.loge(e.getClass().getSimpleName() + ": Nema reference na objekt " + name);
        }
        */

    }


    private static String toJavaName(String name) {
        String[] temp = name.split("_");
        if (temp.length > 1) {
            name = temp[0];
            for (int i = 1; i < temp.length; i++) {
                name += temp[i].substring(0, 1).toUpperCase() + temp[i].substring(1);
            }
        }
        return name;
    }


    private static final Map<Class, Class> wrapperClasses = new HashMap<Class, Class>(){{
        put(boolean.class, Boolean.class);
        put(byte.class, Byte.class);
        put(char.class, Character.class);
        put(double.class, Double.class);
        put(float.class, Float.class);
        put(int.class, Integer.class);
        put(long.class, Long.class);
        put(short.class, Short.class);
        put(void.class, Void.class);
    }};

    protected static final Map<Class, String> masterDetailClasses = new HashMap<Class, String>(){{
        put(OrderItemSupplement.class, "orderItemId");
        put(OrderItem.class, "orderId");
    }};


    protected static final Map<Class, String> classCreatePath = new HashMap<Class, String>(){{
        put(Order.class, "orders");
        put(OrderItem.class, "orders/-1/orderItems");
        put(OrderItemSupplement.class, "orders/-1/orderItems/-1/orderItemSupplements");
    }};

    public static String encrypt(String text){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(text.getBytes());
            byte[] digest = md.digest();
            return String.format("%064x", new BigInteger(1, digest));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            EatOnObject.loge("UNABLE TO ENCRYPT "+e.getMessage());
        }


        return text;
    }

    protected static List<MenuItem> filterMenuItems(List<MenuItem> menuItems, Map<String, Object> params){
        if(menuItems == null) return null;

        List<MenuItem> fmi = new ArrayList<>();
        String search = (String) params.get(User.SEARCH_PARAM);
        if(search != null) search = search.toLowerCase();

        Float maxDist = (Float) params.get(User.SEARCH_PARAM_MAX_DISTANCE);
        Float maxDel = (Float) params.get(User.SEARCH_PARAM_MAX_DELIVERY_TIME);
        boolean delivery = (Boolean) params.get(User.SEARCH_PARAM_DELIVERY_MANDATORY);

        MenuType foodType = null;
        String sFoodType = (String) params.get(User.SEARCH_PARAM_FOOD_TYPE);

        if(sFoodType != null && !sFoodType.equals(User.FOOD_TYPE_ALL))
            foodType = MenuType.valueOf(sFoodType.toUpperCase());

        for (MenuItem menuItem : menuItems) {
            if (!delivery || menuItem.getMenu().getCompany().isDelivery()) {
                //Company c = menuItem.getMenu().getCompany();
                if(foodType == null || foodType == menuItem.getMenu().getType()){
                    if(search == null ||
                            (sFoodType.toLowerCase().contains(search) ||
                             menuItem.getName().toLowerCase().contains(search)
                            )
                       )
                    fmi.add(menuItem);
                }
            }
        }

        //TODO: SORT BY
        return fmi;
    }

    protected static List<Company> filterCompanies(List<Company> companies, Map<String, Object> params){
        if(companies == null) return null;

        List<Company> fC = new ArrayList<>();

        String search = (String) params.get(User.SEARCH_PARAM);
        if(search != null) search = search.toLowerCase();

        Float maxDist = (Float) params.get(User.SEARCH_PARAM_MAX_DISTANCE);
        Float maxDel = (Float) params.get(User.SEARCH_PARAM_MAX_DELIVERY_TIME);
        boolean delivery = (Boolean) params.get(User.SEARCH_PARAM_DELIVERY_MANDATORY);

        MenuType foodType = null;
        String sFoodType = (String) params.get(User.SEARCH_PARAM_FOOD_TYPE);

        if(sFoodType != null && !sFoodType.equals(User.FOOD_TYPE_ALL))
            foodType = MenuType.valueOf(sFoodType.toUpperCase());


        for (Company company : companies) {
            if( (foodType == null || hasMenuType(company, foodType)) &&
                 (search == null || containsString(company,search))
               ) fC.add(company);
        }

        return fC;
    }

    static boolean containsString(Company c, String s){
        return  c.getName().toLowerCase().contains(s) ||
                (c.getDescription()!= null && c.getDescription().toLowerCase().contains(s) ) ||
                c.getAddress().toLowerCase().contains(s);
    }

    static boolean hasMenuType(Company c, MenuType menuType){

        for (Menu menu : c.getMenus()) {
            if(menu.getType() == menuType)return true;
        }
        return false;
    }

}
