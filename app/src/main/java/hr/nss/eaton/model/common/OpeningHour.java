package hr.nss.eaton.model.common;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import hr.nss.eaton.model.user.EatOnEntity;

/**
 * Created by Robert on 28-May-18.
 */

public class OpeningHour extends EatOnObject {

    protected int day;
    protected EatOnTime from;
    protected EatOnTime to;

    public OpeningHour() {

    }

    public OpeningHour(Long id, int day, EatOnTime from, EatOnTime to) {
        super(id);
        this.day = day;
        this.from = from;
        this.to = to;
    }

    public OpeningHour(int day, EatOnTime from, EatOnTime to) {
        this(getNextId(), day, from, to);
    }



    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public EatOnTime getFrom() {
        return from;
    }

    public void setFrom(EatOnTime from) {
        this.from = from;
    }

    public EatOnTime getTo() {
        return to;
    }

    public void setTo(EatOnTime to) {
        this.to = to;
    }


    public String getTimeRange() {
        return from.toTimeString() + " - " + to.toTimeString();
    }
}
