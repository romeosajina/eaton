package hr.nss.eaton.model.transaction;

import java.util.Date;

import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.common.EatOnTime;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.User;

/**
 * Created by Robert on 28-May-18.
 */

public class Transaction extends EatOnObject {

    protected long companyId;
    protected long userId;

    protected EatOnTime date;
    protected Status status;
    protected PaymentMethod paymentMethod;
    protected Company company;
    protected User user;
    protected String remark;



    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
        refObjectSet(Company.class, companyId);
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
        refObjectSet(User.class, userId);
    }

    public EatOnTime getDateTime() {
        return date;
    }

    public void setDateTime(EatOnTime dateTime) {
        this.date = dateTime;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
        refObjectSet(Company.class, company);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        refObjectSet(User.class, user);
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    public void confirm(){
        if(getStatus() == Status.INITIALIZED){
            setStatus(Status.ORDERED);
        }
    }

    public boolean isOrdered(){
        return status == Status.ORDERED;
    }
    public boolean isCancelable(){
        return getStatus() == Status.INITIALIZED || getStatus() == Status.ORDERED;
    }
    public boolean isNew(){
        return status == Status.INITIALIZED;
    }
}
