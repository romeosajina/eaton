package hr.nss.eaton.model.transaction;

import hr.nss.eaton.model.common.EatOnObject;

/**
 * Created by Robert on 31-Oct-18.
 */

public enum Status{

    INITIALIZED(0,"I"), // Narudzba/rezervacija je tek kreirana
    ORDERED(1,"O"),  // Korisnik je potvrdia narudzbu, sad ceka da mu se jave iz company
    CONFIRMED(2,"C"), // Company je potvrdila narudzbu
    PROCESSING(3,"P"), //Narudzba se sada priprema
    FINISHED(4,"F"), //Narudzba se sada dostavlja
    DELIVERED(5,"D"), //Narudzba je dostavljena na adresu
    REQUESTED(6,"R"), //Zahtjev za rezervacijom je poslan
    CANCELED(7,"L");  //Zahtjev za rezervacijom je ponisten, ili od strane korisnika ili coompany

    private int value;
    private String letter;
    Status(int val,String let){
        this.value = val;
        this.letter = let;

    }

    @Override
    public String toString() {
        return getLetter();
    }

    public int getValue() {
        return value;
    }
    public String getLetter(){
        return letter;
    }

    public static Status find(String letter){
        for(Status s : values()){
            if(s.letter.equals(letter))return s;
        }
        return INITIALIZED;
    }

    /*
    public static Status valueOf(int value){
        Status[] values = values();
        if(values.length>value && value >= 0){
            return values[value];
        }
        return INITIALIZED;
    }
    */
}
