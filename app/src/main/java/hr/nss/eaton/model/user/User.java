package hr.nss.eaton.model.user;


import android.util.Log;
import android.view.OrientationEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.common.EatOnTime;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.transaction.OrderItem;
import hr.nss.eaton.model.transaction.Reservation;
import hr.nss.eaton.model.transaction.Status;

/**
 * Created by Robert on 28-May-18.
 */

public class User extends EatOnEntity {

    public static final User findById(Long id){
        return findById(User.class, id);
    }

    public static final String SEARCH_PARAM = "search_param";
    public static final String SEARCH_PARAM_MAX_DISTANCE = "max_distance";//U metrima
    public static final String SEARCH_PARAM_SORT_BY = "sort_by";
    public static final String SEARCH_PARAM_FOOD_TYPE = "food_type";
    public static final String SEARCH_PARAM_DELIVERY_MANDATORY = "delivery_mandatory";
    public static final String SEARCH_PARAM_MAX_DELIVERY_TIME = "max_delivery_time";//U minutama

    public static final String SORT_BY_DELIVERY_TIME = "delivery";
    public static final String SORT_BY_RECOMMENDED = "recommended";
    public static final String SORT_BY_MARK = "mark";
    public static final String SORT_BY_PRICE = "price";

 //   public static final String FOOD_TYPE_PIZZA = "pizza";
    public static final String FOOD_TYPE_ALL = "all";

    protected String surname;
    protected boolean showNumber = true;
    protected String password;

    private List<MenuItem> recentMenuItems = null;
    private List<Company> recentCompanies = null;
    private Map<String, Object> searchParams = null;

    private List<Order> newOrders = null;



    /*
    public User(long id, String name, String surname, String city, String address, double latitude, double longitude, String phoneNumber, EatOnImage photo, List<Reservation> reservations, List<Order> orders) {
        super(id, name, city, address, latitude, longitude, phoneNumber, photo, reservations, orders);
        this.surname = surname;
    }
*/

    public User(Long id, String name, String surname, String city, String address, boolean showNumber, double latitude, double longitude) {
        super(id, name, city, address, latitude, longitude);
        this.surname = surname;
        this.showNumber = showNumber;
    }

    public User(){
    }


    public static User get() {
        return DataProvider.get().getUser();
    }



    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isShowNumber() {
        return showNumber;
    }

    public void setShowNumber(boolean showNumber) {
        this.showNumber = showNumber;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public Map<String, Object> getSearchParams() {
        /*
        if(searchParams == null){
            searchParams = new HashMap<>();
            searchParams.put(SEARCH_PARAM_MAX_DISTANCE, 500f);// 500 metara
            searchParams.put(SEARCH_PARAM_SORT_BY, SORT_BY_DELIVERY_TIME);
            searchParams.put(SEARCH_PARAM_FOOD_TYPE, FOOD_TYPE_ALL);
            searchParams.put(SEARCH_PARAM_DELIVERY_MANDATORY, true);
            searchParams.put(SEARCH_PARAM_MAX_DELIVERY_TIME, 30f); //30 minuta
        }
        */
        return searchParams;
    }

    public void setSearchParams(Map<String, Object> searchParams) {
        this.searchParams = searchParams;
    }

    public void setSearchParam(String paramName, Object value) {
        if(searchParams == null) searchParams = new HashMap<>();
        this.searchParams.put(paramName, value);
    }

    public List<MenuItem> getRecentMenuItems() {
        if(recentMenuItems == null) recentMenuItems = new ArrayList<>();
        return recentMenuItems;
    }

    public void setRecentMenuItems(List<MenuItem> recentMenuItems) {
        this.recentMenuItems = recentMenuItems;
    }
    public void addRecentMenuItem(MenuItem recentMenuItem) {
        if(!getRecentMenuItems().contains(recentMenuItem))
            getRecentMenuItems().add(recentMenuItem);
    }

    public List<Company> getRecentCompanies() {
        if(recentCompanies == null) recentCompanies = new ArrayList<>();
        return recentCompanies;
    }

    public void setRecentCompanies(List<Company> recentCompanies) {
        this.recentCompanies = recentCompanies;
    }
    public void addRecentCompany(Company recentCompany) {
        if(!getRecentCompanies().contains(recentCompany))
            getRecentCompanies().add(recentCompany);
    }

    public <T> T getSearchParam(String paramName){
        return (T) getSearchParams().get(paramName);
    }


    /**
     * vraća narudzbu prema company samo ako je narudzba nova
     * ako narudzba ima neki drugi status onda se vise na nju
     * ne more dodavat
     * @param company
     * @return
     */
    public Order ensureNewOrder(Company company){

        Order o = findNewOrder(company);

        if(o == null){
            o = Order.create(this, company);
            addNewOrder(o);
        }

        return o;
    }

    public void addNewOrder(Order order) {
        order.setUser(this);
        getNewOrders().add(order);
  //      log("addNewOrder: "+order.getId());
    }

    public boolean removeOrder(Order order){
        if(removeNewOrder(order))return true;
        if (getOrders().remove(order)) {
            order.setUser(null);
            return true;
        }
        return false;
    }


    @Override
    public Order findOrderById(long id) {
        for (Order order : getNewOrders()) {
            if(order.getId() == id) return order;
        }
        return super.findOrderById(id);
    }

    public boolean removeNewOrder(Order order){

        if (getNewOrders().remove(order)) {
            order.setUser(null);
            return true;
        }

        return false;
    }

    public List<Order> getNewOrders() {
        if (newOrders == null) {
            newOrders = new ArrayList<>();
        }
        return newOrders;
    }


    public void setNewOrders(List<Order> newOrders) {
        this.newOrders = newOrders;
    }

    @Override
    public List<Order> getPendingOrders() {
        List<Order> pending = super.getPendingOrders();

/*
        String s = "";
        for(Order o : pending){
            s+=o.getId()+" ";
        }
        log("Pending: "+s);

        s = "";
        for(Order o : getNewOrders()){
            s+=o.getId()+" ";
        }
        log("New: "+s);


        */
        // Nemore se desit duplikati jer se pending svaki put novi array napravi
        // kad se pozove super
        pending.addAll(getNewOrders());

        return pending;
    }

    public OrderItem createOrderItem(MenuItem menuItem){


        Order order = ensureNewOrder(menuItem.getMenu().getCompany());
        OrderItem orderItem = order.addOrderItem(menuItem);

        // more ne imat supplemente pa u ten slucaju nece imat opcije
        if (menuItem.hasMenuItemOptionsOptions()) {
            orderItem.setMenuItemOption(menuItem.getMenuItemOptions().get(0));
        }

        return orderItem;
    }

    public boolean removeOrderItem(OrderItem orderItem){


        Order order = orderItem.getOrder();

        if (order != null) {
            //ovo i postavlja order na orderItemu na null
            order.removeOrderItem(orderItem);
            //ako je zadnji artikal u narudzbi onda ostaje prazna narudzba
            //pa ni ona vise nije potrebna
            if (!order.hasOrderItems()) {
                removeNewOrder(order);
            }

            return true;
        }

        return false;
    }


    public Order findNewOrder(Company company){

        for (Order order : getNewOrders()) {
            if (order.getCompany().equals(company) &&
                    order.getStatus() == Status.INITIALIZED) {
                return order;
            }
        }
        return null;
    }


    public OrderItem findNewOrderItemByMenuItemId(long menuItemId){
        OrderItem item = null;

        for(Order o : getNewOrders()){
            item = o.findOrderItemByMenuItemId(menuItemId);
            if(item != null)
                break;
        }

        return item;
    }


}
