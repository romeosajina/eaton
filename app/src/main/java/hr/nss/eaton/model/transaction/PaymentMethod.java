package hr.nss.eaton.model.transaction;

public enum  PaymentMethod {

    CASH("C"),
    ETH("E");

    private String letter;
    PaymentMethod(String letter){
        this.letter = letter;
    }
    public String getLetter(){
        return letter;
    }


    @Override
    public String toString() {
        return getLetter();
    }


    public static PaymentMethod find(String letter){
        for(PaymentMethod s : values()){
            if(s.letter.equals(letter))return s;
        }
        return CASH;
    }
}
