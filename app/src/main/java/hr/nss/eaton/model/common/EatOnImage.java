package hr.nss.eaton.model.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.data.HttpUtils;

/**
 * Created by Robert on 28-May-18.
 */

public class EatOnImage {

    private static Map<String, Bitmap> loadedImages = new HashMap<>();

    private String url;
    private Bitmap image;

    public EatOnImage() {
    }

    public EatOnImage(String url) {
        setURL(url);
    }

    public void setURL(String url) {
        this.url = url;
        if(this.url != null && this.url.trim().isEmpty())this.url = null;
        else if(this.url != null) this.url = this.url.replace("\\", "/");

        image = null;
    }

    public boolean hasURL(){
        return getURL() != null;
    }
    public String getURL() {
        return url == null ? null : DataProvider.get().getServerUrlPrefix() + url;
    }

    public void loadImage(final ImageListener imageListener) {

        if(getURL() == null)return;

        if(image == null){
            image = loadedImages.get(getURL());
        }

        if (image == null) {
            new DownloadImageTask(imageListener).execute(getURL());
        } else {
            imageListener.onLoad(image);
        }


    }

    public interface ImageListener {
        void onLoad(Bitmap bitmap);
    }


    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageListener imageListener;

        public DownloadImageTask(ImageListener imageListener) {
            this.imageListener = imageListener;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;

            try {

                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if(result != null){
                image = result;
                loadedImages.put(getURL(), result);
                imageListener.onLoad(result);
            }
        }
    }
}



