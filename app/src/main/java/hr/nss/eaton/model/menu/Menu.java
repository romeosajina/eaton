package hr.nss.eaton.model.menu;

import java.util.ArrayList;
import java.util.List;

import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.user.Company;

/**
 * Created by Robert on 28-May-18.
 */

public class Menu extends EatOnObject {

    public static final Menu findById(long id){
        return findById(Menu.class, id);
    }


    protected long companyId;
    protected MenuType type;
    protected EatOnImage photo;
    protected List<MenuItem> menuItems;
    protected Company company;

    public void setPhoto(EatOnImage photo) {
        this.photo = photo;
    }

    public EatOnImage getPhoto() {
        return photo;
    }

    public MenuType getType() {
        return type;
    }

    public void setType(MenuType type) {
        this.type = type;
    }

    public List<MenuItem> getMenuItems() {
        if(menuItems == null) menuItems = new ArrayList<>();
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> items) {
        this.menuItems = items;
    }

    public void addItem(MenuItem item) {
        item.setMenu(this);
        getMenuItems().add(item);
    }
    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
        refObjectSet(Company.class, companyId);
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
        refObjectSet(Company.class, company);
    }
}
