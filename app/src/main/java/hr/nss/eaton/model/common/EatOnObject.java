package hr.nss.eaton.model.common;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.data.TestDataProvider;
import hr.nss.eaton.utils.ReflectionUtils;

/**
 * Created by Robert on 31-Oct-18.
 */

public abstract class EatOnObject {
    public static final long NULL_REFERENCE_ID = 0;
    private static long nextId = -1L;
    private static final Map<Class, List<EatOnObject>> eatOnObjects = new HashMap<>();

    static {
        /*
        nextId = -1L;

        for (Company company : DataProvider.get().getCompanies()) {
            DataProvider.get().getMenus(company);
        }
        DataProvider.get().getMenuItems(null);
        DataProvider.get().getReservations(DataProvider.get().getUser());
        DataProvider.get().getOrders(DataProvider.get().getUser());
        */
    }

    public static long getNextId() {
        return nextId--;
    }


    protected long id;
    public EatOnObject() {
    }
    public EatOnObject(long id) {
        setId(id);
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
        eatOnObjectAdded(this);
    }


    protected void refObjectSet(Class<? extends EatOnObject> objectClass, EatOnObject eatOnObject) {
        String idName = toIdName(objectClass.getSimpleName());
        Object value = null;
        if(eatOnObject == null) value = NULL_REFERENCE_ID;
        else if(eatOnObject.getId() != (long) ReflectionUtils.getFieldValue(this, idName))
            value = eatOnObject.getId();
        if(value != null){
            ReflectionUtils.setFieldValue(this, idName, value);
        }

    }

    protected void refObjectSet(Class<? extends EatOnObject> objectClass, long refId) {
        String name = toName(objectClass.getSimpleName());

        EatOnObject eatOnObject = (EatOnObject) ReflectionUtils.getFieldValue(this, name);

        if(refId == NULL_REFERENCE_ID && eatOnObject == null) return;

        Object value = null;

        if(refId  != NULL_REFERENCE_ID){
            value = findById(ReflectionUtils.getFieldClass(getClass(), name), refId);
        }

        ReflectionUtils.setFieldValue(this, name, value);

    }

    private String toName(String className){
        return className.substring(0, 1).toLowerCase() + className.substring(1);
    }

    private String toIdName(String className){
        return toName(className)+ "Id";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EatOnObject)) return false;

        EatOnObject that = (EatOnObject) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        String s = getClass().getSimpleName() +" id: "+id;
        return s;
        /*
        String s = getClass().getSimpleName() + "[";
        Field[] fields = getClass().getFields();
        for (Field field : fields) {
            try {
                if(!field.isAccessible()) field.setAccessible(true);
                s+=field.getName()+"="+field.get(this)+", ";
            } catch (IllegalAccessException e) {
                s+=field.getName()+"=unaccessible, ";
            }
        }

        if(s.endsWith(", "))s = s.substring(0, s.length()-2);
        return s+"]";
        */
    }

    public static void eatOnObjectAdded(EatOnObject eatOnObject) {

        if(eatOnObject != null){

            Class clazz = eatOnObject.getClass();
            List<EatOnObject> eatOnObjectsList = eatOnObjects.get(clazz);

            if(eatOnObjectsList == null){
                eatOnObjectsList = new ArrayList<>();
                eatOnObjects.put(clazz, eatOnObjectsList);
            }

            if(!eatOnObjectsList.contains(eatOnObject)){
                eatOnObjectsList.add(eatOnObject);
             //   loge(eatOnObject.getClass().getSimpleName() + " added with id = " + eatOnObject.id);
            }
        }
    }
    public static void eatOnObjectRemoved(EatOnObject eatOnObject){
        if(eatOnObject != null){
            Class clazz = eatOnObject.getClass();
            List<EatOnObject> eatOnObjectsList = eatOnObjects.get(clazz);
            if(eatOnObjectsList == null )return;
            eatOnObjectsList.remove(eatOnObject);
            log(eatOnObject.getClass().getSimpleName() + " removed with id = " + eatOnObject.id);
        }
    }

    public static <T extends EatOnObject> T findById( Class<T> clazz, long id){

        List<EatOnObject> eatOnObjectsList = eatOnObjects.get(clazz);

        T eatOnObject = findInList(eatOnObjectsList, id);
        if(eatOnObject == null){
                if(DataProvider.get() instanceof TestDataProvider){
                    eatOnObject = findInList(((TestDataProvider) DataProvider.get()).loadJSON(clazz), id);
                }
                if(eatOnObject == null)
                   log(clazz.getSimpleName()+" not found for id "+ id);
        }

        return eatOnObject;
    }

    private static <T extends EatOnObject> T findInList( List<EatOnObject> list, long id){

        if(list == null || list.isEmpty()) return null;

        for(int i = 0; i < list.size(); i++){
            EatOnObject eatOnObject = list.get(i);
     //       log("FIND: " + eatOnObject.getId() + " = " + id);
            if(eatOnObject.getId() == id) return (T) eatOnObject;
        }

        return null;
    }

    public static List<? extends EatOnObject> getAllInstances(Class<?> clazz){
        return eatOnObjects.get(clazz);
    }

    public static List<? extends EatOnObject> getAllInstances(Class<?> clazz, int limit){
        return getAllInstances(clazz, 0, limit);
    }

    public static List<? extends EatOnObject> getAllInstances(Class<?> clazz,int from,  int limit){
        List<EatOnObject> list = eatOnObjects.get(clazz);
        if(list == null || list.size() <= limit) return list;
        return list.subList(from, limit);
    }


    public static void log(Object value){
        Log.d("EatOnObject", value+"");
    }

    public static void loge(Object value){
        Log.e("EatOnObjectError", "ERROR: "+value+"");
        if(value instanceof Exception)((Exception) value).printStackTrace();
    }
}
