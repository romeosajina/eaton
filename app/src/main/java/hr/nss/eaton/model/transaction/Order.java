package hr.nss.eaton.model.transaction;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import hr.nss.eaton.model.common.EatOnTime;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.User;


/**
 * Created by Robert on 28-May-18.
 */

public class Order extends Transaction {




    public static final Order findById(Long id){
        return findById(Order.class, id);
    }

    protected EatOnTime deliveryTime;
    protected float rate;

    protected List<OrderItem> orderItems;

    public static Order create(User user, Company company){

        Order o = new Order();
        o.setId(getNextId());
        o.setDateTime(EatOnTime.now());
        o.setStatus(Status.INITIALIZED);
        o.setDeliveryTime(EatOnTime.delivery());
        o.setUser(user);
        o.setCompany(company);

        return o;
    }

    public EatOnTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(EatOnTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }
    public boolean hasOrderItems(){
        List<OrderItem> oi = getOrderItems();
        return oi != null && !oi.isEmpty();
    }
    public int getOrderItemCount(){
        return getOrderItems().size();
    }

    public List<OrderItem> getOrderItems() {
        if(orderItems == null) orderItems = new ArrayList<>();
        return orderItems; }

    public void setOrderItems(List<OrderItem> orderItems) { this.orderItems = orderItems; }

    public Double getTotal(){
        Double total = 0.0;
        for (OrderItem oi : getOrderItems()) {
            total += oi.getTotal();
        }
        return  total;
    }

    public OrderItem addOrderItem(MenuItem menuItem) {
       /*
        OrderItem orderItem = findOrderItemByMenuItemId(menuItem.getId());
        if(orderItem == null){
            orderItem = OrderItem.create(menuItem);
            orderItem.setOrder(this);
            getOrderItems().add(orderItem);
        }
        */
        OrderItem orderItem = OrderItem.create(this, menuItem);
        getOrderItems().add(orderItem);
        return orderItem;
    }


    
/*
    protected OrderItem findOrderItemById(Long id){
        for(OrderItem i : getOrderItems())
            if(i.getId() == id)
                return i;
        return null;
    }
*/
    public OrderItem findOrderItemByMenuItemId(long id){
        for(OrderItem i : getOrderItems())
            if(i.getMenuItem().getId() == id)
                return i;
        return null;
    }

    public boolean removeOrderItem(OrderItem orderItem){
        if(!canRemoveOrderItem())return false;

        if(getOrderItems().remove(orderItem)){
            orderItem.setOrder(null);
            return true;
        }
        return false;
    }



    public boolean canRemoveOrderItem(){
        return getStatus() == Status.INITIALIZED;
    }


    private List<OrderItem> cloneOrderItems(Order o){
        List<OrderItem> ois = new ArrayList<>();

        for (OrderItem orderItem : getOrderItems()) {

            OrderItem newOi = o.addOrderItem(orderItem.getMenuItem());
            ois.add(newOi);

            if(orderItem.getMenuItemOption() != null){
                newOi.setMenuItemOption(orderItem.getMenuItemOption());

                for (OrderItemSupplement orderItemSupplement : orderItem.getOrderItemSupplements()) {
                    newOi.addOrderItemSupplement(orderItemSupplement.getMenuItemOptionSupplement());
                }
            }
        }
        return ois;
    }

    public Order copyToNewOrders(){
        Order order = create(getUser(), getCompany());
        order.setRemark(remark);
        order.setOrderItems(cloneOrderItems(order));

        getUser().addNewOrder(order);

        return order;
    }

}
