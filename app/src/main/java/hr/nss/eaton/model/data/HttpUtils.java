package hr.nss.eaton.model.data;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.entity.StringEntity;
import hr.nss.eaton.model.common.EatOnObject;

/**
 * Created by Robert on 01-Nov-18.
 */

public class HttpUtils {

    public static final String AVAILABLE = "available";
    public static final String COMPANIES = "companies?expand=menus,menuItems,menuItemOptions,menuItemOptionSupplements";
    public static final String ORDERS = "orders?expand=orderItems,orderItemSupplements";
    public static final String ORDERS_BY_ID = "orders/";
    public static final String DELETE_ALL_CHILDREN = "?deleteAllConnectedChildren=true";
    public static final String AUTH = "auth";
    public static final String USER = "users";
    public static final String USER_BY_ID = "users/";
    public static final String BATCH = "/batch";
    //private static final String IP = "http://192.168.1.16";
    private static final String IP = "http://www.romeosajina.ga";

    public static final String BASE_URL = IP + "/EatOn/public/server/";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get2(String url, AsyncHttpResponseHandler responseHandler) {
        client.get(url, null, responseHandler);
    }
    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        EatOnObject.log("GET "+url);
        client.get(buildUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        EatOnObject.log("POST "+url);
        client.post(buildUrl(url), params, responseHandler);
    }
    public static void post(String url, JSONObject jsonObject, AsyncHttpResponseHandler responseHandler){
        EatOnObject.log("POST "+url);
        try {
            StringEntity entity = new StringEntity(jsonObject.toString());
            client.post(null, buildUrl(url), entity,"application/json", responseHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void patch(String url, JSONObject jsonObject, AsyncHttpResponseHandler responseHandler){
        EatOnObject.log("PATCH "+url);
        try {
            StringEntity entity = new StringEntity(jsonObject.toString());
            client.patch(null, buildUrl(url), entity,"application/json", responseHandler);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    public static void delete(String url, AsyncHttpResponseHandler responseHandler){
        EatOnObject.log("DELETE "+url);
        client.delete(buildUrl(url), null, responseHandler);
    }

    private static void getByUrl(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(url, params, responseHandler);
    }

    private static void postByUrl(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(url, params, responseHandler);
    }

    public static void setAuth(String email, String password){
        client.setBasicAuth(email, password);
    }
    private static String buildUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
