package hr.nss.eaton.model.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Robert on 28-May-18.
 */

public class EatOnTime extends GregorianCalendar {

    private Date date;

    public EatOnTime(){
        //Samo privremeno za testiranje
        date = EatOnTime.now().getDate();
    }
    public EatOnTime(String dateTime){
      //  this();
        try {
            date = printDateTimeFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
      //  EatOnObject.loge("EatOnTime: " + dateTime+" " +" " +date+" " + toString());

    }
    public EatOnTime(Date date){
        this.date = date;
    }

    private static SimpleDateFormat dateFormat = (SimpleDateFormat)SimpleDateFormat.getDateInstance();

    private static SimpleDateFormat dateTimeFormat = (SimpleDateFormat) SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.MEDIUM, SimpleDateFormat.SHORT);

    private static SimpleDateFormat timeFormat = (SimpleDateFormat) SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT);

    private static SimpleDateFormat printDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");



    public String toTimeString(){
            return timeFormat.format(date);
    }
    public String toDateString() {
      return  dateFormat.format(date);
    }

    public String toDateTimeString(){
        return dateTimeFormat.format(date);
    }
    public static EatOnTime now(){
        return new EatOnTime(getInstance().getTime());
    }

    /**
     * Vraća novu instancu EatOnTime + 30 min kao vrijeme dostave
     * @return
     */
    public static EatOnTime delivery(){
        Calendar c = getInstance();
        c.add(MINUTE, 30);
        return new EatOnTime(c.getTime());
        // new EatOnTime(new Date(getInstance().getTimeInMillis()+200000000));
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public int compareTo(EatOnTime other) {
        return date.compareTo(other.date);
    }

    @Override
    public String toString() {
        return  printDateTimeFormat.format(date);
    }
}
