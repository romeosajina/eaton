package hr.nss.eaton.model.transaction;

import java.util.ArrayList;
import java.util.List;

import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.menu.MenuItemOption;
import hr.nss.eaton.model.menu.MenuItemOptionSupplement;

/**
 * Created by Robert on 28-May-18.
 */

public class OrderItem extends EatOnObject {

    public static final OrderItem findById(Long id){
        return findById(OrderItem.class, id);
    }



    protected long orderId;
    protected long menuItemId;
    protected long menuItemOptionId;

    protected int amount;
    protected double discount;
    protected String remark;

    protected Order order;
    protected MenuItem menuItem;
    protected MenuItemOption menuItemOption;
    protected List<OrderItemSupplement> orderItemSupplements;

    public static OrderItem create(Order order, MenuItem menuItem){
        OrderItem orderItem = new OrderItem();
        orderItem.setId(getNextId());
        orderItem.setMenuItem(menuItem);
        orderItem.setOrder(order);
        orderItem.setAmount(1);
        orderItem.setDiscount(menuItem.getDiscount());
        return orderItem;
    }

    public void increaseAmountBy(int amount) {
        this.amount += amount;
    }

    public void decreaseAmountBy(int amount) {
        if(this.amount - amount < 0)
            throw new IllegalArgumentException("Količina mora biti veća ili jednaka 0");

        this.amount -= amount;
    }


    public long getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(long menuItemId) {
        this.menuItemId = menuItemId;
        refObjectSet(MenuItem.class, menuItemId);
    }

    public long getMenuItemOptionId() {
        return menuItemOptionId;
    }

    public void setMenuItemOptionId(long menuItemOptionId) {
        this.menuItemOptionId = menuItemOptionId;
        refObjectSet(MenuItemOption.class, menuItemOptionId);
        removeOrderItemSupplements();
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
        refObjectSet(MenuItem.class, menuItem);
    }

    public MenuItemOption getMenuItemOption() {
        return menuItemOption;
    }

    public void setMenuItemOption(MenuItemOption menuItemOption) {
        this.menuItemOption = menuItemOption;
        refObjectSet(MenuItemOption.class, menuItemOption);
        removeOrderItemSupplements();
    }

    public boolean hasOrderItemSupplements(){
        return !getOrderItemSupplements().isEmpty();
    }
    public List<OrderItemSupplement> getOrderItemSupplements() {
        if(orderItemSupplements == null) orderItemSupplements = new ArrayList<>();
        return orderItemSupplements;
    }

    public void setOrderItemSupplements(List<OrderItemSupplement> orderItemSupplements) {
        this.orderItemSupplements = orderItemSupplements;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
        refObjectSet(Order.class, orderId);
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
        refObjectSet(Order.class, order);
    }

    public void removeOrderItemSupplements() {
        getOrderItemSupplements().clear();
    }


    public void removeOrderItemSupplement(MenuItemOptionSupplement menuItemOptionSupplement){

        OrderItemSupplement orderItemSupplement = findOrderItemSupplement(menuItemOptionSupplement);

        if(orderItemSupplement != null){
            orderItemSupplement.setOrderItem(null);
            getOrderItemSupplements().remove(orderItemSupplement);
        }

    }


    public OrderItemSupplement ensureOrderItemSupplement(MenuItemOptionSupplement menuItemOptionSupplement){

        OrderItemSupplement orderItemSupplement = findOrderItemSupplement(menuItemOptionSupplement);

        if(orderItemSupplement == null)
            orderItemSupplement = addOrderItemSupplement(menuItemOptionSupplement);

        return orderItemSupplement;

    }

    public OrderItemSupplement addOrderItemSupplement(MenuItemOptionSupplement menuItemOptionSupplement){
        // ako se ni postavija option onda ga popuni iz supplement
        // triba pozvat na pocetku jer on na setteru prazni sve orderItemSupplemente pa bi se
        // u protivnom izbrisa i ovaj supplement
        if(getMenuItemOption() == null)
            setMenuItemOption(menuItemOptionSupplement.getMenuItemOption());

        OrderItemSupplement orderItemSupplement = new OrderItemSupplement();

        orderItemSupplement.setMenuItemOptionSupplement(menuItemOptionSupplement);
        orderItemSupplement.setOrderItem(this);

        getOrderItemSupplements().add(orderItemSupplement);
        return orderItemSupplement;
    }


    public OrderItemSupplement findOrderItemSupplement(MenuItemOptionSupplement menuItemOptionSupplement){

        for (OrderItemSupplement orderItemSupplement : getOrderItemSupplements()) {

            if(orderItemSupplement.getMenuItemOptionSupplement().equals(menuItemOptionSupplement))
                return orderItemSupplement;
        }

        return null;
    }


    public double getTotal(){
        double total;

        if(getMenuItemOption() != null){
            total = getMenuItemOption().getPrice();
        }else{
            total = getMenuItem().getPrice();
        }
        if(getOrderItemSupplements() != null ){
            for (OrderItemSupplement orderItemSupplement : getOrderItemSupplements()) {
                total += orderItemSupplement.getMenuItemOptionSupplement().getPrice();
            }
        }
        total *= getAmount();
        return total;
    }

}
