package hr.nss.eaton.model.user;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.common.EatOnTime;
import hr.nss.eaton.model.common.OpeningHour;
import hr.nss.eaton.model.data.DataProvider;
import hr.nss.eaton.model.menu.Menu;
import hr.nss.eaton.view.utils.ComponentUtils;

/**
 * Created by Robert on 28-May-18.
 */

public class Company extends EatOnEntity {

    public static final Company findById(Long id){
        return findById(Company.class, id);
    }
    public static  final Company findByName(String name){
        List<Company> all = (List<Company>) getAllInstances(Company.class);
        if(all == null)return null;
        for (Company company : all) {
            if(company.getName().equals(name)) return company;
        }
        return null;
    }


    protected boolean delivery;
    protected boolean pickUp;

    protected double minDeliveryPrice;
    protected double maxDestinationRange;
    protected double maxFreeDeliveryRange;

    protected double deliveryPrice;

    protected String description;


    protected List<OpeningHour> openingHours;
    protected List<Menu> menus;

    /*
    public Company(){

    }
    public Company(long id, String name, String city, String address,
                   double latitude, double longitude, String phoneNumber, EatOnImage photo, boolean delivery, boolean pickUp) {
        super(id, name, city, address, latitude, longitude, phoneNumber, photo);
        set(delivery, pickUp, 0, 0, 0, null, null);
    }

    protected void set(boolean delivery, boolean pickUp, double minDeliveryPrice, long maxDestinationRange, long maxFreeDeliveryRange, String description, List<Menu> menus) {
        this.delivery = delivery;
        this.pickUp = pickUp;
        this.minDeliveryPrice = minDeliveryPrice;
        this.maxDestinationRange = maxDestinationRange;
        this.maxFreeDeliveryRange = maxFreeDeliveryRange;
        this.description = description;
       // this.openingHours = openingHours;
        this.menus = menus;
    }
    */

    public boolean isDelivery() {
        return delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    public boolean isPickUp() {
        return pickUp;
    }

    public void setPickUp(boolean pickUp) {
        this.pickUp = pickUp;
    }

    public double getMinDeliveryPrice() {
        return minDeliveryPrice;
    }

    public void setMinDeliveryPrice(double minDeliveryPrice) {
        this.minDeliveryPrice = minDeliveryPrice;
    }

    public double getMaxDestinationRange() {
        return maxDestinationRange;
    }

    public void setMaxDestinationRange(double maxDestinationRange) {
        this.maxDestinationRange = maxDestinationRange;
    }

    public double getMaxFreeDeliveryRange() {
        return maxFreeDeliveryRange;
    }

    public void setMaxFreeDeliveryRange(double maxFreeDeliveryRange) {
        this.maxFreeDeliveryRange = maxFreeDeliveryRange;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OpeningHour> getOpeningHours() {
        if(openingHours == null){
            openingHours = new ArrayList<>();
            //TODO: SAMAO INICIJALIZACIJA, promjenit u nesto smisleno
            setForDay(1, EatOnTime.now(), EatOnTime.delivery());
            setForDay(2, EatOnTime.now(), EatOnTime.delivery());
            setForDay(3, EatOnTime.now(), EatOnTime.delivery());
            setForDay(4, EatOnTime.now(), EatOnTime.delivery());
            setForDay(5, EatOnTime.now(), EatOnTime.delivery());
            setForDay(6, EatOnTime.now(), EatOnTime.delivery());
            setForDay(7, EatOnTime.now(), EatOnTime.delivery());
        }
        return openingHours;
    }

    public void setOpeningHours(List<OpeningHour> openingHours) {
        this.openingHours = openingHours;
    }

    public List<Menu> getMenus() {
        // Ovo je samo za testiranje, nemore postojat company bez menu-a
        if(menus == null) menus = new ArrayList<>();
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }
    public double getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(double deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    
    public void setForDay(int day, EatOnTime from, EatOnTime to){
        OpeningHour dayHours = getForDay(day);
        if(dayHours == null){
            dayHours = new OpeningHour(day, from, to);

            int index = 0;

            for (OpeningHour dh : getOpeningHours()) {
                if(dh.getDay() < day)break;
                index++;
            }

            getOpeningHours().add(index, dayHours);

        }
        else{
            dayHours.setFrom(from);
            dayHours.setTo(to);
        }

    }

    public boolean canOrderNow(){

        OpeningHour oh = getForToday();
        EatOnTime now = EatOnTime.now();
        //MOZDA DA SE NE MORE NARUCIT ZADNJIH POL URE
        return (oh.getFrom().compareTo(now) <= 0 && oh.getTo().compareTo(now) > 0);
    }

    public OpeningHour getForToday(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK) - 1; // -1 jer zapocinje sa nedjeljom
        if(day == 0) day = 7;

        return getForDay(day);
    }

    public OpeningHour getForDay(int day){
        for (OpeningHour dh : getOpeningHours()) {
            if(dh.getDay()  == day) return dh;
        }
        return null;
    }


}
