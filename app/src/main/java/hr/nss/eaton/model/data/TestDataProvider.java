package hr.nss.eaton.model.data;

import android.content.Context;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.protocol.HTTP;
import hr.nss.eaton.model.common.EatOnObject;
import hr.nss.eaton.model.menu.MenuItem;
import hr.nss.eaton.model.menu.Menu;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.transaction.Reservation;
import hr.nss.eaton.model.user.Company;
import hr.nss.eaton.model.user.EatOnEntity;
import hr.nss.eaton.model.user.User;

/**
 * Created by Robert on 29-May-18.
 */

public class TestDataProvider extends DataProvider {
    @Override
    public User getUser() {
        if(userInstance == null){

            User user = new User(1L, "Name", "LastName", "Pula", "Rovinjska 14", true, 44.8674726,13.8485643);
            user.setPhone("098 1029 241");
            user.setEmail("name.lastname@gmail.com");

            //long id, MenuItem item, int amount, double discount, String remark
//        for (Order order : getOrders(user)) {
            //      user.addNewOrder(order);
            //       }

            user.setRecentMenuItems(getMenus(getCompanies().get(0)).get(0).getMenuItems());
            user.setRecentCompanies(getCompanies());
            userInstance = user;
            getOrders(user);
        }

        return userInstance;
    }

    @Override
    public List<Company> getCompanies() {
        return (List<Company>) getOrLoad(Company.class);
    }



    @Override
    public void requestCompanies(final RequestListener<List<Company>> requestListener) {
        requestListener.success(getCompanies());
    }

    @Override
    public void requestCompanies(RequestListener<List<Company>> requestListener, Map<String, Object> params) {
        requestListener.success(filterCompanies(getCompanies(),params));
    }

    @Override
    public void requestCompany(RequestListener<Company> requestListener, Long id) {
        getCompanies();
        requestListener.success(Company.findById(id));
    }

    @Override
    public void requestMenus(RequestListener<List<Menu>> requestListener, Company company) {
        requestListener.success(getMenus(company));

    }

    @Override
    public void requestOrders(RequestListener<List<Order>> requestListener) {
        requestListener.success(getOrders(getUser()));

    }

    @Override
    public void requestMenuItems(RequestListener<List<MenuItem>> requestListener, Map<String, Object> params) {
        requestListener.success(filterMenuItems(getMenuItems(params), params));

    }

    @Override
    public void requestReservations(RequestListener<List<Reservation>> requestListener, EatOnEntity eatOnEntity) {
        requestListener.success(getReservations(eatOnEntity));
    }



    @Override
    public void requestUser(final RequestListener<User> requestListener, final String email, final String password) {

     //   if("r".equals(password)){
            requestListener.success(getUser());
     //   }

    }

    @Override
    public void registerUser(User user, RequestListener<User> requestListener) {

    }

    @Override
    public void updateUser(User user, RequestListener<User> requestListener) {

    }

    @Override
    public void deleteOrder(Order order, RequestListener<Order> requestListener) {

    }

    @Override
    public void postOrder(Order order, RequestListener<Order> requestListener) {
        requestListener.success(order);

    }

    @Override
    public void requestOrder(long id, RequestListener<Order> requestListener) {
        requestListener.success(Order.findById(id));
    }

    @Override
    public List<Company> getCompanies(Map<String, Object> params) {
        return getCompanies();
    }

    @Override
    public Company getCompany(long id) {
        return getCompanies().get(0);
    }

    @Override
    public List<Menu> getMenus(Company company) {
        return (List<Menu>) getOrLoad(Menu.class);
    }

    @Override
    public List<Reservation> getReservations(EatOnEntity eatOnEntity) {
        return (List<Reservation>) getOrLoad(Reservation.class);
    }

    @Override
    public List<Order> getOrders(EatOnEntity eatOnEntity) {
        getMenus(getCompanies().get(0));
        return (List<Order>) getOrLoad(Order.class);
    }

    @Override
    public List<MenuItem> getMenuItems(Map<String, Object> params) {
        getCompanies();
        return (List<MenuItem>)EatOnEntity.getAllInstances(MenuItem.class);
    }

    private static Context context;
    public static void setContext(Context c){
        if (DataProvider.get() instanceof TestDataProvider) {
                context = c;
        }
    }


    /*
        kad se doda nova narudzba pa se gre citat orders onda
        se ta nova narudzba zajedno sa svim drugima vrati kao order
        pa se onda moru javit duple nove narudzbe
     */
    private List<? extends EatOnObject> getOrLoad(Class clazz){
        List<? extends EatOnObject> list = EatOnObject.getAllInstances(clazz);
        if(list == null){
            list = loadJSON(clazz);
        }
        return list;
    }


    public List<EatOnObject> loadJSON(Class clazz) {


        try {
            InputStream is = context.getAssets().open(clazz.getSimpleName().toLowerCase()+".json");
            EatOnObject.log("__________READING__________________"+clazz.getSimpleName().toLowerCase()+".json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            String json = new String(buffer, "UTF-8");

            return (List<EatOnObject> )parseJSON(clazz, json);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }



    /*
    @Override
    public User getUser() {
        User user = new User(1L, "Name", "LastName", "Pula", "Rovinjska 14", true, 44.8674726,13.8485643);
        user.setPhoneNumber("098 1029 241");

        //long id, MenuItem item, int amount, double discount, String remark
        OrderItem item = new OrderItem(100, getMenuItems(null).get(0), 1, 0.5, null);
        Order order = getOrders(null).get(0);
        order.addOrderItem(item);

        user.addNewOrder(order);

        user.setRecentMenuItems(getMenuItems(null));
        user.setRecentCompanies(getCompanies());
        return user;
    }

    @Override
    public List<Company> getCompanies() {
        List<Company> companies = new ArrayList<>();


        Company c = new Company(7, "Restarurant test6", "Pula", "Some address 6", 44.8672919,13.8494811, "5555555", new EatOnImage("http://www.piksel-print.hr/wp-content/uploads/2015/07/Restoran-Kristal-znak-i-logo-500x363.jpg"), true, true);
        c.setDescription("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s");
        c.setMenus(getMenus(c));

        companies.add(c);

        //( id, name, city, address,
        //  latitude, longitude, phoneNumber, photo, delivery, openingHours)
        companies.add(new Company(2, "Restarurant test1", "Pula", "Some address 1", 44.8701229,13.8475767, "0000000", new EatOnImage("https://image.freepik.com/free-vector/restaurant-logo_23-2147506959.jpg"), true, true));
        companies.add(new Company(3, "Restarurant test2", "Pula", "Some address 2", 44.8701008,13.849889, "1111111", new EatOnImage("http://www.restaurant-feral.com/konobalogo.png"), false, true));
        companies.add(new Company(4, "Restarurant test3", "Pula", "Some address 3", 44.8699143,13.8514041, "2222222", new EatOnImage("https://logopond.com/logos/33ad7ed4b6c243941289d647a0cd6efc.png"), true, true));
        companies.add(new Company(5, "Restarurant test4", "Pula", "Some address 4", 44.8684779,13.8510951, "3333333", new EatOnImage("http://devfloat.net/wp-content/uploads/2016/04/aldo-pizza-logo.jpg"), false, true));
        companies.add(new Company(6, "Restarurant test5", "Pula", "Some address 5", 44.8672909,13.8494801, "4444444", new EatOnImage("http://pizzeriaguerrilla.ca/wp-content/uploads/2016/10/Pizzeria_Logo-Drop.png"), true, true));

        return companies;
    }

    @Override
    public List<Company> getCompanies(Map<String, Object> params) {
        return getCompanies();
    }

    @Override
    public Company getCompany(long id) {
       /* for (Company company : Company.getCompanies()) {
            if(company.getId() == id)return company;
        }*//*
        return null;
    }
*/

/*
    @Override
    public List<Menu> getMenus(Company company) {
        List<Menu> menus = new ArrayList<>();
//item: id, String name, double price,float rate, EatOnImage photo, double discount, String description
        menus.add(new Menu(7, MenuType.VEGAN,  new ArrayList<MenuItem>(){{
            add(new MenuItem(9, "Salate", 20.33, 1,new EatOnImage("https://www.cicis.com/media/1235/saladbar_saladmix.png"), 0, "Odlično jelo za ručak ili večeru. Lagano i fino, a usto i zdravo!"));
            add(new MenuItem(10, "Salate small", 43.32, 2, new EatOnImage("https://recipes.heart.org/-/media/aha/recipe/recipe-images/mediterranean-salad.jpg?h=672&w=940&la=en&hash=37880DC5279DCD1DFC0EB5A60153B3705C50E568"), 10, "Smaller portion of salate with some extra ingredients"));
            add(new MenuItem(11, "Salate big", 5.99, 5, new EatOnImage("https://www.ndtv.com/cooks/images/tossed-mixed-salad-620.jpg"), 5, "Odlično jelo za ručak ili večeru. Lagano i fino, a usto i zdravo!"));
            add(new MenuItem(12, "Pizza mješana", 50.99, 5, new EatOnImage("http://www.camyspizzasurrey.com/wp-content/uploads/2015/10/home_pizza_slider_1.png"), 5, "Odlično jelo za ručak ili večeru. Lagano i fino, a usto i zdravo!"));
            add(new MenuItem(13, "Pizza šunka sir", 22.32, 4, new EatOnImage("https://istanbulcuisinewa.com/wp-content/uploads/2017/06/pizza_adven_zestypepperoni-600x600.png"), 5, "Odlično jelo za ručak ili večeru. Lagano i fino, a usto i zdravo!"));
        }}));


        menus.add(new Menu(12, MenuType.MEAT, new ArrayList<MenuItem>(){{
            add(new MenuItem(13, "Stake", 120.33,3,  new EatOnImage("https://foodstreet.pk/wp-content/uploads/2015/06/juicy-steak.jpg"), 0, "Odlično jelo za ručak ili večeru. Lagano i fino, a usto i zdravo!"));
            add(new MenuItem(14, "Stake small", 143.32, 0,new EatOnImage("http://static6.uk.businessinsider.com/image/54639154dd08957c558b4573-480/rib-steak-at-new-york-steakhouse-old-homestead.jpg"), 10, "Smaller portion of stake with some extra ingredients"));
            add(new MenuItem(15, "Stake big", 250.99, 1,new EatOnImage("https://img.sndimg.com/food/image/upload/w_555,h_416,c_fit,fl_progressive,q_95/v1/img/recipes/21/36/59/picUmHJUy.jpg"), 5, "Big portion of stake"));
        }}));

        for (Menu mi : menus) {
            mi.setCompany(company);
            for (MenuItem menuItem : mi.getMenuItems()) {
                menuItem.setMenu(mi);

            }
        }


        return menus;
    }

    @Override
    public List<Reservation> getReservations(EatOnEntity eatOnEntity) {
        final Company company = getCompanies().get(0);
        //id, EatOnTime dateTime, int status, String remark
        List<Reservation> reservations = new ArrayList<Reservation>() {{
            add(new Reservation(16, new EatOnTime(), Status.REQUESTED, company, null));
            add(new Reservation(17, new EatOnTime(), Status.CANCELED, company, null));
            add(new Reservation(18, new EatOnTime(), Status.CONFIRMED, company, "By the window"));
        }};

        return reservations;
    }

    @Override
    public List<Order> getOrders(EatOnEntity eatOnEntity) {
        final Company company = getCompanies().get(0);
        List<Order> orders = new ArrayList<Order>() {{
            add(new Order(19, new EatOnTime(), Status.ORDERED, company, new EatOnTime(), 0, "Molim da na svaki od jela stavite origano, te da za svakog dodate zasebne vilice."));
            add(new Order(20, new EatOnTime(), Status.CONFIRMED, company, new EatOnTime(), 0, "bring it on the roof"));
            add(new Order(21, new EatOnTime(), Status.PROCESSING, company, new EatOnTime(), 0, "ring the bell"));
            add(new Order(22, new EatOnTime(), Status.DELIVERED, company, new EatOnTime(), 4, null));

        }};

        Order o = orders.get(0);
        MenuItem menuItem = getMenuItems(null).get(0);
        OrderItem item = new OrderItem(100, menuItem, 1, 0.5, null);
        o.addOrderItem(item);
        item = new OrderItem(101, menuItem, 2, 0.0, null);
        o.addOrderItem(item);

        item = new OrderItem(103, menuItem, 1, 0.0, null);
        o.addOrderItem(item);

        item = new OrderItem(104, menuItem, 5, 0.0, null);
        o.addOrderItem(item);

        item = new OrderItem(105, menuItem, 1, 0.0, null);
        o.addOrderItem(item);

        item = new OrderItem(106, menuItem, 1, 0.0, null);
        o.addOrderItem(item);

        item = new OrderItem(107, menuItem, 1, 0.0, null);
        o.addOrderItem(item);

        o = orders.get(1);
        o.addOrderItem(item);


        item = new OrderItem(102, menuItem, 3, 0.0, null);
        orders.get(0).addOrderItem(item);


        o = orders.get(2);
        o.addOrderItem(item);

        return orders;
    }

    @Override
    public List<MenuItem> getMenuItems(Map<String, Object> params) {
        return null;
    }
/*

    @Override
    public List<MenuItem> getMenuItems(Map<String, Object> params) {

        /*
        Menu m = getMenus(null).get(0);

        m.setCompany(getCompanies().get(0));

        for(MenuItem menuItem : m.getItems())
            menuItem.setMenu(m);

        /*
        List<MenuItem> items = new ArrayList<>();

        items.add(new MenuItem(13, "Stake", 120.33,3, new EatOnImage("http://www.testoviautomobila.net/wp-content/uploads/2017/02/alfa-840x420.jpg"), 0, null));
        items.add(new MenuItem(14, "Stake small", 143.32, 0,new EatOnImage("http://imagesvc.timeincapp.com/v3/foundry/image/?q=70&w=1440&url=https%3A%2F%2Ftimedotcom.files.wordpress.com%2F2017%2F08%2Falfa-quadrifoglio.jpg%3Fquality%3D85"), 10, "Smaller portion of stake with some extra ingredients. Smaller portion of stake with some extra ingredients. Smaller portion of stake with some extra ingredients. Smaller portion of stake with some extra ingredients"));
        items.add(new MenuItem(15, "Stake big", 250.99, 1,new EatOnImage("http://www.camyspizzasurrey.com/wp-content/uploads/2015/10/home_pizza_slider_1.png"), 5, "Big portion of stake"));

        return items;
        */
        /*
      //  return m.getItems();
        List<MenuItem> items = new ArrayList<>();
        for (Menu menu : getMenus(getCompanies().get(0))) {
            items.addAll(menu.getMenuItems());
        }
        Log.d("COUNT, ", items.size() + "");
        return items;
    }
    */
}
