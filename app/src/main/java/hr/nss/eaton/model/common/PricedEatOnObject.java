package hr.nss.eaton.model.common;

/**
 * Created by Robert on 01-Nov-18.
 */

public abstract class PricedEatOnObject extends EatOnObject {

    protected String name;
    protected double price;
    protected EatOnImage photo;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public EatOnImage getPhoto() {
        return photo;
    }

    public void setPhoto(EatOnImage photo) {
        this.photo = photo;
    }
}
