package hr.nss.eaton.utils;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.nss.eaton.model.common.EatOnObject;

/**
 * Created by Robert on 02-Nov-18.
 */

public class ReflectionUtils {


    public static Field getField(Class<?> clazz, String fieldName) {
        Class<?> tmpClass = clazz;
        do {
            try {
                Field f = tmpClass.getDeclaredField(fieldName);
                return f;
            } catch (NoSuchFieldException e) {
                tmpClass = tmpClass.getSuperclass();
            }
        } while (tmpClass != null);
        return null;
    }

    public static boolean setFieldValue(Field field, Object instance, Object value){
        try {
            //Field field = base.getClass().getField(fieldName);

            boolean accesible = field.isAccessible();
            if(!accesible) field.setAccessible(true);


            field.set(instance, value);


            if(!accesible) field.setAccessible(false);
            return true;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static  boolean setFieldValue(Object base, String fieldName, Object value){
         return   setFieldValue(getField(base.getClass(), fieldName), base, value);
    }

    public static Object getFieldValue(Object base, String fieldName){
            Field field = getField(base.getClass(), fieldName);
            return getFieldValue(base, field);
    }

    public static Object getFieldValue(Object base, Field field){
        try {

            boolean accesible = field.isAccessible();
            if(!accesible) field.setAccessible(true);


            Object value = field.get(base);


            if(!accesible) field.setAccessible(false);
            return value;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Class getFieldClass(Class clazz, String fieldName){
            Field field = getField(clazz, fieldName);

            return field.getType();
    }


    public static Class getGenericClassArgumentType(Class clazz){
        return getGenericClassArgumentType(clazz, 0);
    }

    public static Class getGenericClassArgumentType(Class clazz, int index){
        return (Class) (((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[index]);
    }

    public static Class getParameterizedTypeClass(Field field){
        ParameterizedType type = (ParameterizedType) field.getGenericType();
        return ((Class) type.getActualTypeArguments()[0]);
    }

    public static List<Field> getFields(Class c){
//        return Arrays.asList(c.getFields());

        List<Field> fields = new ArrayList<>();
        Class clazz = c;
        while (clazz != Object.class) {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

}
