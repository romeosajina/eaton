package hr.nss.eaton.controller.user;

import java.util.List;

import hr.nss.eaton.controller.status.ActionStatus;
import hr.nss.eaton.model.common.EatOnImage;
import hr.nss.eaton.model.transaction.Order;
import hr.nss.eaton.model.transaction.Reservation;
import hr.nss.eaton.model.user.User;

/**
 * Created by Robert on 29-May-18.
 */

public class UserController {
/*
    private User user;
    private boolean registered = true;
    private static UserController instance;
    public static UserController get(){
        if(instance == null)instance = new UserController();
        return instance;
    }

    private UserController() {
        initUser();
    }

    private void initUser(){
        this.user = User.get();
        if(user == null){
            user = new User(-1L, "", "", "", "",true, 0, 0);
            registered = false;
        }
    }

    public boolean isRegistered() {
        return registered;
    }

    public String getName(){
        return user.getName();
    }
    public String getSurname(){
        return user.getSurname();
    }
    public String getAddress(){
        return user.getAddress();
    }
    public EatOnImage getImage(){
        if(user.getPhoto() != null){
            return user.getPhoto();
        }

        return new EatOnImage();
    }

    public String getPhoneNumber(){
        if(user.isShowNumber())
            return user.getPhoneNumber();
        return null;
    }

    public String getCity(){
        return user.getCity();
    }



    public List<Order> getOrders() {
        return user.getOrders();
    }
    public List<Order> getPendingOrders(){

        return user.getOrders().subList(0,2);
    }
    public List<Order> getCompletedOrders(){
        return user.getOrders();
    }
    public List<Reservation> getReservations(){
        return user.getReservations();
    }


    public ActionStatus createOrder(Order order){

        if(!registered)return ActionStatus.USER_NOT_REGISTERED;

        user.addOrder(order);

        return null;
    }

    public ActionStatus createReservation(Reservation reservation){

        if(!registered)return ActionStatus.USER_NOT_REGISTERED;

        user.addReservation(reservation);

        return null;
    }
    */
}
