package hr.nss.eaton.controller.status;

/**
 * Created by Robert on 29-May-18.
 */

public enum ActionStatus {
    USER_NOT_REGISTERED(0, Status.Error);

    private int msgId;
    private Status status;

     ActionStatus(int msgId, Status status) {
        this.msgId = msgId;
        this.status = status;
    }

    public int getMsgId() {
        return msgId;
    }

    public Status getStatus() {
        return status;
    }

    public boolean isError(){
         return getStatus() == Status.Error;
    }
    public boolean isWarning(){
        return getStatus() == Status.Warning;
    }
    public boolean isInfo(){
        return getStatus() == Status.Info;
    }


    public enum Status {
        Error,
        Warning,
        Info

    }

}
