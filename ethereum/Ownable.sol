pragma solidity ^0.5.1;

contract Ownable {
    
    address payable private owner;

    constructor() public{
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Nisi vlasnik!");
        _;
    }
    
    function getOwner() public view returns (address) {
        return owner;
    }

    function widraw(uint _amount) public onlyOwner {
        require(address(this).balance > _amount);
        
        msg.sender.transfer(_amount);
    }
    
    function die() public onlyOwner {
        selfdestruct(owner);
    }    
    
}