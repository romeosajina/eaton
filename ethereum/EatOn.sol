pragma solidity ^0.5.1;

import './Ownable.sol';
import './Rewardable.sol';


contract EatOn is Ownable, Rewardable {
    
    
    //struktura za pracenje narudzbi
    struct Order {
     //   uint32 id; // id narudzbe
        uint amount; //in wei
        address user; // adressa korisnika koji je narucia narudzbu
        uint date; // vrijeme placanja
        bool rewarded; // oznaka da li je ova narudzba nagrada od company-a
    }
    

    mapping(address => mapping (uint32 => Order)) companyOrders;
    
    // sveukupni promet svih narudzbi
    uint private totalSupply;
    
 
    /**
     * funkcija koja provjerava dali narudzba postoji 
     * Provjerava se dali postoji iznos na narudzbi
     * Ako narudzba ne postoji ili je narudzba vec unovcena vraca false, inace true
     * @param _id id narudzbe 
     * @param _company adresa kompanije kojoj pripada narudzba
     */
    function hasOrder(uint32 _id, address _company) public view returns (bool) {
        // mora postojat taj order sa nekin iznosom na narudzbi
        // ako neki proba pozvat metodu bacit ce gresku je on npr ni company
        // ili ta narudzba ne postoji tu
        return (companyOrders[_company][_id].amount > 0 );
    }
    
    /**
     * Funkcija za krace provjeravanje postojanja narudzbe za company
     * 
     */
    function hasOrder(uint32 _id) public view returns (bool) {
        return hasOrder(_id, msg.sender);
    }
    
    
    
    /**
     * Funkcija koja vraca dali je narudzba nagradena od strane kompanije
     * @param _id id narudzbe za koju se provjerava da li je nagradena
     * @param _company adresa kompanije kojoj pripada narudzba
     * 
     */
    function isOrderRewarded(uint32 _id, address _company) public view returns (bool) {
        return (companyOrders[_company][_id].rewarded);
    }
    
    
    /**
     * Funckija koja vraca dali je pozivatelj nagradia narudzbu sa id-em _id
     * @param _id id narudzbe za koju se provjerava da li je nagradena
     * 
     */
    function isOrderRewarded(uint32 _id) public view returns (bool){
        return isOrderRewarded(_id, msg.sender);
    }
    
    /**
     * Modifier koji provjerava dali narudzba potoji
     * 
     */
    modifier orderExist(uint32 _id, address _company) {
        
        require(hasOrder(_id, _company), "Narudzba ne postoji ili je vec obradena.");
        
        _;
    }    

    /**
     * Modifier koji provjerava da narudzba ne postoji
     * 
     */
    modifier orderNotExist(uint32 _id, address _company) {
        
        require(!hasOrder(_id, _company), "Narudzba postoji i nemoze se mjenjat.");
        
        _;
    }       
    
    /**
     * modifier koji provjerava da narudzba nije nagradena
     * @param _id id narudzbe za koju se provjerava
     * @param _company kompanija kojoj narudzba pripada
     * 
     */
    modifier orderNotRewarded(uint32 _id, address _company) {
        
        require(!isOrderRewarded(_id, _company), "Narudzba nesmije biti nagradena.");
        
        _;
    }    
    
    
    


    /**
     * modifier koji provjerava da li postoji narudzba koju su korisnik i kompanija uspostavili
     * @param _id id narudzbe za koju se provjerava
     * @param _company kompanija kojoj narudzba pripada
     * @param _user korisnik koji je narudzbu narucia
     * 
     */
    modifier userOrder(uint32 _id, address _company, address _user){
          require(msg.sender == companyOrders[_company][_id].user, 
                        "Sender nije korisnik koji je narucio narudzbu");
          
          _;
    }
    

    

    /**
     * Funkcija koju poziva koju poziva klijent kada placa narudzbu
     * Ako ta narudzba vec postoji javlja se greska
     * @param _id id narudzbe koju klijent zeli platiti
     * @param _to adresa company-a kojoj se placa narudzba
     */
    function placeOrder(uint32 _id, address _to) public orderNotExist(_id, _to) payable returns (bool) {
        // mora imat neku vrijednost pridodanu transakciji
        require(msg.value > 0 );

        //oduzmemo gaslimit od vrijednosti da moremo poslat nazad korisniku ili company-u
        uint amount = msg.value - block.gaslimit;
        //kreiraj nazudzbu  i izracunaj da li je korisnik nagraden besplatom kupovinom
        Order memory order = Order (/*_id,*/ amount, msg.sender, now, isRewardedFrom(_to));
        
        // dodaj narudzbu u mapping narudzbi company-a
        companyOrders[_to][_id] = order;
        
        // povecaj promet
        totalSupply += amount;  
        
        // ako je nagraden vrati nazad novce korisniku
        if(order.rewarded) msg.sender.transfer(amount);
        
        return true;
    }

    
    /**
     * Funkcija ponistava narudzbu i vraca korisniku novac, ako je korisnik kupac te narudzbe
     * Ako je narudzba vec potvrdena ili ne postoji, izbacit ce gresku
     * @param _id id narudzbe
     * @param _company adresa company-e kojoj narudzba pripada
     * 
     */
    function cancelOrder(uint32 _id, address _company) public 
                orderExist(_id, _company) 
                userOrder(_id, _company, msg.sender) 
                orderNotRewarded(_id, _company) {
        
        uint amount = companyOrders[_company][_id].amount;
        
        // izbrisi narudzbu
        delete companyOrders[_company][_id];
        
        //vrati nazad novac korisniku
        msg.sender.transfer(amount); 
    }
    

     /**
      * Poziva je company kad se narudzba potvrdi
      * @param _id id narudzbe
      * ako narudzba s tim id-om ne postoji ili je vec unovcena javlja se greska
      */
    function completeOrder(uint32 _id) public orderExist(_id, msg.sender) orderNotRewarded(_id, msg.sender) {
    
        uint amount = companyOrders[msg.sender][_id].amount;
        
        // izbrisi narudzbu iz popisa narudzbi company-e
        delete companyOrders[msg.sender][_id];

        // prenesi ether companiji
        msg.sender.transfer(amount); 
    }
    


    /**
     * Vraca ukupan promet koji je prosa kroz ovaj contract
     * 
     */
    function currentSupply() public view returns (uint){
        return totalSupply;
    }
    
    
    // Get any donation
    function () external payable {}
}