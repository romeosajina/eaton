pragma solidity ^0.5.1;

import './Random.sol';


/**
 * Company ima kod sebe definiran parametar nakon koliko narudzbi 
 * korisnik dobiva besplatnu kupovinu. Ako je parametar manji ili jednak nula onda ne nagraduju
 * 
 * Svaki put kad korisnik dobije besplatno narudzbu, onda se resetira broj narudzbi koje je korisnik narucia
 * Otkazivanje narudzbi se "nagraduje" negativnim bodovima
 * 
 * Kada korisnik stekne predizpozicije za placenu narudzbu random se racuna da li je korisnik osvojia besplatnu kupovinu
 * Trebalo bi se povecavat sanse sve ca vise odskace od parametra
 * 
 * 
 */

contract Rewardable is Random {

   
   mapping(address => int8) private rewardableCount;
   
   mapping(address => int8) private userCount;
   
   // konstanta za minimalnu vrijednost da se narudzba broji kao rewardable narudzba
   // inace bi se moglo spamat dodavanje random narudzbi i povecalo brojac
   uint constant private MIN_AMOUNT = 0.05 ether;
    
    
    function setRewardableCount(int8 count) public {
        rewardableCount[msg.sender] = count;
    }
    
    function getRewardableCount() public view returns(int8) {
        return getRewardableCount(msg.sender);
    }
    
    function getRewardableCount(address _from) public view returns(int8) {
        return rewardableCount[_from];
    }
    
    
    function addUserCount() internal {
        userCount[msg.sender] += 1;
    }
    
    function resetUserCount() internal {
        userCount[msg.sender] = 0;
    }
    
    function getUserCount() internal view returns(int8) {
        return userCount[msg.sender];
    }
    
    
    function rewards(address _address) internal view returns(bool) {
        return rewardableCount[_address] > 0;
    }
    
    
    /**
     * @param _address adresa za koju se provjerava i prati dali nagraduje
     */
    function canRewardUser(address _address) internal view returns (bool) {
       
        // ako adresa ne nagraduje ili 
        // ako je vrijednost poslana sa transakcijom manja od minimalne onda se ne moze nagradit korisnika
        if(!rewards(_address) || msg.value < MIN_AMOUNT) return false;
        
        // ako je narucia vise od zadanog parametra onda se moze nagradit korisnika
        return getUserCount() >= getRewardableCount(_address);
    }
    
    
    
    /**
     * Funkcija koja vraca da li je korisnik nagraden
     * ako nije nagraden onda se povecava njegov brojac narudzbi
     * Svaki put kada se korisnik nagradi resetira se njegov brojac
     * Ako adresa ne nagraduje ili korisnik nije uplatio minimalni iznos vraca false
     * @param _address adresa za koju se provjerava i prati dali nagraduje
     * 
     */
    function isRewardedFrom(address _address) internal returns (bool) {
        
        if(canRewardUser(_address)) return false;
        
        
        bool isRewarded = randomBool();
        
        if(isRewarded){
            resetUserCount();
            return true;
        }
        
        addUserCount();
        
        return false;
    }
    
    
    /**
     * Random funkcija sa vjerojatnosti od 10%
     * 
     */
    function randomBool() internal returns (bool) {
        
        return random(10) == 5;
    }
    
}