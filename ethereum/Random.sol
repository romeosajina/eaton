pragma solidity ^0.5.1;


contract Random {

    uint private seed;

   function random(uint max) internal returns (uint) {
    seed = uint(keccak256(abi.encodePacked(now, msg.sender, seed))) % max;
    return seed;
    }
}

